$(window).ready(function() {
	
	$('#btn_open_cont_editar_conta').on('click', function() {
		
		$('#cont_editar_conta').css({"right":"0"});
	});

	$('#cont_editar_conta').find('.btn_close').on('click', function() {
		
		$('#cont_editar_conta').css({"right":"-110%"});
	});

	// ======== add noticia ========
	$('#btn_open_cont_add_noticia').on('click', function() {
		
		$('#cont_add_noticia').css({"right":"0"});
	});

	$('#cont_add_noticia').find('.btn_close').on('click', function() {
		
		$('#cont_add_noticia').css({"right":"-110%"});
	});

	$('#input_file_add_img').off().on('change', function() {
			
		var extension = $(this).val().split('.').pop().toLowerCase();

		if($.inArray(extension, ['png','jpg','jpeg']) != -1) {
		    
		    if(this.files && this.files[0]) {
	            var reader = new FileReader();
	            var img = new Image();

	            reader.onload = function (e) {
	                $('#img_add_noticia').attr('src', e.target.result);	

		            img.src = $('#img_add_noticia').attr('src');

		            var canvas = $( '<canvas class="snapshot-generator" id="snapshot-generator"></canvas>' ).appendTo(document.body)[0];
                    var image = new Image();
                    image.src = e.target.result;
                    
                    image.onload = function (imageEvent) {

                        canvas.width = image.width;
                        canvas.height = image.height;
                        var ctx = canvas.getContext('2d');
                        ctx.fillStyle ="#fff";
                        ctx.fillRect(0, 0, image.width, image.height);

                        ctx.drawImage(image, 0, 0, image.width, image.height);
                        
                        $('#img_add_noticia').attr('src', canvas.toDataURL()).fadeIn();	
                    }
	            }
	            reader.readAsDataURL(this.files[0]);
	        }
		} else {

			$('#img_add_noticia').attr('src', '').hide();
		}
	});

	$('#cont_add_noticia').find('.btn_send').on('click', function() {
		
		var cont = $('#cont_add_noticia');
		var fonte = $.trim(cont.find('input[name="fonte"]').val());
		var link = $.trim(cont.find('input[name="link"]').val());
		var descricao = $.trim(cont.find('textarea').val());

		if(fonte != '' && link != '' && descricao != '') {

			var sender = new FormData();

			sender.append('acao', 'add_noticia');
			sender.append('fonte', fonte);
			sender.append('link', link);
			sender.append('descricao', descricao);

			var image = new Image();
	        image.src = $('#img_add_noticia').attr('src');	
	        image.onload = function (imageEvent) {

	        	if($('#snpashot-generator').length > 0) {

	                var canvas = document.getElementById('snpashot-generator');
	            } else {

	                var canvas = $( '<canvas class="snapshot-generator" id="snapshot-generator"></canvas>' ).appendTo(document.body)[0];
	            }

	            width = image.width;
	            height = image.height;

	            var maxSize = 500; //maximo da largura da imagem

	            height *= maxSize / width;
	            width = maxSize;

	            canvas.width = width;
	            canvas.height = height;

	            canvas.getContext('2d').drawImage(image, 0, 0, width, height);

	            canvas.toBlob(function (imagem) {

	            	sender.append('file', imagem);
	            	
	            	sender_add_noticia(sender);
	            }, 'image/jpeg', 0.8);
	        }
		} else {

			$('#cont_add_noticia').find('.alert').html('<strog>Oops: </strong>Todos os campos são obrigatórios!').fadeIn();
		}
	});
	// ======== fim add noticia ========


	//===== deletar noticia =======
	$('#cont_alert_del_noticia').find('.btn_close').on('click', function() {
		
		$('#cont_alert_del_noticia').css({"right":"-110%"});
	});

	$('.btn_deletar_noticia').on('click', function() {
		
		var id = this.id;

		$('#cont_alert_del_noticia').css({"right":"0"});

		$('#cont_alert_del_noticia').find('.btn_sim').on('click', function() {
		
			window.location.href = "jornalista/delet_noticia/"+id;
		});
	});	
	//===== fim deletar noticia ========

	//===== editar noticia ============
	$('#cont_update_noticia').find('.btn_close').on('click', function() {
		
		$('#cont_update_noticia').css({"right":'-110%'});
	});

	$('#input_file_update_img').off().on('change', function() {
			
		var extension = $(this).val().split('.').pop().toLowerCase();

		if($.inArray(extension, ['png','jpg','jpeg']) != -1) {
		    
		    if(this.files && this.files[0]) {
	            var reader = new FileReader();
	            var img = new Image();

	            reader.onload = function (e) {
	                $('#img_update_noticia').attr('src', e.target.result);	

		            img.src = $('#img_update_noticia').attr('src');

		            var canvas = $( '<canvas class="snapshot-generator" id="snapshot-generator"></canvas>' ).appendTo(document.body)[0];
                    var image = new Image();
                    image.src = e.target.result;
                    
                    image.onload = function (imageEvent) {

                        canvas.width = image.width;
                        canvas.height = image.height;
                        var ctx = canvas.getContext('2d');
                        ctx.fillStyle ="#fff";
                        ctx.fillRect(0, 0, image.width, image.height);

                        ctx.drawImage(image, 0, 0, image.width, image.height);
                        
                        $('#img_update_noticia').attr('src', canvas.toDataURL()).fadeIn();	
                    }
	            }
	            reader.readAsDataURL(this.files[0]);
	        }
		} else {

			$('#img_update_noticia').attr('src', '').hide();
		}
	});

	$('.btn_editar_noticia').on('click', function() {
		
		var id = this.id;

		var cont = $(this).closest('td');
		var descricao = cont.prev().text();
		var link = cont.prev().prev().text();
		var fonte = cont.prev().prev().prev().text();
		var img = cont.prev().prev().prev().prev().find('img').attr('src');

		cont = $('#cont_update_noticia');
		cont.find('input[name="fonte"]').val(fonte);
		cont.find('input[name="link"]').val(link);
		$('#img_update_noticia').attr('src', img).fadeIn();
		cont.find('textarea').val(descricao);

		cont.css({"right":'0'});

		$('#cont_update_noticia').find('.btn_savar_update_noticia').on('click', function() {

			var cont = $('#cont_update_noticia');
			var fonte = $.trim(cont.find('input[name="fonte"]').val());
			var link = $.trim(cont.find('input[name="link"]').val());
			var descricao = $.trim(cont.find('textarea').val());

			if(fonte != '' && link != '' && descricao != '') {

				var sender = new FormData();

				sender.append('acao', 'update_noticia');
				sender.append('fonte', fonte);
				sender.append('link', link);
				sender.append('descricao', descricao);
				sender.append('id', id);

				var extension = $('#input_file_update_img').val().split('.').pop().toLowerCase();

				if($.inArray(extension, ['png','jpg','jpeg']) != -1) {
					
					var image = new Image();
			        image.src = $('#img_update_noticia').attr('src');	
			        image.onload = function (imageEvent) {

			        	if($('#snpashot-generator').length > 0) {

			                var canvas = document.getElementById('snpashot-generator');
			            } else {

			                var canvas = $( '<canvas class="snapshot-generator" id="snapshot-generator"></canvas>' ).appendTo(document.body)[0];
			            }

			            width = image.width;
			            height = image.height;

			            var maxSize = 500; //maximo da largura da imagem

			            height *= maxSize / width;
			            width = maxSize;

			            canvas.width = width;
			            canvas.height = height;

			            canvas.getContext('2d').drawImage(image, 0, 0, width, height);

			            canvas.toBlob(function (imagem) {

			            	sender.append('file', imagem);
			            	
			            	sender_update_noticia(sender);
			            }, 'image/jpeg', 0.8);
			        }
			    } else {
			            	
			        sender_update_noticia(sender);
			    }
			} else {

				$('#cont_update_noticia').find('.alert').html('<strog>Oops: </strong>Todos os campos são obrigatórios!').fadeIn();
			}
		});
	});
	//===== fim editar noticia ============
});


function sender_add_noticia(sender) {
	
	$.ajax({
		processData:false,
		contentType:false,
        url: 'jornalista/crud',
        type: 'POST',
        data: sender,
        dataType: 'json',
        error: function(argument) { 
            console.log(argument);

            $('#cont_add_noticia').find('.alert').html('<strog>Oops: </strong>Erro inesperado!').fadeIn();
        },
        success: function(argument) {

        	if(argument == '1') {

        		$('#cont_add_noticia').find('.alert').html('<strog>Oops: </strong>Todos os campos são obrigatórios!').fadeIn();
        	} else if(argument == '2') {

        		$('#cont_add_noticia').find('.alert').html('<strog>Oops: </strong>Formato de imagem inválido!').fadeIn();
        	} if(argument == '5') {

        		window.location.reload();
        	}
        }
    });
}

function sender_update_noticia(sender) {
	
	$.ajax({
		processData:false,
		contentType:false,
        url: 'jornalista/crud',
        type: 'POST',
        data: sender,
        dataType: 'json',
        error: function(argument) { 
            console.log(argument);

            $('#cont_update_noticia').find('.alert').html('<strog>Oops: </strong>Erro inesperado!').fadeIn();
        },
        success: function(argument) {

        	console.log(argument);

        	if(argument == '1') {

        		$('#cont_update_noticia').find('.alert').html('<strog>Oops: </strong>Todos os campos são obrigatórios!').fadeIn();
        	} else if(argument == '2') {

        		$('#cont_update_noticia').find('.alert').html('<strog>Oops: </strong>Formato de imagem inválido!').fadeIn();
        	} if(argument == '5') {

        		window.location.reload();
        	}
        }
    });
}