<?php
class mpController extends controller {

	public function index() {

	}

	public function notificacao_mp() {

		$c = new Config();
		$config = $c->get_dados_config();
        
        $mp = new MP($config['MP_AppID'], $config['MP_SecretKey']);
        $mp->sandbox_mode(false);

        //file_put_contents('mp_log'.time().'get.txt', print_r($_SERVER["REQUEST_URI"], true));

        $array = explode('&', explode('?', $_SERVER["REQUEST_URI"])[1]);

        $id = '';

        foreach ($array as $key => $value) {
            
            if(explode('=', $value)[0] == 'id') {

                $id = explode('=', $value)[1];
            }
        }

        $info = $mp->get_payment_info($id);

        //file_put_contents('mp_log'.time().'info.txt', print_r($info, true));

        if($info['status'] == '200') {

            $array = $info['response'];
            
            $ref = $array['external_reference'];
            $status = $array['status'];
            /*
            pending = Em análise
            approved = Aprovado
            in_procress = Em revisão
            in_mediation = Em processo de disputa
            rejected = Foi rejeitado
            cancelled = Foi cancelado
            refunded = Reembolsado
            charged_back = Chargeback
            */

            $p = new Produtos_veda();

            if($status == 'approved') {
                $p->aprovar_mp($ref);
            } elseif($status == 'cancelled' || $status == 'rejected' || $status == 'refunded') {
                $p->cancelar_mp($ref);
            }
        }
    }

    public function success_mp() {

    	
    }

    public function analise_mp() {

    }

    public function cancelado_mp() {

    }
}