<?php 
class politicasController extends controller {

	public function index() {

		$dados = array();

		$c = new Config();

		$dados['politicas'] = $c->get_dados_config()['politicas'];

		$this->loadView('politicas', $dados);
	}
}