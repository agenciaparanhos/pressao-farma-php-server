<?php
class homeController extends controller {

	public function index() {

		$dados = array();

		$this->loadView('home', $dados);
	}

    public function crud() {

        switch ($_POST['acao']) {
            case 'get_home':

                $token = addslashes($_POST['token']);

                $c  = new Categorias();
                $f = new Produtos_favoritos();
                $b = new Banner();
                $pr = new Promocoes();

                $categorias = $c->get();
                $banner = $b->get();
                $favoritos = $f->get_usuario_favoritos($token);
                $promocoes = $pr->get();


                echo json_encode(array(
                    'r' => '5', 
                    'categorias' => $categorias, 
                    'favoritos' => $favoritos, 
                    'banner' => $banner,
                    'promocoes' => $promocoes,
                ));

                break;

            case 'get_notificacoes':
                
                $token = addslashes($_POST['token']);

                $a = new Avisos();
                $a->get($token);
                break;

            case 'get_minha_loja':
                
                $token = addslashes($_POST['token']);

                $l = new Lojas();
                $l->get_minha_loja($token);
                break;

            case 'get_categorias':

                $c = new Categorias();
                $categorias = $c->get();
                
                echo json_encode(array('r' => '5', 'dados' => $categorias));
                break;

            case 'get_subcategorias':

                $categoria = addslashes($_POST['categoria']);

                $c = new Categorias();
                $subcategorias = $c->get_sub($categoria);
                
                echo json_encode(array('r' => '5', 'dados' => $subcategorias));
                break;

            case 'add_produto':
                
                $token = addslashes($_POST['token']);
                $nome = addslashes($_POST['nome']);
                $tamanho = addslashes($_POST['tamanho']);
                $condicao = addslashes($_POST['condicao']);
                $marca = addslashes($_POST['marca']);
                $descricao = addslashes($_POST['descricao']);
                $categoria = addslashes($_POST['categoria']);
                $subcategoria = addslashes($_POST['subcategoria']);
                $peso = str_replace(',', '.', $_POST['peso']);
                $peso = addslashes($peso);
                $largura = addslashes($_POST['largura']);
                $comprimento = addslashes($_POST['comprimento']);
                $altura = addslashes($_POST['altura']);
                $preco = str_replace(',', '.', $_POST['preco']);
                $preco = addslashes($preco);

                $total = count($_FILES['imgs']['name']);

                $nomes = array();
                // Loop through each file
                for( $i=0 ; $i < $total ; $i++ ) {

                    $ext = explode('.', $_FILES['imgs']['name'][$i]);
                    $ext = array_pop($ext);

                    $name = md5(time().rand(0,999)).'.'.$ext;

                  //Get the temp file path
                  $tmpFilePath = $_FILES['imgs']['tmp_name'][$i];

                  //Make sure we have a file path
                  if ($tmpFilePath != ""){
                    //Setup our new file path
                    $newFilePath = "../assets/img/produtos/" . $name;

                    //Upload the file into the temp dir
                    if(move_uploaded_file($tmpFilePath, $newFilePath)) {

                      //Handle other code here
                        $nomes[] = $name;
                    }
                  }
                }

                $p = new Produtos();
                $p->add($token, $nome, $tamanho, $condicao, $marca, $descricao, $preco, $nomes, $categoria, $subcategoria, $altura, $comprimento, $largura, $peso);
                break;

            case 'get_minha_loja_to_edit':

                $token = addslashes($_POST['token']);
                
                $l = new Lojas();
                $l->get_minha_loja_to_edit($token);
                break;

            case 'update_minha_loja':
                
                $token = addslashes($_POST['token']);
                $nome = addslashes($_POST['nome']);
                $bairro = addslashes($_POST['bairro']);
                $endereco = addslashes($_POST['endereco']);
                $cidade = addslashes($_POST['cidade']);
                $estado = addslashes($_POST['estado']);
                $cep = addslashes($_POST['cep']);

                $l = new Lojas();

                if(isset($_FILES['banner']) && !empty($_FILES['banner']['tmp_name'])) {

                    $ext = explode('.', $_FILES['banner']['name']);
                    $ext = array_pop($ext);

                    $name = md5(time().rand(0,999)).'.'.$ext;

                    $tmpFilePath = $_FILES['banner']['tmp_name'];

                    if ($tmpFilePath != ""){
                        
                        $newFilePath = "../assets/img/lojas/" . $name;

                        //Upload the file into the temp dir
                        if(move_uploaded_file($tmpFilePath, $newFilePath)) {

                          
                          $l->update_banner($name, $token);
                        }
                    }
                }

                if(isset($_FILES['logo']) && !empty($_FILES['logo']['tmp_name'])) {

                    $ext = explode('.', $_FILES['logo']['name']);
                    $ext = array_pop($ext);

                    $name = md5(time().rand(0,999)).'.'.$ext;

                    $tmpFilePath = $_FILES['logo']['tmp_name'];

                    if ($tmpFilePath != ""){
                        
                        $newFilePath = "../assets/img/lojas/" . $name;

                        //Upload the file into the temp dir
                        if(move_uploaded_file($tmpFilePath, $newFilePath)) {

                          
                          $l->update_logo($name, $token);
                        }
                    }
                }

                $l->update_nome($nome, $cep, $bairro, $endereco, $cidade, $estado, $token);
                break;

            case 'update_produto':
                
                $token = addslashes($_POST['token']);
                $nome = addslashes($_POST['nome']);
                $tamanho = addslashes($_POST['tamanho']);
                $condicao = addslashes($_POST['condicao']);
                $marca = addslashes($_POST['marca']);
                $descricao = addslashes($_POST['descricao']);
                $categoria = addslashes($_POST['categoria']);
                $subcategoria = addslashes($_POST['subcategoria']);
                $largura = addslashes($_POST['largura']);
                $peso = str_replace(',', '.', $_POST['peso']);
                $peso = addslashes($peso);
                $comprimento = addslashes($_POST['comprimento']);
                $altura = addslashes($_POST['altura']);

                $old_imgs = explode(',', $_POST['old_imgs']);

                $i = array();

                foreach ($old_imgs as $key => $value) {
                    
                    $i[] = "'".addslashes($value)."'";
                }

                $old_imgs = implode(',', $i);

                $id = addslashes($_POST['id']);
                $preco = str_replace(',', '.', $_POST['preco']);
                $preco = addslashes($preco);

                $nomes = array();

                if(isset($_FILES['imgs']) && !empty($_FILES['imgs'])) {

                    $total = count($_FILES['imgs']['name']);

                    // Loop through each file
                    for( $i=0 ; $i < $total ; $i++ ) {

                        $ext = explode('.', $_FILES['imgs']['name'][$i]);
                        $ext = array_pop($ext);

                        $name = md5(time().rand(0,999)).'.'.$ext;

                      //Get the temp file path
                      $tmpFilePath = $_FILES['imgs']['tmp_name'][$i];

                      //Make sure we have a file path
                      if ($tmpFilePath != ""){
                        //Setup our new file path
                        $newFilePath = "../assets/img/produtos/" . $name;

                        //Upload the file into the temp dir
                        if(move_uploaded_file($tmpFilePath, $newFilePath)) {

                          //Handle other code here
                            $nomes[] = $name;
                        }
                      }
                    }
                }

                $p = new Produtos();
                $p->update($token, $nome, $tamanho, $condicao, $marca, $descricao, $preco, $nomes, $categoria, $subcategoria, $id, $old_imgs, $altura, $comprimento, $largura, $peso);
                break;

            case 'deletar_produto':
                
                $token = addslashes($_POST['token']);
                $id = addslashes($_POST['id']);

                $p = new Produtos();
                $p->deletar($token, $id);
                break;

            case 'get_produto':
                
                $token = addslashes($_POST['token']);
                $id = addslashes($_POST['id']);

                $p = new Produtos();
                $p->get_produto($token, $id);
                break;

            case 'add_favorito':
                
                $token = addslashes($_POST['token']);
                $id_produto = addslashes($_POST['id_produto']);
                
                $f = new Produtos_favoritos();
                $f->add($token, $id_produto);
                break;

            case 'get_favoritos':
                
                $token = addslashes($_POST['token']);
                
                $f = new Produtos_favoritos();
                $f->get_favoritos($token);
                break;

            case 'seguir':
                
                $token = addslashes($_POST['token']);
                $id_loja = addslashes($_POST['id_loja']);
                
                $f = new Lojas();
                $f->seguir($token, $id_loja);
                break;

            case 'get_loja':
                
                $token = addslashes($_POST['token']);
                $id_loja = addslashes($_POST['id_loja']);
                
                $f = new Lojas();
                $f->get_loja($token, $id_loja);
                break;

            case 'verificar_existencia_endereco_loja':
                
                $token = addslashes($_POST['token']);
                
                $u = new Usuarios();
                $u->verificar_existencia_endereco_loja($token);
                break;

            case 'criar_loja':
                
                $token = addslashes($_POST['token']);
                $cidade = addslashes($_POST['cidade']);
                $estado = addslashes($_POST['estado']);
                $bairro = addslashes($_POST['bairro']);
                $endereco = addslashes($_POST['endereco']);
                $cep = addslashes($_POST['cep']);

                $l = new Lojas();
                $l->add($token, $estado, $cidade, $bairro, $endereco, $cep);
                break;

            case 'calcular_frete':
                
                $token = addslashes($_POST['token']);
                $produto_id = addslashes($_POST['produto_id']);
                $cep = addslashes($_POST['cep']);
                $tipo_frete = addslashes($_POST['tipo_frete']);

                $l = new Lojas();
                $l->calcular_frete($token, $produto_id, $cep, $tipo_frete);
                break;

            case 'add_venda':
                
                $token = addslashes($_POST['token']);
                $produto_id = addslashes($_POST['produto_id']);
                $cep = addslashes($_POST['cep']);
                $tipo_frete = addslashes($_POST['tipo_frete']);
                $cidade = addslashes($_POST['cidade']);
                $estado = addslashes($_POST['estado']);
                $endereco = addslashes($_POST['endereco']);
                $bairro = addslashes($_POST['bairro']);

                $p = new Produtos_venda();
                $p->add_venda($token, $produto_id, $cep, $tipo_frete, $cidade, $estado, $endereco, $bairro);
                break;

            case 'add_venda_promocao':
                
                $token = addslashes($_POST['token']);
                $produto_id = addslashes($_POST['produto_id']);
                $cep = addslashes($_POST['cep']);
                $tipo_frete = addslashes($_POST['tipo_frete']);
                $cidade = addslashes($_POST['cidade']);
                $estado = addslashes($_POST['estado']);
                $endereco = addslashes($_POST['endereco']);
                $bairro = addslashes($_POST['bairro']);
                $id_promocao = addslashes($_POST['id_promocao']);
                $id_produto_promocao = addslashes($_POST['id_produto_promocao']);

                $p = new Produtos_venda();
                $p->add_venda_promocao($token, $produto_id, $cep, $tipo_frete, $cidade, $estado, $endereco, $bairro, $id_produto_promocao, $id_promocao);
                break;

            case 'get_compras':
                
                $token = addslashes($_POST['token']);
                
                $p = new Produtos_venda();
                $p->get($token);
                break;

            case 'get_vendas':
                
                $token = addslashes($_POST['token']);
                
                $p = new Produtos_venda();
                $p->get_vendas($token);
                break;

            case 'update_codigo':
                
                $token = addslashes($_POST['token']);
                $codigo = addslashes($_POST['codigo']);
                $id_venda = addslashes($_POST['id_venda']);
                
                $p = new Produtos_venda();
                $p->update_codigo($token, $id_venda, $codigo);
                break;

            case 'pesquisar':
                
                $token = addslashes($_POST['token']);
                $pesquisa = addslashes($_POST['pesquisa']);
                
                $p = new Produtos();
                $l = new Lojas();

                $p = $p->get_pesquisa($token, $pesquisa);
                $l = $l->get_pesquisa($token, $pesquisa);

                echo json_encode(array('r' => '5', 'produtos' => $p, 'lojas' => $l));
                break;

            case 'add_suporte':
                
                $token = addslashes($_POST['token']);
                $duvida = addslashes($_POST['duvida']);
                $assunto = addslashes($_POST['assunto']);

                $s = new Suporte();
                $s->add($token, $duvida, $assunto);
                break;

            case 'get_duvidas':
                
                $token = addslashes($_POST['token']);

                $s = new Duvidas();
                $s->get();
                break;

            default:
                echo json_encode('Erro ao selecionar ação!');
                break;
        }
    }
}

















