<!-- <!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>Delivery App</title>

    ========== ogrigatorio ============
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="assets/img/icon.png">
    
    <link rel="stylesheet" href="assets/css/plugins/bootstrap.css">
    <link rel="stylesheet" href="assets/css/plugins/font-awesome.min.css">

    <script src="assets/js/plugins/jquery-3.2.1.min.js"></script>
    <script src="assets/js/plugins/tether.min.js"></script>
    <script src="assets/js/plugins/bootstrap.min.js"></script>
    ========== fim ogrigatorio ============
    
    <link rel="stylesheet" href="assets/css/css_views/template.css">
    <link rel="stylesheet" href="assets/css/css_views/home.css">
	
	<style type="text/css">
		html, body {width: 100%; height: 100%; overflow: auto;}

		#section_1 img {display: none;}
		#section_1 {text-align: center;}
		#section_2 {display: none;}

		@media screen and (min-width: 500px) {

			#section_1 img {display: block;}			
		}

		@media screen and (min-width: 990px) {

			#section_2 {display: block;}			
		}
	</style>
  </head>
<body>
	<header class="mt_design" style="width: 100%; position: fixed; top: 0; background: #fff; z-index: 2;">
		<div class="container">
			<div class="row">
				<div class="col-12 d-flex justify-content-between align-items-center">
					<img style="width: 60px; height: 60px;" src="<?= BASE ?>assets/img/icon.png">
					<p class="mb-0" style="font-size: 1.8rem; line-height: 100%; text-transform: uppercase;">Delivery App</p>
				</div>
			</div>
		</div>
	</header>

	<section id="section_1" style="width: 100%; height: 100vh; padding-top: 70px; background: #eee;">
		<div class="container h-100">
			<div class="row h-100">
				<div class="col-12 h-100 d-flex align-items-center justify-content-center">
					<img style="width: 200px;" class="mt_design" src="<?= BASE ?>assets/img/Screenshot_2018-06-19-14-41-39.jpg">
					<div class="ml-4">
						<h1 class="mb-0" style="text-transform: uppercase;"><strong>Delivery App</strong></h1>
						<h2 style="opacity: 0.8;">Delivery</h2>
						
						<a target="_blank" href="https://play.google.com/store/apps/details?id=com.DeliveryApp.DeliveryApp">
							<button class="btn btn_2" style="border-radius: 30px;">Baixar na Google play</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="section_2" style="width: 100%; height: calc(100vh - 70px); padding-top: 50px; background: #fff;">
		<div class="container h-100">
			<div class="row h-100">
				<h2 class="mb-0 w-100" style="text-transform: uppercase; text-align: center;"><strong>características</strong></h2>
				<div class="col-12 h-100 d-flex align-items-center justify-content-center">
					<div style="height: 355.55px; padding: 1rem 0;" class="d-flex flex-column justify-content-between">
						<p class="mb-0 mb-3 d-flex justify-content-end align-items-center" style="font-weight: bold;">
							Facil de usar
							<span class="ml-3" style="display: block; border-radius: 50%; width: 50px; height: 50px; background: #f70920;"><i class="fa fa-thumbs-up d-block m-auto" style="font-size: 2rem; text-align: center; line-height: 50px; color: #fff;"></i></span>
						</p>

						<p class="mb-0 mb-3 d-flex justify-content-end align-items-center" style="font-weight: bold;">
							<span style="text-align: right;">Pagamento na entrega <br> ou no app</span>
							<span class="ml-3" style="display: block; border-radius: 50%; width: 50px; height: 50px; background: #f70920;"><i class="fa fa-credit-card d-block m-auto" style="font-size: 2rem; text-align: center; line-height: 50px; color: #fff;"></i></span>
						</p>

						<p class="mb-0 mb-3 d-flex justify-content-end align-items-center" style="font-weight: bold;">
							<span style="text-align: right;">Adicione os melhores <br> aos favoritos</span>
							<span class="ml-3" style="display: block; border-radius: 50%; width: 50px; height: 50px; background: #f70920;"><i class="fa fa-heart d-block m-auto" style="font-size: 2rem; text-align: center; line-height: 50px; color: #fff;"></i></span>
						</p>

						<p class="mb-0 d-flex justify-content-end align-items-center" style="font-weight: bold;">
							<span style="text-align: right;">Seus pedidos ficam salvos</span>
							<span class="ml-3" style="display: block; border-radius: 50%; width: 50px; height: 50px; background: #f70920;"><i class="fa fa-server d-block m-auto" style="font-size: 2rem; text-align: center; line-height: 50px; color: #fff;"></i></span>
						</p>
					</div>
					<img style="width: 200px; height: 355.55px; margin: 0 1.35rem;" src="<?= BASE ?>assets/img/Screenshot_2018-06-19-14-43-07.jpg">
					<div style="height: 355.55px; padding: 1rem 0;" class="d-flex flex-column justify-content-between">
						<p class="mb-0 mb-3 d-flex flex-row-reverse justify-content-end align-items-center" style="font-weight: bold;">
							Mote pizzas com <br> dois sabores
							<span class="mr-3" style="display: block; border-radius: 50%; width: 50px; height: 50px; background: #f70920;"><i class="fa fa-thumbs-up d-block m-auto" style="font-size: 2rem; text-align: center; line-height: 50px; color: #fff;"></i></span>
						</p>

						<p class="mb-0 mb-3 d-flex flex-row-reverse justify-content-end align-items-center" style="font-weight: bold;">
							Adiciones extras aos <br> seu pedidos
							<span class="mr-3" style="display: block; border-radius: 50%; width: 50px; height: 50px; background: #f70920;"><i class="fa fa-smile-o d-block m-auto" style="font-size: 2rem; text-align: center; line-height: 50px; color: #fff;"></i></span>
						</p>

						<p class="mb-0 mb-3 d-flex flex-row-reverse justify-content-end align-items-center" style="font-weight: bold;">
							Localização dos estabelecimentos <br> mais próximos
							<span class="mr-3" style="display: block; border-radius: 50%; width: 50px; height: 50px; background: #f70920;"><i class="fa fa-map-marker d-block m-auto" style="font-size: 2rem; text-align: center; line-height: 50px; color: #fff;"></i></span>
						</p>

						<p class="mb-0 mb-3 d-flex flex-row-reverse justify-content-end align-items-center" style="font-weight: bold;">
							Entrega imediata
							<span class="mr-3" style="display: block; border-radius: 50%; width: 50px; height: 50px; background: #f70920;"><i class="fa fa-truck d-block m-auto" style="font-size: 2rem; text-align: center; line-height: 50px; color: #fff;"></i></span>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer style="background: #eee; width: 100%; padding: 0.5rem 1rem;">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<p class="mb-0 float-left">&copy;<?php echo date("Y"); ?> Todos os direitos reservados - Delivery App
					</p>
				</div>
			</div>
		</div>
	</footer>
</body>
</html>



 -->