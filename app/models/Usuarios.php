<?php 
class Usuarios extends model {

	public function login($email, $senha) {

		$sql = "SELECT token FROM usuarios WHERE email = '$email' AND senha = '$senha'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {
			
			$token = $sql->fetch()['token'];

			echo json_encode(array('r' => '5', 'token' => $token));
		} else {
			
			echo json_encode("2");
		}
	}

	public function cadastrar($telefone, $nome, $email, $senha, $sexo) {

		$sql = "SELECT * FROM usuarios WHERE email = '$email'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() == 0) {

			$token = md5(time().rand(0,999));

			$sql = "INSERT INTO usuarios SET img_perfil = 'user_m.png', sexo = '$sexo', telefone = '$telefone', email = '$email', nome = '$nome', token = '$token', senha = '$senha', data = NOW()";
			$this->db->query($sql);

			echo json_encode(array('r' => '5', 'token' => $token));
		} else {

			echo json_encode('2');
		}
	}

	public function set_code($email, $code) {

		$sql = "SELECT * FROM usuarios WHERE email = '$email'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {
			$sql = $sql->fetch();
			$id_usuario = $sql['id'];

			$c = new Config();
			$c = $c->get_dados_config();

			$sql = "SELECT * FROM usuario_recuperar_senha WHERE id_usuario = '$id_usuario'";
			$sql = $this->db->query($sql);

			if($sql->rowCount() == 0) {

				$sql = "INSERT INTO usuario_recuperar_senha SET id_usuario = '$id_usuario', codigo = '$code', data_cadastro = NOW()";
				$this->db->query($sql);
			} else {

				$sql = "UPDATE usuario_recuperar_senha SET codigo = '$code', data_cadastro = NOW() WHERE id_usuario = '$id_usuario'";
				$this->db->query($sql);
			}

			if(ENVIRONMENT != 'development') {

				$c = new Config();
                $dados_config = $c->get_dados_config();

                $html = "Código: ".$code;

                $headers = 'From: '.$dados_config['email_contato'].''."\r\n";
                $headers .= 'Reply-To: '.$dados_config['email_contato']."\r\n";
                $headers .= 'X-Mailer: PHP/'.phpversion();

                mail($email, "Código para mudar sua senha", $html, $headers);
			}

			echo json_encode("5");
		} else {

			echo json_encode("2");exit;
		}
	}

	public function consult_code($email, $codigo) {
		$sql = "SELECT * FROM usuarios WHERE email = '$email'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {
			$sql = $sql->fetch();
			$id_usuario = $sql['id'];

			$sql = "SELECT * FROM usuario_recuperar_senha WHERE id_usuario = '$id_usuario' AND codigo = '$codigo'";
			$sql = $this->db->query($sql);

			if($sql->rowCount() > 0) {

				echo json_encode("5");
			} else {

				echo json_encode("2");
			}
		}
	}

	public function update_senha_com_code($email, $codigo, $senha) {
		$sql = "SELECT * FROM usuarios WHERE email = '$email'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();
			$token = $sql['token'];

			$sql = "SELECT * FROM usuario_recuperar_senha WHERE id_usuario = '".$sql['id']."' AND codigo = '$codigo'";
			$sql = $this->db->query($sql);

			if($sql->rowCount() > 0) {

				$sql = "UPDATE usuarios SET senha = '$senha' WHERE email = '$email'";
				$this->db->query($sql);

				echo json_encode(array("r" => "5", 'token' => $token));
			} else {

				echo json_encode('3');
			}
		} else {
			echo json_encode("2"); 
		}
	}

	public function get_dados($token) {

		$tot = 0;

		$sql = "SELECT * FROM usuarios WHERE token = '$token'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {
			$usuario = $sql->fetch(PDO::FETCH_ASSOC);

			$sql = "SELECT * FROM lojas WHERE id_usuario = '".$usuario['id']."'";
			$sql = $this->db->query($sql);

			if($sql->rowCount() > 0) {
				$id_loja = $sql->fetch(PDO::FETCH_ASSOC)['id'];

				$sql = "SELECT count(id) as tot FROM produtos WHERE id_loja = '$id_loja'";
				$sql = $this->db->query($sql);

				if($sql->rowCount() > 0) {

					$tot = $sql->fetch()['tot'];
				}
			}

			$usuario['tot_produtos'] = $tot;

			echo json_encode(array('r' => '5', 'dados' => $usuario));
		} else {

			echo json_encode("2");
		}
	}

	public function update_senha($senha, $token) {

		$sql = "UPDATE usuarios SET senha = '$senha' WHERE token = '$token'";
		$this->db->query($sql);
	}

	public function update($telefone, $nome, $email, $token, $sexo) {

		$sql = "SELECT * FROM usuarios WHERE email = '$email' AND token != '$token'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() == 0) {

			$sql = "UPDATE usuarios SET telefone = '$telefone', email = '$email', sexo = '$sexo', nome = '$nome' WHERE token = '$token'";
			$this->db->query($sql);

			echo json_encode('5');
		} else {

			echo json_encode('2');
		}
	}

	public function update_push_app_key($push_app_key, $token) {

		$sql = "UPDATE usuarios SET push_app_key = '$push_app_key' WHERE token = '$token'";
		$this->db->query($sql);

		echo json_encode('5');
	}

	public function verificar_existencia_endereco_loja($token) {

		$sql = "SELECT id FROM usuarios WHERE token = '$token'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$id_usuario = $sql->fetch()['id'];

			$status = 'nao';

			$sql = "SELECT * FROM lojas WHERE id_usuario = '$id_usuario'";
			$sql = $this->db->query($sql);

			if($sql->rowCount() > 0) {

				$l = $sql->fetch();

				if($l['endereco'] != '' && $l['cidade'] != '' && $l['cep'] != '' && $l['estado'] != '') {

					$status = 'sim';
				}
			}

			echo json_encode(array('r' => '5', 'status' => $status));
		}
	}
}












