<?php 
class Produtos_venda extends model {

    public function add_venda($token, $produto_id, $cep, $tipo_frete, $cidade, $estado, $endereco, $bairro) {

        $sql = "SELECT * FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $u = $sql->fetch();

            $sql = "SELECT * FROM produtos WHERE id = '$produto_id'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $produto = $sql->fetch();
                $id_loja = $produto['id_loja'];

                $sql = "SELECT * FROM lojas WHERE id = '$id_loja'";
                $sql = $this->db->query($sql);

                if($sql->rowCount() > 0) {

                    $loja = $sql->fetch();

                    if($tipo_frete == 'SEDEX') {

                        $tipoFrete = '40010';
                    } else {

                        $tipoFrete = '41106';
                    }

                    $correios = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=".$loja['cep']."&sCepDestino=".$cep."&nVlPeso=".$produto['peso']."&nCdFormato=1&nVlComprimento=".$produto['comprimento']."&nVlAltura=".$produto['altura']."&nVlLargura=".$produto['largura']."&sCdMaoPropria=n&nVlValorDeclarado=0&sCdAvisoRecebimento=n&nCdServico=".$tipoFrete."&nVlDiametro=0&StrRetorno=xml";

                      $xml = simplexml_load_file($correios);

                      $_arr_ = array();
                      if($xml->cServico->Erro == '0'):
                        
                        $frete = str_replace(',', '.', $xml->cServico->Valor);
                        $prazo_entrega = $xml->cServico->PrazoEntrega .' Dias';

                        $preco = $produto['preco'] + $frete;

                        $sql = "INSERT INTO produtos_venda SET id_usuario = '".$u['id']."', id_loja = '$id_loja', id_produto = '$produto_id', endereco = '$endereco', bairro = '$bairro', cidade = '$cidade', estado = '$estado', cep = '$cep', produto_preco = '".$produto['preco']."', produto_nome = '".$produto['nome']."', frete = '$frete', frete_tipo = '$tipo_frete', data = NOW()";
                        $this->db->query($sql);

                        $id_pagamento = $this->db->lastInsertId();

                        $dados_api = new Config();
                        $dados_api = $dados_api->get_dados_config();

                        $mp = new MP($dados_api['MP_AppID'], $dados_api['MP_SecretKey']);
                        $mp->sandbox_mode(false);

                        $data = array(
                            'items' => array(),
                            'back_urls' => array(
                                'success' => BASE.'mp/success_mp',
                                'panding' => BASE.'mp/analise_mp',
                                'failure' => BASE.'mp/cancelado'
                            ),
                            'notification_url' => BASE.'mp/notificacao_mp',
                            'auto_return' => 'all',
                            'external_reference' => $id_pagamento
                        );

                        $data['items'][] = array(
                            'title' => "Pagamento pressaofarma",
                            'quantity' => 1,
                            'currency_id' => 'BRL',
                            'unit_price' => floatval($preco)
                        );

                        $link = $mp->create_preference($data);

                        if($link['status'] == '201') {

                            $sql = "UPDATE produtos_venda SET link_pg = '".$link['response']['init_point']."' WHERE id = '$id_pagamento'";
                            $this->db->query($sql);

                            echo json_encode(array('r' => '5', 'link' => $link['response']['init_point']));

                            exit;
                        } else {

                            echo json_encode('3');
                        }
                      else:
                         
                         echo json_encode('2');
                      endif;
                }
            }
        }
    }

    public function add_venda_promocao($token, $produto_id, $cep, $tipo_frete, $cidade, $estado, $endereco, $bairro, $id_produto_promocao, $id_promocao) {

        $sql = "SELECT * FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $u = $sql->fetch();

            $desconto = 0;

            $sql = "SELECT * FROM promocoes_produtos WHERE id = '$id_produto_promocao'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $desconto = $sql->fetch()['desconto'];
            }

            $sql = "SELECT * FROM produtos WHERE id = '$produto_id'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $produto = $sql->fetch();
                $id_loja = $produto['id_loja'];

                $sql = "SELECT * FROM lojas WHERE id = '$id_loja'";
                $sql = $this->db->query($sql);

                if($sql->rowCount() > 0) {

                    $loja = $sql->fetch();

                    if($tipo_frete == 'SEDEX') {

                        $tipoFrete = '40010';
                    } else {

                        $tipoFrete = '41106';
                    }

                    $correios = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=".$loja['cep']."&sCepDestino=".$cep."&nVlPeso=".$produto['peso']."&nCdFormato=1&nVlComprimento=".$produto['comprimento']."&nVlAltura=".$produto['altura']."&nVlLargura=".$produto['largura']."&sCdMaoPropria=n&nVlValorDeclarado=0&sCdAvisoRecebimento=n&nCdServico=".$tipoFrete."&nVlDiametro=0&StrRetorno=xml";

                      $xml = simplexml_load_file($correios);

                      $_arr_ = array();
                      if($xml->cServico->Erro == '0'):
                        
                        $frete = str_replace(',', '.', $xml->cServico->Valor);
                        $prazo_entrega = $xml->cServico->PrazoEntrega .' Dias';

                        $produto_preco = $produto['preco'];

                        $produto_preco = $produto_preco - ($produto_preco * $desconto / 100);

                        $preco = $produto_preco + $frete;

                        $sql = "INSERT INTO produtos_venda SET id_usuario = '".$u['id']."', id_promocao = '$id_promocao', id_promocao_produto = '$id_produto_promocao', desconto = '$desconto', id_loja = '$id_loja', id_produto = '$produto_id', endereco = '$endereco', bairro = '$bairro', cidade = '$cidade', estado = '$estado', cep = '$cep', produto_preco = '".$produto_preco."', produto_nome = '".$produto['nome']."', frete = '$frete', frete_tipo = '$tipo_frete', data = NOW()";
                        $this->db->query($sql);

                        $id_pagamento = $this->db->lastInsertId();

                        $dados_api = new Config();
                        $dados_api = $dados_api->get_dados_config();

                        $mp = new MP($dados_api['MP_AppID'], $dados_api['MP_SecretKey']);
                        $mp->sandbox_mode(false);

                        $data = array(
                            'items' => array(),
                            'back_urls' => array(
                                'success' => BASE.'mp/success_mp',
                                'panding' => BASE.'mp/analise_mp',
                                'failure' => BASE.'mp/cancelado'
                            ),
                            'notification_url' => BASE.'mp/notificacao_mp',
                            'auto_return' => 'all',
                            'external_reference' => $id_pagamento
                        );

                        $data['items'][] = array(
                            'title' => "Pagamento pressaofarma",
                            'quantity' => 1,
                            'currency_id' => 'BRL',
                            'unit_price' => floatval($preco)
                        );

                        $link = $mp->create_preference($data);

                        if($link['status'] == '201') {

                            $sql = "UPDATE produtos_venda SET link_pg = '".$link['response']['init_point']."' WHERE id = '$id_pagamento'";
                            $this->db->query($sql);

                            echo json_encode(array('r' => '5', 'link' => $link['response']['init_point']));

                            exit;
                        } else {

                            echo json_encode('3');
                        }
                      else:
                         
                         echo json_encode('2');
                      endif;
                }
            }
        }
    }

    public function aprovar_mp($ref) {

        $sql = "UPDATE produtos_venda SET status_pg = '2' WHERE id = '$ref'";
        $this->db->query($sql);

        $sql = "SELECT * FROM produtos_venda WHERE id = '$ref'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $sql = $sql->fetch();

            $sql = "SELECT * FROM usuarios WHERE id = '".$sql['id_usuario']."'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $u = $sql->fetch();

                $id_usuario = $u['id'];

                $data_ = array('tipo' => 'pagamento', 'msg' => 'O Status do seu agendamento mudou', 'titulo' => 'pressaofarma', 'img' => '');

                $push = new push_notificationHelpers();
                $push->sendMessage('O Status do seu agendamento mudou', $data_, '', array($u['push_app_key']));


                $sql_2 = "INSERT INTO avisos SET id_usuario = '$id_usuario', titulo = 'pressaofarma', msg = 'O Status do seu agendamento mudou', data = NOW()";
                $this->db->query($sql_2);
            }
        }
    }

    public function cancelar_mp($ref) {

        $sql = "UPDATE produtos_venda SET status_pg = '1' WHERE id = '$ref'";
        $this->db->query($sql);

        $sql = "SELECT * FROM produtos_venda WHERE id = '$ref'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $sql = $sql->fetch();

            $sql = "SELECT * FROM usuarios WHERE id = '".$sql['id_usuario']."'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $u = $sql->fetch();

                $id_usuario = $u['id'];

                $data_ = array('tipo' => 'pagamento', 'msg' => 'O Status do seu agendamento mudou', 'titulo' => 'pressaofarma', 'img' => '');

                $push = new push_notificationHelpers();
                $push->sendMessage('O Status do seu agendamento mudou', $data_, '', array($u['push_app_key']));


                $sql_2 = "INSERT INTO avisos SET id_usuario = '$id_usuario', titulo = 'pressaofarma', msg = 'O Status do seu agendamento mudou', data = NOW()";
                $this->db->query($sql_2);
            }
        }
    }

    public function get($token) {

        $sql = "SELECT id FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $id_usuario = $sql->fetch()['id'];

            $sql = "SELECT * FROM produtos_venda WHERE id_usuario = '$id_usuario' ORDER BY data DESC";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $sql = $sql->fetchAll(PDO::FETCH_ASSOC);

                foreach ($sql as $key => $value) {

                    $sql[$key]['data'] = (new DateTime($value['data']))->format('d/m/Y h:m:s');
                    
                    $s = "SELECT img FROM produtos_img WHERE id_produto = '".$value['id_produto']."' LIMIT 1";
                    $s = $this->db->query($s);

                    if($s->rowCount() > 0) {

                        $sql[$key]['img'] = $s->fetch(PDO::FETCH_ASSOC)['img'];
                    }
                }
            } else {

                $sql = array();
            }

            echo json_encode(array('r' => '5', 'dados' => $sql));
        }
    }

    public function get_vendas($token) {

        $sql = "SELECT id FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $id_usuario = $sql->fetch()['id'];

            $sql = "SELECT id FROM lojas WHERE id_usuario = '$id_usuario'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $id_loja = $sql->fetch()['id'];
            }

            $sql = "SELECT * FROM produtos_venda WHERE id_loja = '$id_loja' ORDER BY data DESC";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $sql = $sql->fetchAll(PDO::FETCH_ASSOC);

                foreach ($sql as $key => $value) {

                    $sql[$key]['data'] = (new DateTime($value['data']))->format('d/m/Y h:m:s');
                    
                    $s = "SELECT img FROM produtos_img WHERE id_produto = '".$value['id_produto']."' LIMIT 1";
                    $s = $this->db->query($s);

                    if($s->rowCount() > 0) {

                        $sql[$key]['img'] = $s->fetch(PDO::FETCH_ASSOC)['img'];
                    }

                    $s = "SELECT nome, email, telefone FROM usuarios WHERE id = '".$value['id_usuario']."'";
                    $s = $this->db->query($s);

                    if($s->rowCount() > 0) {

                        $sql[$key]['usuario'] = $s->fetch(PDO::FETCH_ASSOC);
                    }
                }
            } else {

                $sql = array();
            }

            echo json_encode(array('r' => '5', 'dados' => $sql));
        }
    }

    public function update_codigo($token, $id_venda, $codigo) {

        $sql = "SELECT id, push_app_key FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $sql = $sql->fetch();
            $id_usuario = $sql['id'];
            $push_app_key = $sql['push_app_key'];

            $sql = "UPDATE produtos_venda SET codigo_rastreio = '$codigo' WHERE id = '$id_venda'";
            $this->db->query($sql);

            $data_ = array('tipo' => 'pagamento', 'msg' => 'Sua compra foi enviada. Código de ratreio: '.$codigo, 'titulo' => 'pressaofarma', 'img' => '');

            $push = new push_notificationHelpers();
            $push->sendMessage('Sua compra foi enviada. Código de ratreio: '.$codigo, $data_, '', array($push_app_key));

            $sql_2 = "INSERT INTO avisos SET id_usuario = '$id_usuario', titulo = 'pressaofarma', msg = 'Sua compra foi enviada. Código de ratreio: ".$codigo."', data = NOW()";
            $this->db->query($sql_2);

            echo json_encode('5');
        }
    }
}