<?php 
class Produtos_favoritos extends model {

    public function get_usuario_favoritos($token) {

        $sql = "SELECT id FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $id_usuario = $sql->fetch()['id'];

            $f = array();

            $sql = "SELECT * FROM produtos_favoritos WHERE id_usuario = '$id_usuario'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $f = array_column($sql->fetchAll(PDO::FETCH_ASSOC), 'id_produto');
            }

            return $f;
        } else {

            return array();
        }
    }

    public function add($token, $id_produto) {

        $sql = "SELECT * FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $id_usuario = $sql->fetch()['id'];

            $sql = "SELECT * FROM produtos_favoritos WHERE id_produto = '$id_produto' AND id_usuario = '$id_usuario'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $sql = "DELETE FROM produtos_favoritos WHERE id_produto = '$id_produto' AND id_usuario = '$id_usuario'";
                $this->db->query($sql);
            } else {

                $sql = "SELECT id_loja FROM produtos WHERE id = '$id_produto'";
                $sql = $this->db->query($sql);

                if($sql->rowCount() > 0) {

                    $id_loja = $sql->fetch()['id_loja'];

                    $sql = "INSERT INTO produtos_favoritos SET  id_produto = '$id_produto', id_usuario = '$id_usuario', id_loja = '$id_loja', data = NOW()";
                    $this->db->query($sql);
                }
            }

            echo json_encode('5');
        } else {

            echo json_encode('1');
        }
    }

    public function get_favoritos($token) {

        $f = array();

        $sql = "SELECT id FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $id_usuario = $sql->fetch()['id'];

            $sql = "SELECT * FROM produtos_favoritos WHERE id_usuario = '$id_usuario'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $ids = implode(',', array_column($sql->fetchAll(PDO::FETCH_ASSOC), 'id_produto'));

                $sql = "SELECT * FROM produtos WHERE id IN (".$ids.")";
                $sql = $this->db->query($sql);

                if($sql->rowCount() > 0) {

                    $f = $sql->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($f as $key => $value) {
                        
                        $sql = "SELECT * FROM produtos_img WHERE id_produto = '".$value['id']."'";
                        $sql = $this->db->query($sql);

                        if($sql->rowCount() > 0) {

                            $f[$key]['img'] = $sql->fetch()['img'];
                        }
                    }
                }
            }
        }

        echo json_encode(array('r' => '5', 'dados' => $f));
    }

    public function update($token, $cep, $numero, $endereco, $bairro, $ponto_referencia, $complemento, $estado, $cidade, $id) {

        $sql = "SELECT id FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $id_usuario = $sql->fetch()['id'];

            $sql = "UPDATE favoritos SET cep = '$cep', numero = '$numero', endereco = '$endereco', bairro = '$bairro', ponto_referencia = '$ponto_referencia', complemento = '$complemento', estado = '$estado', cidade = '$cidade' WHERE id = '$id' AND id_usuario = '$id_usuario'";
            $this->db->query($sql);

            echo json_encode('5');
        }
    }
}