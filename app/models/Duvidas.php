<?php 
class Duvidas extends model {

    public function get() {

        $sql = "SELECT * FROM duvidas";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $c = $sql->fetchAll(PDO::FETCH_ASSOC);
        } else {

            $c = array();
        }

        echo json_encode(array('r' => '5', 'dados' => $c));
    }
}