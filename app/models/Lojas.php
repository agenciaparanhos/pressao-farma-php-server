<?php 
class Lojas extends model {

    public function get_minha_loja($token) {

        $l = '';

        $sql = "SELECT id FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $id_usuario = $sql->fetch()['id'];

            $sql = "SELECT * FROM lojas WHERE id_usuario = '$id_usuario'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $l = $sql->fetch(PDO::FETCH_ASSOC);

                $sql = "SELECT * FROM produtos WHERE id_loja = '".$l['id']."'";
                $sql = $this->db->query($sql);

                if($sql->rowCount() > 0) {

                    $p = $sql->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($p as $key => $value) {
                        
                        $sql = $sql = "SELECT * FROM produtos_img WHERE id_produto = '".$value['id']."'";
                        $sql = $this->db->query($sql);

                        if($sql->rowCount() > 0) {

                            $imgs = $sql->fetchAll(PDO::FETCH_ASSOC);

                            $p[$key]['img'] = $imgs[0]['img'];
                            $p[$key]['imgs'] = $imgs;
                        }

                        $sql = $sql = "SELECT nome FROM categorias WHERE id = '".$value['id_categoria']."'";
                        $sql = $this->db->query($sql);

                        if($sql->rowCount() > 0) {

                            $p[$key]['categoria'] = $sql->fetch()['nome'];
                        }

                        $sql = $sql = "SELECT nome FROM subcategorias WHERE id = '".$value['id_subcategoria']."'";
                        $sql = $this->db->query($sql);

                        if($sql->rowCount() > 0) {

                            $p[$key]['subcategoria'] = $sql->fetch()['nome'];
                        } else {

                            $p[$key]['subcategoria'] = '';
                        }
                    }

                    $l['produtos'] = $p;
                } else {

                    $l['produtos'] = array();
                }

                $sql = "SELECT count(id) as tot FROM loja_seguidores WHERE id_loja = '".$l['id']."'";
                $sql = $this->db->query($sql);

                if($sql->rowCount() > 0) {

                    $l['seguidores'] = $sql->fetch()['tot'];
                }

                $sql = "SELECT count(id) as tot FROM produtos_venda WHERE id_loja = '".$l['id']."'";
                $sql = $this->db->query($sql);

                if($sql->rowCount() > 0) {

                    $l['vendas'] = $sql->fetch()['tot'];
                }
            }
        }

        echo json_encode(array("r" => '5', 'dados' => $l));
    }

    public function get_minha_loja_to_edit($token) {

        $sql = "SELECT id FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $id_usuario = $sql->fetch()['id'];

            $sql = "SELECT nome, logo, banner, cep, estado, cidade, endereco, bairro FROM lojas WHERE id_usuario = '$id_usuario'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $l = $sql->fetch(PDO::FETCH_ASSOC);
            } else {

                $l = array();
            }

            echo json_encode(array('r' => '5', 'dados' => $l));
        }
    }

    public function update_logo($logo_name, $token) {

        $sql = "SELECT id FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $id_usuario = $sql->fetch()['id'];

            $sql = "SELECT * FROM lojas WHERE id_usuario = '$id_usuario'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $loja = $sql->fetch(PDO::FETCH_ASSOC);

                if($loja['logo'] != 'logo_loja.png') {

                    @unlink('../assets/img/lojas/'.$loja['logo']);
                }

                $sql = "UPDATE lojas SET logo = '$logo_name' WHERE id_usuario = '$id_usuario'";
                $this->db->query($sql);
            }
        }
    }

    public function update_banner($banner_name, $token) {

        $sql = "SELECT id FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $id_usuario = $sql->fetch()['id'];

            $sql = "SELECT * FROM lojas WHERE id_usuario = '$id_usuario'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $loja = $sql->fetch(PDO::FETCH_ASSOC);

                if($loja['banner'] != 'banner.png') {

                    @unlink('../assets/img/lojas/'.$loja['banner']);
                }
            }

            $sql = "UPDATE lojas SET banner = '$banner_name' WHERE id_usuario = '$id_usuario'";
            $this->db->query($sql);
        }
    }

    public function update_nome($nome, $cep, $bairro, $endereco, $cidade, $estado, $token) {

        $sql = "SELECT id FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $id_usuario = $sql->fetch()['id'];

            $sql = "SELECT * FROM lojas WHERE nome = '$nome' AND id_usuario != '$id_usuario' ";
            $sql = $this->db->query($sql);

            if($sql->rowCount() == 0) {

                $sql = "UPDATE lojas SET nome = '$nome', cep = '$cep', endereco = '$endereco', bairro = '$bairro', cidade = '$cidade', estado = '$estado' WHERE id_usuario = '$id_usuario'";
                $this->db->query($sql);

                echo json_encode('5');
            } else {

                echo json_encode('2');
            }
        }
    }

    public function seguir($token, $id) {

        $sql = "SELECT id FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $id_usuario = $sql->fetch()['id'];

            $sql = "SELECT * FROM lojas WHERE id = '$id'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $sql = $sql->fetch();
                $id_loja = $sql['id'];

                if($sql['id_usuario'] == $id_usuario) {

                    echo json_encode('2');
                } else {

                    $sql = "SELECT * FROM loja_seguidores WHERE id_loja = '$id_loja' AND id_usuario = '$id_usuario'";
                    $sql = $this->db->query($sql);

                    if($sql->rowCount() > 0) {

                        $sql = "DELETE FROM loja_seguidores WHERE id_loja = '$id_loja' AND id_usuario = '$id_usuario'";
                        $this->db->query($sql);

                        echo json_encode('4');
                    } else {

                        $sql = "INSERT INTO loja_seguidores SET id_loja = '$id_loja', id_usuario = '$id_usuario', data = NOW()";
                        $this->db->query($sql);

                        echo json_encode('5');
                    }
                }
            }
        }
    }

    public function get_loja($token, $id_loja) {

        $l = '';
        $id_usuario = 0;

        $sql = "SELECT id FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $id_usuario = $sql->fetch()['id'];
        }

        $sql = "SELECT * FROM lojas WHERE id = '$id_loja'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $l = $sql->fetch(PDO::FETCH_ASSOC);

            $l['seguindo'] = 'nao';

            $sql = "SELECT id FROM loja_seguidores WHERE id_usuario = '$id_usuario' AND id_loja = '".$l['id']."'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $l['seguindo'] = 'sim';
            }

            $sql = "SELECT * FROM produtos WHERE id_loja = '".$l['id']."'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $p = $sql->fetchAll(PDO::FETCH_ASSOC);

                foreach ($p as $key => $value) {
                    
                    $sql = $sql = "SELECT * FROM produtos_img WHERE id_produto = '".$value['id']."'";
                    $sql = $this->db->query($sql);

                    if($sql->rowCount() > 0) {

                        $imgs = $sql->fetchAll(PDO::FETCH_ASSOC);

                        $p[$key]['img'] = $imgs[0]['img'];
                    }
                }

                $l['produtos'] = $p;
            } else {

                $l['produtos'] = array();
            }

            $sql = "SELECT count(id) as tot FROM loja_seguidores WHERE id_loja = '".$l['id']."'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $l['seguidores'] = $sql->fetch()['tot'];
            }

            $sql = "SELECT count(id) as tot FROM produtos_venda WHERE id_loja = '".$l['id']."'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $l['vendas'] = $sql->fetch()['tot'];
            }
        }

        echo json_encode(array("r" => '5', 'dados' => $l));
    }

    public function add($token, $estado, $cidade, $bairro, $endereco, $cep) {

        $sql = "SELECT id, nome, sexo FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $sql = $sql->fetch();
            $id_usuario = $sql['id'];

            if($sql['sexo'] == 'Homem') {

                $nome = 'do '.$sql['nome'];
            } else {

                $nome = 'da '.$sql['nome'];
            }
            
            $sql = "SELECT * FROM lojas WHERE id_usuario = '$id_usuario'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() == 0) {

                $sql = "INSERT INTO lojas SET id_usuario = '$id_usuario', banner = 'banner.png', logo = 'logo_loja.png', estado = '$estado', cidade = '$cidade', bairro = '$bairro', endereco = '$endereco', cep = '$cep', nome = '$nome', data = NOW()";
                $this->db->query($sql);
            }

            echo json_encode('5');
        }
    }

    public function calcular_frete($token, $produto_id, $cep, $tipo_frete) {

        $sql = "SELECT * FROM produtos WHERE id = '$produto_id'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $produto = $sql->fetch();
            $id_loja = $produto['id_loja'];

            $sql = "SELECT * FROM lojas WHERE id = '$id_loja'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $loja = $sql->fetch();

                if($tipo_frete == 'SEDEX') {

                    $tipo_frete = '40010';
                } else {

                    $tipo_frete = '41106';
                }

                $correios = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=".$loja['cep']."&sCepDestino=".$cep."&nVlPeso=".$produto['peso']."&nCdFormato=1&nVlComprimento=".$produto['comprimento']."&nVlAltura=".$produto['altura']."&nVlLargura=".$produto['largura']."&sCdMaoPropria=n&nVlValorDeclarado=0&sCdAvisoRecebimento=n&nCdServico=".$tipo_frete."&nVlDiametro=0&StrRetorno=xml";

                  $xml = simplexml_load_file($correios);

                  $_arr_ = array();
                  if($xml->cServico->Erro == '0'):
                     $_arr_['codigo'] = $xml->cServico->Codigo ;
                     $_arr_['valor'] = 'R$ '.$xml->cServico->Valor;
                     $_arr_['prazo'] = $xml->cServico->PrazoEntrega .' Dias' ;
                     // return $xml->cServico->Valor;

                     echo json_encode(array('r' => '5', 'dados' => $_arr_));
                  else:
                     
                     echo json_encode('2');
                  endif;
            }
        }
    }

    public function get_pesquisa($token, $pesquisa) {

        $sql = "SELECT * FROM lojas WHERE nome LIKE '%$pesquisa%' LIMIT 5";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $lojas = $sql->fetchAll(PDO::FETCH_ASSOC);
        } else {

            $lojas = array();
        }

        return $lojas;
    }
}













