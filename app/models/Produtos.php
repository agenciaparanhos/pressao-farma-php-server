<?php 
class Produtos extends model {

    public function add($token, $nome, $tamanho, $condicao, $marca, $descricao, $preco, $nomes, $categoria, $subcategoria, $altura, $comprimento, $largura, $peso) {

        $sql = "SELECT * FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $u = $sql->fetch();

            $sql = "SELECT * FROM lojas WHERE id_usuario = '".$u['id']."'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $id_loja = $sql->fetch()['id'];
            }

            $sql = "SELECT * FROM categorias WHERE nome = '$categoria'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $id_categoria = $sql->fetch()['id'];

                $id_subcategoria = 0;

                $sql = "SELECT id FROM subcategorias WHERE nome = '$subcategoria'";
                $sql = $this->db->query($sql);

                if($sql->rowCount() > 0) {

                    $id_subcategoria = $sql->fetch()['id'];
                }

                $sql = "INSERT INTO produtos SET nome = '$nome', tamanho = '$tamanho', condicao = '$condicao', marca = '$marca', descricao = '$descricao', preco = '$preco', id_categoria = '$id_categoria', altura = '$altura', comprimento = '$comprimento', largura = '$largura', peso = '$peso', id_loja = '$id_loja', id_subcategoria = '$id_subcategoria', data = NOW()";
                $this->db->query($sql);

                $id_produto = $this->db->lastInsertId();

                foreach ($nomes as $key => $value) {
                    
                    $sql = "INSERT INTO produtos_img SET id_produto = '$id_produto', img = '$value'";
                    $this->db->query($sql);
                }
            }

            echo json_encode('5');
        }
    }

    public function update($token, $nome, $tamanho, $condicao, $marca, $descricao, $preco, $nomes, $categoria, $subcategoria, $id, $old_imgs, $altura, $comprimento, $largura, $peso) {

        $sql = "SELECT * FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $u = $sql->fetch();

            $sql = "SELECT * FROM categorias WHERE nome = '$categoria'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $id_categoria = $sql->fetch()['id'];

                $id_subcategoria = 0;

                $sql = "SELECT id FROM subcategorias WHERE nome = '$subcategoria'";
                $sql = $this->db->query($sql);

                if($sql->rowCount() > 0) {

                    $id_subcategoria = $sql->fetch()['id'];
                }

                $sql = "UPDATE produtos SET nome = '$nome', tamanho = '$tamanho', altura = '$altura', comprimento = '$comprimento', largura = '$largura', peso = '$peso', condicao = '$condicao', marca = '$marca', descricao = '$descricao', id_subcategoria = '$id_subcategoria', preco = '$preco', id_categoria = '$id_categoria' WHERE id = '$id'";
                $this->db->query($sql);

                $id_produto = $id;

                $sql = "DELETE FROM produtos_img WHERE id_produto = '$id_produto' AND img NOT IN (".$old_imgs.")";
                $this->db->query($sql);

                foreach ($nomes as $key => $value) {
                    
                    $sql = "INSERT INTO produtos_img SET id_produto = '$id_produto', img = '$value'";
                    $this->db->query($sql);
                }
            }

            echo json_encode('5');
        }
    }

    public function deletar($token, $id) {

        $sql = "SELECT * FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $u = $sql->fetch();

            $sql = "SELECT * FROM produtos_img WHERE id = '$id'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $p = $sql->fetchAll();

                foreach ($p as $key => $value) {
                    
                    @unlink('../assets/img/produtos/'.$value['img']);
                }

                $sql = "DELETE FROM produtos_img WHERE id_produto = '$id'";
                $this->db->query($sql);
            }

            $sql = "DELETE FROM produtos WHERE id = '$id'";
            $this->db->query($sql);

            echo json_encode('5');
        }
    }

    public function get_produto($token, $id) {

        $u = array();

        $sql = "SELECT * FROM usuarios WHERE token = '$token'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $u = $sql->fetch(PDO::FETCH_ASSOC);
        }

        $sql = "SELECT * FROM produtos WHERE id = '$id'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $p = $sql->fetch(PDO::FETCH_ASSOC);

            $sql = "SELECT * FROM produtos_img WHERE id_produto = '$id'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $p['imgs'] = $sql->fetchAll(PDO::FETCH_ASSOC);
            }

            $sql = "SELECT * FROM lojas WHERE id = '".$p['id_loja']."'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $loja = $sql->fetch(PDO::FETCH_ASSOC);

                unset($loja['senha']);

                $loja['seguindo'] = 'nao';

                if($u != array()) {

                    if($loja['id_usuario'] == $u['id']) {

                        $u['minha_loja'] = 'sim';
                    } else {

                        $u['minha_loja'] = 'nao';
                    }

                    $sql = "SELECT id FROM loja_seguidores WHERE id_usuario = '".$u['id']."' AND id_loja = '".$loja['id']."'";
                    $sql = $this->db->query($sql);

                    if($sql->rowCount() > 0) {

                        $loja['seguindo'] = 'sim';
                    }
                }

                $p['loja'] = $loja;
            }

            $nota = 0;

            $sql = "SELECT count(id) as count, SUM(nota) as sum FROM produtos_avaliacoes WHERE id_produto = '$id'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $sql = $sql->fetch();

                if($sql['count'] > 0) {

                    $nota = $sql['sum'] / $sql['count'];
                }
            }

            $p['nota'] = round($nota);
        }

        echo json_encode(array('r' => '5', 'dados' => $p));
    }

    public function get_pesquisa($token, $pesquisa) {

        $sql = "SELECT * FROM produtos WHERE nome LIKE '%$pesquisa%' OR descricao LIKE '%$pesquisa%' LIMIT 10";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $produtos = $sql->fetchAll(PDO::FETCH_ASSOC);
        } else {

            $produtos = array();
        }

        return $produtos;
    }
}













