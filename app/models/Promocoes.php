<?php 
class Promocoes extends model {

    public function get() {

        $sql = "SELECT * FROM promocoes";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $promocoes = $sql->fetchAll(PDO::FETCH_ASSOC);

            foreach ($promocoes as $key => $value) {

                $promocoes[$key]['cor'] = '0xff'.implode('', explode('#', $value['cor']));

                $sql = "SELECT * FROM promocoes_produtos WHERE id_promocao = '".$value['id']."'";
                $sql = $this->db->query($sql);

                if($sql->rowCount() > 0) {

                    $sql = $sql->fetchAll(PDO::FETCH_ASSOC);

                    $produtos = array();

                    foreach ($sql as $key_ => $value_) {
                        
                        $sql = "SELECT * FROM produtos WHERE id = '".$value_['id_produto']."'";
                        $sql = $this->db->query($sql);

                        if($sql->rowCount() > 0) {

                            $p = $sql->fetch(PDO::FETCH_ASSOC);
                                
                            $sql = "SELECT * FROM produtos_img WHERE id_produto = '".$p['id']."' LIMIT 1";
                            $sql = $this->db->query($sql);

                            if($sql->rowCount() > 0) {

                                $p['img'] = $sql->fetch()['img'];
                            }

                            $p['desconto'] = $value_['desconto'];
                            $p['id_produto_promocao'] = $value_['id'];

                            $produtos[] = $p;
                        }
                    }

                    if(count($produtos) > 0) {

                        $promocoes[$key]['produtos'] = $produtos;
                    } else {

                        unset($promocoes[$key]);
                    }
                } else {

                    unset($promocoes[$key]);
                }
            }

            $p = array();

            foreach ($promocoes as $key => $value) {
                
                $p[] = $value;
            }

            $promocoes = $p;
        } else {

            $promocoes = array();
        }

        return $promocoes;
    }
}













