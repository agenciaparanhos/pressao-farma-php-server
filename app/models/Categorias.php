<?php 
class Categorias extends model {

    public function get() {

        $sql = "SELECT * FROM categorias";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $c = $sql->fetchAll(PDO::FETCH_ASSOC);

            foreach ($c as $key => $value) {

                $sql = "SELECT * FROM subcategorias WHERE id_categoria = '".$value['id']."'";
                $sql = $this->db->query($sql);

                if($sql->rowCount() > 0) {

                    $c[$key]['subcategorias'] =  $sql->fetchAll(PDO::FETCH_ASSOC);
                } else {

                    $c[$key]['subcategorias'] = array();
                }

                $sql = "SELECT * FROM produtos WHERE id_categoria = '".$value['id']."'";
                $sql = $this->db->query($sql);

                if($sql->rowCount() > 0) {

                    $p = $sql->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($p as $key_ => $value_) {
                        
                        $sql = "SELECT * FROM produtos_img WHERE id_produto = '".$value_['id']."' LIMIT 1";
                        $sql = $this->db->query($sql);

                        if($sql->rowCount() > 0) {

                            $p[$key_]['img'] = $sql->fetch()['img'];
                        }

                        $sql = "SELECT nome FROM categorias WHERE id = '".$value_['id_categoria']."'";
                        $sql = $this->db->query($sql);

                        if($sql->rowCount() > 0) {

                            $p[$key_]['categoria'] =  $sql->fetch()['nome'];
                        }

                        $sql = "SELECT nome FROM subcategorias WHERE id = '".$value_['id_subcategoria']."'";
                        $sql = $this->db->query($sql);

                        if($sql->rowCount() > 0) {

                            $p[$key_]['subcategoria'] =  $sql->fetch()['nome'];
                        }
                    }

                    $p[$key_]['categoria'] = $value['nome'];

                    $c[$key]['produtos'] = $p;
                } else {

                    $c[$key]['produtos'] = array();
                }
            }
        } else {

            $c = array();
        }

        return $c;
    }

    public function get_sub($categoria) {

        $c = array();

        $sql = "SELECT id FROM categorias WHERE nome = '$categoria'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $id_categoria = $sql->fetch()['id'];

            $sql = "SELECT * FROM subcategorias WHERE id_categoria = '$id_categoria'";
            $sql = $this->db->query($sql);

            if($sql->rowCount() > 0) {

                $c = $sql->fetchAll(PDO::FETCH_ASSOC);
            }
        }

        return $c;
    }
}