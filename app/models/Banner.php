<?php 
class Banner extends model {

    public function get() {

        $sql = "SELECT * FROM banner ORDER BY id DESC";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {

            $b = $sql->fetchAll(PDO::FETCH_ASSOC);

            foreach ($b as $key => $value) {
                
                $sql = "SELECT nome FROM produtos WHERE id = '".$value['id_produto']."'";
                $sql = $this->db->query($sql);

                if($sql->rowCount() > 0) {

                    $b[$key]['produto_nome'] = $sql->fetch()['nome'];
                }
            }
        } else {

            $b = array();
        }

        return $b;
    }
}