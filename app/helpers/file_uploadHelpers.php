<?php 
class file_uploadHelpers {

	public function resize_and_upload($imagem, $destino) {

		if(in_array($imagem['type'], array('image/jpeg', 'image/jpg', 'image/png'))) {
            $ext = 'jpg';
            if($imagem['type'] == 'image/png') {
                $ext = 'png';
            }

            $imagem_grande = md5(time().rand(0,999)).'.'.$ext;

            $tmpName = $imagem['tmp_name'];
			
			if (($imagem['type'] == 'image/jpeg') || ($imagem['type'] == 'image/jpg')) {
				$exif = @exif_read_data($tmpName);

				$imagem_orientation = imagecreatefromjpeg($tmpName);
				if (!empty($exif['Orientation'])) {
				 
				    switch ($exif['Orientation']) {
				        case 3:
				        $imagem_orientatio = imagerotate($imagem_orientation, 180, 0);
				        break;
				        case 6:
				        $imagem_orientatio = imagerotate($imagem_orientation, -90, 0);
				        break;
				        case 8:
				        $imagem_orientatio = imagerotate($imagem_orientation, 90, 0);
				        break;
				    }
				}
				imagejpeg($imagem_orientation, $destino.$imagem_grande, 80);
			} elseif($imagem['type'] == 'image/png') {

				$imagem_orientation = imagecreatefrompng($tmpName);
				
				$background = imagecolorallocatealpha($imagem_orientation, 255, 255, 255, 127);
				imagefill($imagem_orientation, 0, 0, $background);
				imagealphablending($imagem_orientation, false);
				imagesavealpha($imagem_orientation, true);

				imagepng($imagem_orientation, $destino.$imagem_grande);
			}

            $arquivo = $destino.$imagem_grande;

            $largura = 400;
            $altura = 400;

            list($largura_original, $altura_original) = getimagesize($arquivo);

            $ratio = $largura_original / $altura_original;

            if($largura/$altura > $ratio) {
                $largura = $altura *$ratio;
            } else {
                $altura = $largura / $ratio;
            }

            $imagem_final = imagecreatetruecolor($largura, $altura);

            $background = imagecolorallocatealpha($imagem_final, 255, 255, 255, 127);
			imagefill($imagem_final, 0, 0, $background);
			imagealphablending($imagem_final, false);
			imagesavealpha($imagem_final, true);

            if($imagem['type'] == 'image/png') {

                $imagem_original = imagecreatefrompng($arquivo);

                imagecopyresampled($imagem_final, 
                    $imagem_original, 
                    0, 0, 0, 0, 
                    $largura, 
                    $altura, 
                    $largura_original, 
                    $altura_original);

                imagepng($imagem_final, $destino.'mini/'.$imagem_grande);

            }elseif (($imagem['type'] == 'image/jpeg') || ($imagem['type'] == 'image/jpg')) {

                $imagem_original = imagecreatefromjpeg($arquivo);

                imagecopyresampled($imagem_final, 
                    $imagem_original, 
                    0, 0, 0, 0, 
                    $largura, 
                    $altura, 
                    $largura_original, 
                    $altura_original);

                imagejpeg($imagem_final, $destino.'mini/'.$imagem_grande, 80);

            }

            return $imagem_grande;
        } else {

        	return 'not_img';
        }
	}
}