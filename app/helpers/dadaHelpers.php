<?php
class dadaHelpers extends model {

	public function get_data($input_criacao) {

		$current = $this->db->prepare("SELECT NOW() AS now");
		$current->execute();
		$row = $current->fetchObject();
		$timestamp = $row->now;

		//===== data do banco ======
		list($data, $tempo) = explode(' ', $input_criacao);
		list($ano, $mes, $dia) = explode('-', $data);
		list($hora, $minuto, $segundo) = explode(':', $tempo);
		//===== fim data do banco ======

		//===== data de agora======
		list($data_2, $tempo_2) = explode(' ', $timestamp);
		list($ano_2, $mes_2, $dia_2) = explode('-', $data_2);
		list($hora_2, $minuto_2, $segundo_2) = explode(':', $tempo_2);
		//===== fim data de agora======


		$dia1 = mktime($hora, $minuto, $segundo, $mes, $dia, $ano);
		$dia2 = mktime($hora_2, $minuto_2, $segundo_2, $mes_2, $dia_2, $ano_2);
		$d3 = ($dia2 - $dia1);

		$segundos = round($d3);
		$minutos = round($d3/60);
		$horas = round($d3/60/60);
		$dias = round($d3/60/60/24);
		$meses = round($d3/60/60/24/30);
		$anos = round($d3/60/60/24/30/365);

		if($anos >= 1) {

			$data_return = strftime('%d de %B de %Y', strtotime($input_criacao)).' as '.$hora.':'.$minuto;
		} else {

			if(($meses > 1) || ($dias > 7)) {

				//$data_return = "Há ".$meses." meses";
				
				setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
				$data_return = strftime('%d de %B', strtotime($input_criacao)).' as '.$hora.':'.$minuto;
			} else {

				if($dias > 2) {
					
					setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
					$data_return = strftime('%A', strtotime($input_criacao)).' as '.$hora.':'.$minuto;
				} elseif($dias == 2) {

					setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
					$data_return = 'Ontem as '.$hora.':'.$minuto;
				} else {

					if($horas > 1) {

						$data_return = "Há ".$horas." horas";
					} elseif($horas == 1) {

						$data_return = "Há 1 hora";
					} else {

						if($minutos > 1) {

							$data_return = "Há ".$minutos." minutos";
						} elseif($minutos == 1) {

							$data_return = "Há 1 minuto";
						} else {

							$data_return = "Há menos de 1 minuto";
						}
					}
				}
			}
		}  

		return $data_return;
	}
}