<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>TATUDAKI</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="assets/img/icon.png">
    <script src="assets/js/plugins/jquery-3.2.1.min.js"></script>

	<link rel="stylesheet" href="assets/css/plugins/bootstrap.css">
    <link rel="stylesheet" href="assets/css/css_views/template.css">

	<script type="text/javascript">
		$(function() {

			window.location = 'https://tatudaki.net/';
			return false;
			
			var userAgent = navigator.userAgent || navigator.vendor || window.opera;

			if(userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i )){
			    
			    window.location = "https://apps.apple.com/us/app/TATUDAKI/id1500902149?l=pt&ls=1";
			} else if(userAgent.match(/Android/i)) {

				window.location = 'https://play.google.com/store/apps/details?id=org.TATUDAKI.TATUDAKI';
			} else {
				
				window.location = 'https://tatudaki.net/';
			}
		});
	</script>
</head>
<body>
	<div class="w-100 h-100 d-flex justify-content-center align-items-center">
		<div>
        	<img style="width: 80%; max-width: 300px;" src="<?= BASE ?>assets/img/logo.png">
        </div>
    </div>
</body>
</html>