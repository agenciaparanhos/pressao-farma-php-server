
<!DOCTYPE html>
<html class="no-js" lang="zxx"> 
  
<head>
  <title>pressaofarma</title>
    
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="Anderson Barbosa Cavalcanti">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

    <link rel="shortcut icon" href="<?php echo BASE ?>assets/img/icon.png">

    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/owl.theme.css">
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/owl.transitions.css">
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/owl.carousel.css">
    <!-- Owl Stylesheet -->
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/magnific-popup.css">
    <!-- Magnific PopUP Stylesheet -->
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/isotope.css">
    <!-- Isotope Stylesheet -->
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/bootstrap.min.css" />
    <!-- <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/bootstrap.css" /> -->
    <!-- Bootstrap Stylesheet -->
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/et-lant-font.css" />
    <!-- Et-Lant Font Stylesheet -->
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/3dslider.css" />
    <!-- 3D Slider Stylesheet -->
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/animate.css" />
    <!-- Animate Stylesheet -->
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/material-design-iconic-font.css" />
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/swiper.min.css">
    <!-- Iconic Font Stylesheet -->

    <link rel="stylesheet" href="<?= BASE ?>assets/css/style.css">
    <!-- Main Style.css Stylesheet -->

    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/responsive.css">
    <!-- Resposive.css Stylesheet -->

    <style type="text/css">
      
      .img_1 {width: 100px;height: 100px;border-radius: 100%;background-color: #fff;box-shadow: 0 18px 32px rgba(0,0,0,.08);margin: 0 auto;text-align: center;line-height: 100px;display: block;border-radius: 50%; overflow: hidden;}

      .padding_h2 {padding-top: 0px;}
  
      @media screen and (min-width: 500px) {

        #cont_add_estabelecimentos .form-group {width: 50% !important; float: left !important;}
        #cont_add_estabelecimentos .form-group:nth-child(1) {width: 100% !important;}

        .cont_2 {padding-right: 1rem;}
        .cont_1 {padding-left: 1rem;}
      }

      @media screen and (min-width: 1024px) {

        .padding_h2 {padding-top: 100px;}
      }
    </style>
  </head>
  <body class="apps-craft-v18">

    <div class="btn_2 mt_design" id="erro_absolute_2" style="<?php if(!empty($_SESSION['error'])) {echo 'display: block;';} else {echo 'display: none;';} ?> position: fixed; top: 70px; right: 2rem; padding: 2rem; border-radius: 5px; z-index: 5; background: rgba(0, 0, 0, 0.5);box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12); color: #fff;">
    
        <?php if(!empty($_SESSION['error'])) { echo $_SESSION['error'];} ?>
    </div>
  
  <!-- Apps Craft Body Start -->
  <div id="preloader"></div>

  <!-- Apps Craft Main Menu -->
  <header style="position: fixed; top: 0; left: 0; width: 100%; z-index: 100; transition: 0.3s; padding: 1.5rem 1rem; background: transparent;">
    <div class="container">
      <div class="row">
        <div class="col-xs-12" style="display: flex; justify-content: space-between; align-items: center;">
          <div class="apps-craft-logo">
            <a href="#apps-craft-home">
              <img src="<?= BASE ?>assets/img/sticky-logo_2.png" style="height: 30px;" alt="pressaofarma Logo">
            </a>
          </div>
          <p style="margin-bottom: 0; color: #fff;">pressaofarma</p>
        </div>
      </div>
    </div>
  </header> <!-- .apps-craft-main-menu END -->

  <!-- Apps Craft Welcome Section -->
  <section class="apps-craft-welcome-section apps-craft-welcome-section-v3" style="background-image: url(img/welcome-version-2.jpg);" id="apps-craft-home">
    <div class="apps-craft-welcome-container">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="apps-craft-welcome-tbl">
              <div class="apps-craft-welcome-tbl-c">
                <div class="apps-craft-welcome-content text-center">
                  <h1>Faça o download Gratuitamente do aplicativo <span>pressaofarma</span> </h1>
                  <p>O aplicativo feito pensando em você ;)</p>

                  <div class="apps-craft-download-store-btn-group">
                    <a href="https://play.google.com/store/apps/details?id=com.pressaofarma.pressaofarma" target="_blanck" class="apps-craft-btn play-store-btn"><img src="<?= BASE ?>assets/img/google-play-logo.png" alt=""></a>
                    <a href="" target="_blanck" class="apps-craft-btn app-store-btn link_apple"><img src="<?= BASE ?>assets/img/app-store-logo.png" alt=""></a
>                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="apps-craft-welcome-screenshort-v3 hidden-xs" itemscope itemtype="http://schema.org/ImageGallery">
          <figure itemprop="associatedMedia" class="text-right" itemscope itemtype="http://schema.org/ImageObject">
            <img src="<?= BASE ?>assets/img/welcome-screenshort-left.png" alt="Apps Craft Welcome Left Screenshort" class="wow fadeIn" data-wow-delay=".2s">
          </figure>

          <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
            <img src="<?= BASE ?>assets/img/welcome-screenshort-center.png" alt="Apps Craft Welcome Center Screenshort" class="wow fadeIn" data-wow-delay=".4s">
          </figure>

          <figure itemprop="associatedMedia" class="text-left" itemscope itemtype="http://schema.org/ImageObject">
            <img src="<?= BASE ?>assets/img/welcome-screenshort-right.png" alt="Apps Craft Welcome Right Screenshort" class="wow fadeIn" data-wow-delay=".6s">
          </figure>
        </div>
      </div>
    </div>
  </section>

  <section class="apps-craft-feature-section section-padding" id="apps-craft-feature">
    <div class="container">
      <div class="apps-craft-section-heading">
        <h2>CARACTERÍSTICAS</h2>
      </div> <!-- .apps-craft-section-heading END -->
      <div class="row content-margin-top">
        <div class="col-md-6 col-sm-12 col-xs-12">
          <div class="apps-craft-feature-img text-center" itemscope itemtype="http://schema.org/ImageGallery">
            <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
              <img src="<?= BASE ?>assets/img/feature-app-screenshort.png" alt="Apps Craft Feature Image">

              <span class="apps-craft-feature-ico icon-1x" data-bottom="transform:translateX(-103px)" data-top="transform:translateX(-63px);">
                <img src="<?= BASE ?>assets/img/icon/Icon-1.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->

              <span class="apps-craft-feature-ico icon-2x" data-bottom="transform:translateX(-171px)" data-top="transform:translateX(-131px);">
                <img src="<?= BASE ?>assets/img/icon/ICON-2.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->

              <span class="apps-craft-feature-ico icon-3x" data-bottom="transform:translateX(-153px)" data-top="transform:translateX(-113px);">
                <img src="<?= BASE ?>assets/img/icon/12.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->

              <span class="apps-craft-feature-ico icon-4x" data-bottom="transform:translateX(-176px)" data-top="transform:translateX(-136px);">
                <img src="<?= BASE ?>assets/img/icon/ICON-5.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->

              <span class="apps-craft-feature-ico icon-5x" data-bottom="transform:translateX(-97px)" data-top="transform:translateX(-55px);">
                <img src="<?= BASE ?>assets/img/icon/ICON-7.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->

              <span class="apps-craft-feature-ico icon-6x" data-bottom="transform:translateY(-130px)" data-top="transform:translateY(-90px);">
                <img src="<?= BASE ?>assets/img/icon/15.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->

              <span class="apps-craft-feature-ico icon-7x" data-bottom="transform:translateY(-134px)" data-top="transform:translateY(-94px);">
                <img src="<?= BASE ?>assets/img/icon/icon-3.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->

              <span class="apps-craft-feature-ico icon-8x" data-bottom="transform:translateY(-160px)" data-top="transform:translateY(-120px);">
                <img src="<?= BASE ?>assets/img/icon/ICON--9.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->

              <span class="apps-craft-feature-ico icon-9x" data-bottom="transform:translateX(-110px)" data-top="transform:translateX(-70px);">
                <img src="<?= BASE ?>assets/img/icon/ICON-11.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->

              <span class="apps-craft-feature-ico icon-10x" data-bottom="transform:translateY(26px)" data-top="transform:translateY(26px);">
                <img src="<?= BASE ?>assets/img/icon/13.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->

              <span class="apps-craft-feature-ico icon-11x" data-bottom="transform:translateX(74px)" data-top="transform:translateX(34px);">
                <img src="<?= BASE ?>assets/img/icon/ICon4.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->

              <span class="apps-craft-feature-ico icon-12x" data-bottom="transform:translateX(160px)" data-top="transform:translateX(120px);">
                <img src="<?= BASE ?>assets/img/icon/ICON-10.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->

              <span class="apps-craft-feature-ico icon-13x" data-bottom="transform:translateX(150px)" data-top="transform:translateX(110px);">
                <img src="<?= BASE ?>assets/img/icon/ICON-6.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->

              <span class="apps-craft-feature-ico icon-14x" data-bottom="transform:translateX(80px)" data-top="transform:translateX(40px);">
                <img src="<?= BASE ?>assets/img/icon/15.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->

              <span class="apps-craft-feature-ico icon-15x" data-bottom="transform:translateX(120px)" data-top="transform:translateX(80px);">
                <img src="<?= BASE ?>assets/img/icon/13.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->

              <span class="apps-craft-feature-ico icon-16x" data-bottom="transform:translateX(-140px)" data-top="transform:translateX(-100px);">
                <img src="<?= BASE ?>assets/img/icon/18.png" alt="Small Icon">
              </span> <!-- .apps-craft-feature-ico END -->
            </figure>
          </div> <!-- .apps-craft-feature-img END -->
        </div>

        <div class="col-md-6 col-sm-12 col-xs-12">
          <div class="apps-craft-feature-container clear-both">
            <div class="apps-craft-single-feature opt_1">
              <div class="apps-craft-feature-content wow fadeIn" data-wow-delay="800ms">
                <i class="icon-trophy apps-craft-round"></i>
                <h3>Ótima Experiência</h3>
                <p>O aplicativo pressaofarma foi feito pensando na sua usabilidade. Interface simples, agradável e fácil de usar.</p>
              </div> <!-- .apps-craft-feature-content END -->
            </div> <!-- .apps-craft-single-feature END -->
            <div class="apps-craft-single-feature opt_1">
              <div class="apps-craft-feature-content wow fadeIn" data-wow-delay="400ms">
                <i class="icon-wine apps-craft-round"></i>
                <h3>Aplicativo completo!</h3>
                <p>Cada detalhe pensado para você. No app você escolhe o melhor produto, consulta o tamanho, ver imagens e mais.</p>
              </div> <!-- .apps-craft-feature-content END -->
            </div> <!-- .apps-craft-single-feature END -->
            <div class="apps-craft-single-feature opt_1">
              <div class="apps-craft-feature-content wow fadeIn" data-wow-delay="600ms">
                <i class="zmdi zmdi-wifi-alt zmdi-hc-fw"></i>
                <h3>Um App para quem ama facilidade</h3>
                <p>Encontre produtos. Descubra suas qualificações, taxa de entrega, descontos e etc.</p>
              </div> <!-- .apps-craft-feature-content END -->
            </div> <!-- .apps-craft-single-feature END -->
            <div class="apps-craft-single-feature opt_1">
              <div class="apps-craft-feature-content wow fadeIn" data-wow-delay="200ms">
                <i class="zmdi zmdi-cutlery zmdi-hc-fw"></i>
                <h3>Produtos divididos por categoria</h3>
                <p>Com os produtos divididos por categoria tudo fica simples e prático.</p>
              </div> <!-- .apps-craft-feature-content END -->
            </div> <!-- .apps-craft-single-feature END -->
          </div> <!-- .apps-craft-feature-container END -->
        </div>
      </div>
    </div>
  </section>

  <section class="apps-carft-screen-short-ssection section-padding" id="apps-carft-screen-short">
    <div class="container">
      <div class="apps-craft-section-heading">
        <h2>Algumas telas</h2>
      </div> <!-- .apps-craft-section-heading END -->

            <div class="row content-margin-top">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 img-full">
                    <!-- Swiper -->
                    <div class="swiper-container two swiper-container-horizontal swiper-container-3d swiper-container-coverflow">
                        <div class="swiper-wrapper" style="transform: translate3d(-1482.5px, 0px, 0px); transition-duration: 0ms;"><div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active" data-swiper-slide-index="0" style="transform: translate3d(750px, 0px, -1125px) rotateX(0deg) rotateY(0deg); z-index: -7; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-1.png" alt="" class="img-responsive">
                                </div>
                            </div><div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next" data-swiper-slide-index="1" style="transform: translate3d(600px, 0px, -900px) rotateX(0deg) rotateY(0deg); z-index: -5; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-2.png" alt="" class="img-responsive">
                                </div>
                            </div><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="2" style="transform: translate3d(450px, 0px, -675px) rotateX(0deg) rotateY(0deg); z-index: -4; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-3.png" alt="" class="img-responsive">
                                </div>
                            </div><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="3" style="transform: translate3d(300px, 0px, -450px) rotateX(0deg) rotateY(0deg); z-index: -2; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-4.png" alt="" class="img-responsive">
                                </div>
                            </div><div class="swiper-slide swiper-slide-duplicate swiper-slide-prev" data-swiper-slide-index="4" style="transform: translate3d(150px, 0px, -225px) rotateX(0deg) rotateY(0deg); z-index: -1; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-5.png" alt="" class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="transform: translate3d(0px, 0px, 0px) rotateX(0deg) rotateY(0deg); z-index: 1; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-1.png" alt="" class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-next" data-swiper-slide-index="1" style="transform: translate3d(-150px, 0px, -225px) rotateX(0deg) rotateY(0deg); z-index: 0; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-2.png" alt="" class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide" data-swiper-slide-index="2" style="transform: translate3d(-300px, 0px, -450px) rotateX(0deg) rotateY(0deg); z-index: -2; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-3.png" alt="" class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide" data-swiper-slide-index="3" style="transform: translate3d(-450px, 0px, -675px) rotateX(0deg) rotateY(0deg); z-index: -3; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-4.png" alt="" class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate-prev" data-swiper-slide-index="4" style="transform: translate3d(-600px, 0px, -900px) rotateX(0deg) rotateY(0deg); z-index: -5; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-5.png" alt="" class="img-responsive">
                                </div>
                            </div>
                        <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active" data-swiper-slide-index="0" style="transform: translate3d(-750px, 0px, -1125px) rotateX(0deg) rotateY(0deg); z-index: -6; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-1.png" alt="" class="img-responsive">
                                </div>
                            </div><div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next" data-swiper-slide-index="1" style="transform: translate3d(-900px, 0px, -1350px) rotateX(0deg) rotateY(0deg); z-index: -8; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-2.png" alt="" class="img-responsive">
                                </div>
                            </div><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="2" style="transform: translate3d(-1050px, 0px, -1575px) rotateX(0deg) rotateY(0deg); z-index: -9; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-3.png" alt="" class="img-responsive">
                                </div>
                            </div><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="3" style="transform: translate3d(-1200px, 0px, -1800px) rotateX(0deg) rotateY(0deg); z-index: -11; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-4.png" alt="" class="img-responsive">
                                </div>
                            </div><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="4" style="transform: translate3d(-1350px, 0px, -2025px) rotateX(0deg) rotateY(0deg); z-index: -12; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-5.png" alt="" class="img-responsive">
                                </div>
                            </div></div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet"></span></div>
                    </div>
                </div>
            </div>
    </div>
  </section>

  <!-- Apps Craft Now Available On -->
  <div class="apps-craft-now-available-section" id="apps-craft-available" style="background-image: url(<?= BASE ?>assets/img/now-available-on-round-bg.png);">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="apps-craft-tbl">
            <div class="apps-craft-tbl-c">
              <div class="apps-craft-now-available-content wow fadeIn">
                <h3>Comece a usar agora mesmo!</h3>
                <p>O aplicativo pressaofarma surgiu da necessidade de reunir em um só lugar informações completas de produtos novos e usados, de forma prática e rápida. Assim, o usuário tem a oportunidade de conhecer todos os produtos, descobrir seus diferenciais, acessar o perfil do vendedor e efetuar sua compra diretamente no app, de forma simples e rápida.</p>

                <div class="apps-craft-download-store-btn-group">
                  <a href="https://play.google.com/store/apps/details?id=com.pressaofarma.pressaofarma" target="_blanck" class="apps-craft-btn play-store-btn"><img src="<?= BASE ?>assets/img/google-play-logo.png" alt=""></a>
                  <a href="" target="_blanck" class="apps-craft-btn app-store-btn link_apple"><img src="<?= BASE ?>assets/img/app-store-logo.png" alt=""></a>
                </div> <!-- .apps-craft-download-store-btn-group END -->
              </div> <!-- .apps-craft-now-available-content END -->
            </div>
          </div>
          <figure class="apps-craft-app-secreenshort" data-bottom="transform:translateY(180px) translateX(-50%);" data-top="transform:translateY(100px) translateX(-50%);" itemscope itemtype="http://schema.org/ImageGallery">
            <img src="<?= BASE ?>assets/img/screenshort-now-available.png" alt="Apps Craft App Screenshort">
          </figure> <!-- .apps-craft-app-secreenshort END -->
        </div>
      </div>
    </div>
  </div> <!-- .apps-craft-now-available-section END -->

  <section class="apps-craft-faq-section section-padding" id="apps-craft-faq">
    <div class="container">
      <div class="apps-craft-section-heading">
        <h2 class="padding_h2">FAQ</h2>
      </div> <!-- .apps-craft-section-heading END -->
      <div class="row content-margin-top">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="apps-craft-accordion wow fadeIn" data-wow-delay="400ms" style="visibility: visible; animation-delay: 400ms; animation-name: fadeIn;">
            <div class="panel-group" id="accordion">

              <?php foreach ($duvidas as $key => $d): ?>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $key; ?>" class="collapsed" aria-expanded="false">
                      <?= $d['duvida'] ?></a>
                    </h4>
                  </div>
                  <div id="collapse<?= $key; ?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body"><?= $d['resposta'] ?></div>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          </div> <!-- .apps-craft-accordion END -->
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="apps-craft-contact-form-content wow fadeIn" data-wow-delay="800ms" style="visibility: visible; animation-delay: 800ms; animation-name: fadeIn;">
            <div class="apps-craft-contact-form-content-inner">
              <h2>Alguma outra pergunta? Você pode entrar em <span>contato conosco.</span></h2>
              <div class="apps-craft-contact-form">
                <form method="POST" action="<?= BASE; ?>home/suporte" id="apps-craft-form">
                  <input type="email" id="apps-craft-input-email" required="resposta" name="email" placeholder="12345@domain.com">
                  <textarea name="msg" id="apps-craft-input-message" required="resposta" cols="30" rows="10" placeholder="Digite sua mensagem aqui.."></textarea>

                  <div class="apps-craft-submit-btn-ar">
                    <input type="submit" name="Envia Mensagem" value="Envia Mensagem" id="apps-craft-input-send">
                  </div>
                </form>
              </div> <!-- .apps-craft-contact-form END -->
            </div> <!-- .apps-craft-contact-form-content-inner END -->
          </div> <!-- .apps-craft-contact-form-content END -->
        </div>
      </div>
    </div>
  </section>

  <!-- Apps Craft Footer Section -->
  <footer class="apps-craft-footer-section" id="apps-craft-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="apps-craft-subscribe-content">
            <div class="wow fadeInUp" data-wow-delay=".3s">
              <h2><span>&copy;</span> Todos os direitos reservados</h2>
            </div>
          </div> <!-- .apps-craft-subscribe-content END -->

          <figure class="apps-craft-footer-logo text-center">
            <a href="#">
              <img style="max-width: 300px;" src="<?= BASE ?>assets/img/logo_b.png" alt="Apps Craft Footer Logo" class="wow fadeInUp" data-wow-delay=".7s">
            </a>
          </figure> <!-- .apps-craft-footer-logo END -->
        </div>
      </div>
    </div>
  </footer> <!-- .apps-craft-footer-section END -->

  <script type="text/javascript" src="<?= BASE ?>assets/js/plugins/jquery-3.1.1.min.js"></script>
  <!-- jquery-3.1.1.min.js -->
  <script type="text/javascript" src="<?= BASE ?>assets/js/plugins/jquery.ajaxchimp.min.js"></script>
  <!--jquery.ajaxchimp.min.js  -->
  <script type="text/javascript" src="<?= BASE ?>assets/js/plugins/jquery.easing.1.3.js"></script>
  <!--jquery.easing.1.3.js  -->
  <script type="text/javascript" src="<?= BASE ?>assets/js/plugins/bootstrap.min.js"></script>
  <!-- bootstrap.min.js -->
  <script src="<?= BASE ?>assets/js/plugins/owl.carousel.min.js"></script>
  <!-- owl.carousel.min.js -->
  <script src="<?= BASE ?>assets/js/plugins/isotope.pkgd.min.js"></script>
  <!-- isotope.pkgd.min.js -->
  <script src="<?= BASE ?>assets/js/plugins/jquery.magnific-popup.min.js"></script>
  <!-- jquery.magnific-popup.min.js -->
  <script src="<?= BASE ?>assets/js/plugins/skrollr.min.js"></script>
  <!-- skrollr.min.js -->
  <script src="<?= BASE ?>assets/js/plugins/utils.js"></script>
  <!-- utils.js -->
  <script src="<?= BASE ?>assets/js/plugins/jquery.parallax.js"></script>
  <!-- jquery.parallax.js -->
  <script src="<?= BASE ?>assets/js/plugins/wow.js"></script>
  <!-- wow.js -->
  <script src="<?= BASE ?>assets/js/plugins/swiper.min.js"></script>
  <script src="<?= BASE ?>assets/js/plugins/main.js"></script>
  <!-- main.js -->
  <script type="text/javascript">
      $(function() {

        if($(document).scrollTop() > 50) {

          $('header').css({"background":'rgba(255, 255, 255, 0.5)'}).find('p').css({"color":"#fff"});
          $('header').find('img').attr('src', '<?= BASE ?>assets/img/sticky-logo_2.png')
        } else {
          
          $('header').css({"background":'transparent'}).find('p').css({"color":"#fff"});
          $('header').find('img').attr('src', '<?= BASE ?>assets/img/sticky-logo_2.png')
        }
        
        $(document).on('scroll', function() {

          if($(document).scrollTop() > 50) {

            $('header').css({"background":'rgba(255, 255, 255, 0.5)'}).find('p').css({"color":"#ff"});
            $('header').find('img').attr('src', '<?= BASE ?>assets/img/sticky-logo_2.png')
          } else {
            
            $('header').css({"background":'transparent'}).find('p').css({"color":"#fff"});
            $('header').find('img').attr('src', '<?= BASE ?>assets/img/sticky-logo_2.png')
          }
        })

        if($('#erro_absolute_2').css('display') == 'block') {
            setTimeout(function() {
            
                $('#erro_absolute_2').fadeOut();
            }, 3000);
        }
      })
    </script>
</body>

</html>