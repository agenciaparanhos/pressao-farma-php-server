<?php
class homeController extends controller {

	public function index() {

		$dados = array();

        $d = new Duvidas();

        $dados['duvidas'] = $d->get();

		$this->loadView('home', $dados);

		unset($_SESSION['error']);
	}

    public function suporte() {

        $email = addslashes($_POST['email']);
        $msg = addslashes($_POST['msg']);

        $d = new Duvidas();
        $d->add($email, $msg);
    }
}