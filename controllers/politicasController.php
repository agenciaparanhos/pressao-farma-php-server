<?php 
class politicasController extends controller {

	public function index() {

		$dados = array();

		$c = new Config();
		$dados['termos'] = $c->get_termos();

		$this->loadView('politicas', $dados);
	}
}