<?php

class Config extends model {

	public function get_termos() {

		$sql = "SELECT termos FROM config";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			return $sql->fetch()['termos'];

		}
	}
}