<?php
/**
 * Html2Pdf Library - example
 *
 * HTML => PDF converter
 * distributed under the OSL-3.0 License
 *
 * @package   Html2pdf
 * @author    Laurent MINGUET <webmaster@html2pdf.fr>
 * @copyright 2017 Laurent MINGUET
 */
require_once 'vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

try {
    
    $content = '<!DOCTYPE html>
    				<html>
						<body>
							<div style="width: 100%;">
								<h2>Adega copa rio delivery</h2>
		    					
		    					'.$_SESSION['dados_exportar'].'
		    				</div>
	    				</body>
    				</html>';


    $html2pdf = new Html2Pdf('P', 'A4', 'fr');
    $html2pdf->writeHTML($content);
    $html2pdf->output('PedidoAdegaCopaDelivery.pdf');
} catch (Html2PdfException $e) {

	print_r($e);exit;
    
    $_SESSION['erro'] = 'Não deu certo.';
    header('location: /painel');
}
