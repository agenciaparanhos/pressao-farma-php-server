<?php
class push_notificationHelpers extends model {

	function sendMessage($msg, $data_, $img, $registrationIds){
		
		$content = array(
			"en" => $msg
			);

		if($img != '') {
		
			$fields = array(
				'app_id' => "8fb652ff-ebdb-4447-8ada-9601d18d022c",
				'include_player_ids' => $registrationIds,
				'data' => $data_,
				'big_picture' => BASE_PAI.'assets/img/push/'.$img,
				'contents' => $content
			);
		} else {

			$fields = array(
				'app_id' => "8fb652ff-ebdb-4447-8ada-9601d18d022c",
				'data' => $data_,
				'include_player_ids' => $registrationIds,
				'contents' => $content
			);
		}
		
		$fields = json_encode($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
	}
}