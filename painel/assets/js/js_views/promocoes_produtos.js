$(window).ready(function() {
	
	$('.btn_delet').on('click', function() {
		
        var id = this.id;
		var id_promocao = $(this).attr('data-id_promocao');
        $('#cont_deletar').modal('show');

		$('#cont_deletar').find('.btn_sim').on('click', function() {
			
			window.location.href = BASE+'promocoes/deletar/'+id+'_'+id_promocao;
		});
	});	

	$('.btn_editar').on('click', function() {

		var id = this.id;

        $('#load_all').show();

        var cont = $('#cont_update');

		$.ajax({
            url: BASE+'promocoes/crud',
            type: 'POST',
            data: "acao=get_dados_produto&id="+id,
            dataType: 'json',
            error: function(argument) {
                console.log(argument);

                alert('Não deu certo!');
            },
            success: function(argument) {
            	
                cont.find('select[name="id_produto"]').val(argument.id_produto).change();;
                cont.find('input[name="desconto"]').val(argument.desconto);
                cont.find('input[name="id"]').val(id);
                cont.modal('show');
            }, 
            complete: function(argument) {

            	$('#load_all').hide();
            }
        });
	});
});