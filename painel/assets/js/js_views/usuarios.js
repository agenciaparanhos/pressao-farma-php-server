$(window).ready(function() {

	$('.btn_delet').on('click', function() {
		
		var id = this.id

		$('#cont_deletar_usuario').find('.btn_sim').on('click', function() {
			
			window.location.href = 'usuarios/deletar/'+id;
		});
	});	

	$('.btn_editar').on('click', function() {

		var id = this.id;

        var cont = $('#cont_update_usuario');
		$('#load_all').show();

		$.ajax({
            url: 'usuarios/crud',
            type: 'POST',
            data: "acao=get_dados_usuario&id="+id,
            dataType: 'json',
            error: function(argument) {
                console.log(argument);

                setTimeout(function() {
                	cont.modal('hide')
                
                	$('#erro_absolute').html('<strong>Oops: </strong>Não deu certo!').fadeIn();
                }, 1000)
            },
            success: function(argument) {
            	console.log(argument)
                cont.find('input[name="nome"]').val(argument.nome);
                cont.find('input[name="email"]').val(argument.email);
                cont.find('input[name="telefone"]').val(argument.telefone);
                cont.find('input[name="id"]').val(id);
                cont.find('.p_load').hide();
                cont.find('form').fadeIn();
            }, 
            complete: function(argument) {

            	$('#load_all').hide();
            }
        });
	});
});