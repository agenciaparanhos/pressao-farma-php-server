$(window).ready(function() {
	
	$('.btn_delet').on('click', function() {
		
		var id = this.id;

		$('#cont_deletar').modal('show');

		$('#cont_deletar').find('.btn_sim').on('click', function() {
			
			window.location.href = 'banner/deletar/'+id;
		});
	});

	$('select[name="tipo"]').on('change', function() {

		var form = $(this).closest('form');

		console.log(this.value);

		form.find('.cont_opt').hide().find('input').removeAttr('required');

		if(this.value == 'link') {

			form.find('.cont_link').show().find('input').attr('required', 'required');
		} else if(this.value == 'produto') {

			form.find('.cont_produtos').show();
		} else if(this.value == 'loja') {

			form.find('.cont_lojas').show();
		}
	})

	$('.btn_editar').on('click', function() {
		
		var id = this.id;

		$('#load_all').show();

		$.ajax({
			url: BASE+'banner/get_dados/'+id,
			type: 'POST',
			dataType: 'json',
			error: function(argument) {

				console.log(argument);
			}, 
			success: function(argument) {

				var cont = $('#cont_update');

				cont.find('select[name="tipo"]').val(argument['tipo']).change();
				cont.find('select[name="id_loja"]').val(argument['id_loja']).change();
				cont.find('select[name="id_proto"]').val(argument['id_proto']).change();
				cont.find('input[name="link"]').val(argument['link']);
				cont.find('input[name="id"]').val(argument['id']);

				cont.modal('show');
			},
			complete: function() {

				$('#load_all').hide();
			}
		})
	});
});