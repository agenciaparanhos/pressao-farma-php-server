$(window).ready(function() {

	$('.btn_delet').on('click', function() {
		
		var id = this.id

        $('#cont_deletar').modal('show');
		$('#cont_deletar').find('.btn_sim').on('click', function() {
			
			window.location.href = 'promocoes/deletar/'+id;
		});
	});	

	$('.btn_editar').on('click', function() {

		var id = this.id;

        $('#load_all').show();
        var cont = $('#cont_update');

		$.ajax({
            url: 'promocoes/crud',
            type: 'POST',
            data: "acao=get_dados&id="+id,
            dataType: 'json',
            error: function(argument) {
                console.log(argument);

                alert('Não deu certo!');
            },
            success: function(argument) {
            	
                cont.find('input[name="nome"]').val(argument.nome);
                cont.find('input[name="cor"]').val(argument.cor);

                cont.find('input[name="id"]').val(id);
                cont.modal('show');
            }, 
            complete: function(argument) {

            	$('#load_all').hide();
            }
        });
	});
});








