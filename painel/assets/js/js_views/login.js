$(window).ready(function() {
	
	$('.cont_login').find('.btn_entrar').on('click', function() {
		
		login();
	});

	$('.cont_login_in').on('keypress', function(event) {
		
		if(event.keyCode == 13) {

			login();
		}
	});
});

function login () {
		
	var this_ = $(this);
	this_.find('img').fadeIn();

	var email = $('.cont_login').find('input[name="email"]').val();
	var senha = $('.cont_login').find('input[name="senha"]').val();

	console.log(email+'-'+senha);

	if($.trim(email) != '' && $.trim(senha) != '') {

		$.ajax({
			dataType: "json",
			data: 'acao=logar&email='+email+"&senha="+senha,
			type: 'POST',
			url: 'login/crud',
			error: function(argument) {
				console.log(argument);

				$('.cont_add_profissao').find('.alert').html('<strong>Ooops: </strong>aconteceu um erro inesperado!').fadeIn();
				$('.cont_all').find('.cont_load').fadeOut();
			},
			success: function(argument) {
				console.log(argument);

				if(argument == '1') {

					$('.cont_login_in').find('.alert').html('<strong>Ooops: </strong> o email e a senha não podem ficar em branco!').fadeIn();
				} else if(argument == '2') {

					$('.cont_login_in').find('.alert').html('<strong>Ooops: </strong> Email e/ou senha errados!').fadeIn();
				} else if(argument == '5') {

					window.location.reload();
				}
				this_.find('img').fadeOut();
			}
		});
	} else {

		$('.cont_login_in').find('.alert').html('<strong>Ooops: </strong> o email e a senha não podem ficar em branco!').fadeIn();
		this_.find('img').fadeOut();
	}
}