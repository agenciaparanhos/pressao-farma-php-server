
<style type="text/css">
	#sidebar-menu > ul > li > a.li_politicas {color: #FFF; background-color: #414d58; border-left: 2px solid #608ab3;}
</style>

<link rel="stylesheet" href="<?= BASE ?>assets/css/jquery.qeditor.css" type="text/css">
<script src="<?= BASE ?>assets/js/jquery.qeditor.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= BASE ?>assets/js/jquery.caret.js"></script>

<script type="text/javascript">
	
	$(function() {
		
		$("#textarea").qeditor({});
	});
</script>

<div class="content">
    
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<ol class="breadcrumb float-left">
						<li class="breadcrumb-item">
                            <h5 class="modal-title float-left m-0">Políticas de uso</h5>
                        </li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<!-- end row -->
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-12">
				<form method="POST" action="<?= BASE ?>politicas/update">
					<div class="col-12">
						<div class="card p-3">
							<textarea class="form-control" name="politicas" id="textarea"><?= $config['politicas']; ?></textarea>
							
							<div class="float-right">
								<button class="btn btn_2" id="btn_salvar">Salvar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>