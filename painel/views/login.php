<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>pressaofarma</title>

  <!--========== fim ogrigatorio ============-->
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="<?php echo BASE ?>assets/img/icon.png">

	<link rel="stylesheet" href="<?php echo BASE ?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo BASE ?>assets/css/font-awesome.min.css">

	<script src="<?php echo BASE ?>assets/js/jquery-3.2.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	<!--========== fim ogrigatorio ============-->
	
	<style type="text/css">
		html, body {width: 100%; height: 100%;}
	</style>

	<link rel="stylesheet" href="<?php echo BASE ?>assets/css/css_views/login.css">
	<script type="text/javascript" src="<?php echo BASE ?>assets/js/js_views/login.js"></script>
 
</head> 
<body>
	<div style="position: fixed; top: 0; left: 0; width: 100%; height: 100%; z-index: -2;">
		<img style="position: absolute; top: 0; left: 0; object-fit: cover;" class="w-100 h-50" src="<?= BASE; ?>assets/img/bg.jpg">
		<div class="w-100 h-50" style="position: fixed; top: 0; left: 0; z-index: 1; background: rgba(0, 0, 0, 0.8);">
			<h5 class="p-3 text-white">Painel</h5>
		</div>
	</div>
	<div class="cont_login d-flex justify-content-center align-items-center">
		
		<div class="cont_login_in" style="position: relative; border-radius: 10px; overflow: hidden; background: #fff;box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);">

			<div class="mb-2">
				<img style="width: 90%;" class="d-block ml-auto mr-auto mb-4" src="<?php echo BASE ?>assets/img/logo.png">
			</div>

			<div class="form-group">
				<p class="mb-0 font-weight-bold">Email:</p>
				<input type="text" name="email" placeholder="Email" class="form-control" autofocus="autofocus">
			</div>

			<div class="form-group">
				<p class="mb-0 font-weight-bold">Senha:</p>
				<input type="password" name="senha" placeholder="Senha" class="form-control">
			</div>
			
			<div class="alert alert-danger" style="text-align: center;" role="alert">
				<!--msg-->
			</div>

			<div class="form-group mt-4">
				<button class="btn btn-block btn_entrar font-weight-bold" style="background: #36e1b8; color: #fff;">
					<img src="<?php echo BASE ?>assets/img/load.gif">
					Entrar
				</button>
			</div>
		</div>
	</div>
</body>
</html>