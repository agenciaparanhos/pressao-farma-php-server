<style type="text/css">
	#sidebar-menu > ul > li > a.li_promocoes {color: #FFF; background-color: #414d58; border-left: 2px solid #608ab3;}
</style>

<script type="text/javascript" src="<?= BASE ?>assets/js/js_views/promocoes_produtos.js?<?= time(); ?>"></script>
<script type="text/javascript">
	$(window).ready(function() {
		
		$('#example1').DataTable({
			"ordering": false,
			"oLanguage": {
		        "sEmptyTable": "Nenhum Produto ainda!"
		    }
		});
	});
</script>

<div class="content" style="padding-bottom: 50px;">
            
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<ol class="breadcrumb float-left">
						<li class="breadcrumb-item">
                            <h5 class="modal-title float-left m-0">Promoções / Produtos</h5>
                        </li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card mb-3">
					<div class="card-header d-flex align-items-center justify-content-between">
						<div><?= $promocao['nome']; ?></div>
						<button class="btn float-right btn_1" type="button" data-toggle="modal" data-target="#cont_add">Novo</button>
					</div>
						
					<div class="card-body pl-0 pr-0 table-responsive">
						
						<table id="example1" class="table table-striped">
						  <thead>
							<tr>
							  <th scope="col">Produto</th>
							  <th scope="col">Desconto</th>
							  <th scope="col">Preço</th>
							  <th scope="col">Ação</th>
							</tr>
						  </thead>
						  <tbody>
						  	<?php foreach ($promocoes_produtos as $p): ?>
								<tr>
									<td><?= 'pressaofarma '.$p['loja']['nome'].' - '.$p['produto']['nome'] ?></td>
									<td><?= $p['desconto'] ?>%</td>
									<td style="white-space: nowrap;">R$ <?= number_format($p['produto']['preco'] - (($p['produto']['preco'] * $p['desconto']) / 100), 2, ',','.'); ?> <span style="text-decoration: line-through;">R$ <?= number_format($p['produto']['preco'], 2, ',','.'); ?></span></td>
								  <td style="white-space: nowrap;">
								  	<button class="btn btn_1 btn_delet" id="<?= $p['id']; ?>" data-id_promocao="<?= $promocao['id']; ?>">Remover</button>
								  	<button class="btn btn_2 btn_editar" id="<?= $p['id']; ?>">Editar</button>
								  </td>
								</tr>
							<?php endforeach; ?>
						  </tbody>
						</table>
						
					</div>							
				</div>			
			</div>
		</div>
	</div>

	<!-- add promocao -->
		<div class="modal" id="cont_add">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Nova</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST" enctype="multipart/form-data" action="<?= BASE?>promocoes/add_produto">
		      	<div class="modal-body">

		        	<div class="form-group">
						<p class="mb-0">Produto: *</p>
						<select class="form-control" name="id_produto">
							<?php foreach ($produtos as $p): ?>
								<option value="<?= $p['id']; ?>">pressaofarma <?= $p['loja']; ?> - <?= $p['nome']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>

					<div class="form-group">
						<p class="mb-0">Desconto: *</p>

						<div class="d-flex form-control p-0" style="box-shadow: none; overflow: hidden;">
							<input type="number" name="desconto" class="form-control border-0" style="border-radius: 0;">
							<div class="py-2 px-3 border-0" style="background: #ccc;">%</div>
						</div>
					</div>

					<input type="hidden" name="id_promocao" value="<?= $promocao['id']; ?>">
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1" data-dismiss="modal">Fechar</button>
			        <button type="submit" class="btn btn_2">Salvar</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim add promocao -->

	<!-- deletar categoria -->
		<div class="modal" id="cont_deletar">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Deletar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      	<div class="modal-body">
		        	<div class="form-group">
						<p style="text-align: center;">Você realmende deseja deletar?</p>
					</div>
					
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1 btn_nao" data-dismiss="modal">Não</button>
			        <button type="button" class="btn btn_2 btn_sim">Sim</button>
			      </div>
		    </div>
		  </div>
		</div>
	<!-- fim deletar promocoes -->

	<!-- ditar promocao -->
		<div class="modal" id="cont_update">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Editar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST" enctype="multipart/form-data" action="<?= BASE?>promocoes/update_produto">
		      	<div class="modal-body">

		        	<div class="form-group">
						<p class="mb-0">Produto: *</p>
						<select class="form-control" name="id_produto">
							<?php foreach ($produtos as $p): ?>
								<option value="<?= $p['id']; ?>">pressaofarma <?= $p['loja']; ?> - <?= $p['nome']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>

					<div class="form-group">
						<p class="mb-0">Desconto: *</p>

						<div class="d-flex form-control p-0" style="box-shadow: none; overflow: hidden;">
							<input type="number" name="desconto" class="form-control border-0" style="border-radius: 0;">
							<div class="py-2 px-3 border-0" style="background: #ccc;">%</div>
						</div>
					</div>

					<input type="hidden" name="id_promocao" value="<?= $promocao['id']; ?>">

					<input type="hidden" name="id">
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1" data-dismiss="modal">Fechar</button>
			        <button type="submit" class="btn btn_2">Salvar</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim ditar promocao -->
</div>

