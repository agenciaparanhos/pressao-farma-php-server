<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<title>pressaofarma</title>

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo BASE ?>assets/img/icon.png">

    <!-- Bootstrap CSS -->
    <link href="<?= BASE; ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    
    <!-- Font Awesome CSS -->
    <link href="<?= BASE; ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    
    <!-- Custom CSS -->
    <link href="<?= BASE; ?>assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE ?>assets/css/ionic/ionic.app.css" rel="stylesheet">
    
    <!-- BEGIN CSS for this page -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>
    <!-- END CSS for this page -->
	<script src="<?= BASE; ?>assets/js/modernizr.min.js"></script>
    <script src="<?= BASE; ?>assets/js/jquery.min.js"></script>
    <script src="<?= BASE; ?>assets/js/moment.min.js"></script>
    <script src="<?= BASE ?>assets/js/maskedinput_2.js"></script>

    <link rel="stylesheet" href="<?php echo BASE ?>assets/css/css_views/template.css">
    <script src="<?= BASE; ?>assets/js/js_views/config.js"></script>
	
    <style type="text/css">
        #sidebar-menu > ul > li > a {padding: 12px 12px !important;}
    </style>

    <script type="text/javascript">
        var BASE = '<?= BASE; ?>';
        var BASE_PAI = '<?= BASE_PAI; ?>';
    </script>
</head>
<body class="adminbody">

    <div class="load_all" id="load_all" style="display: none; position: fixed; top: 0; left: 0; width: 100%; height: 100%; z-index: 999999; background: rgba(0, 0, 0, 0.5);">
        <img style="width: 70px; height: 70px; position: absolute; top: 50%; margin-top: -35px; left: 50%; margin-left: -35px;" src="<?= BASE ?>assets/img/load_2.gif">
      </div>

    <div class="btn_2 mt_design" id="erro_absolute" style="<?php if(!empty($_SESSION['erro_add_categoria'])) {echo 'display: block;';} else {echo 'display: none;';} ?> position: fixed; top: 70px; right: 2rem; padding: 1rem; border-radius: 5px; z-index: 5;">
        
        <?php if(!empty($_SESSION['erro_add_categoria'])) { echo $_SESSION['erro_add_categoria'];} ?>
    </div>

    <div class="btn_2 mt_design" id="erro_absolute_2" style="<?php if(!empty($_SESSION['error'])) {echo 'display: block;';} else {echo 'display: none;';} ?> position: fixed; top: 70px; right: 2rem; padding: 1rem; border-radius: 5px; z-index: 5;">
    
        <?php if(!empty($_SESSION['error'])) { echo $_SESSION['error'];} ?>
    </div>

    <audio src="<?= BASE ?>assets/new_notification.wav" id="audio_nitification" style="display: none;"></audio>

	<div id="main">

	<!-- top bar navigation -->
	<div class="headerbar">

		<!-- LOGO -->
        <div class="headerbar-left" style="background: #fff;">
			<a href="<?= BASE ?>" class="logo"><img alt="Logo" class="img_logo_p" style="display: none;" src="<?= BASE; ?>assets/img/icon.png" /><img alt="Logo" class="img_logo_g" src="<?= BASE; ?>assets/img/logo.png" /></a>
        </div>

        <nav class="navbar-custom" style="background: #333;">
            <ul class="list-inline float-right mb-0">

                <li class="list-inline-item dropdown notif">
                    <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h5 class="text-overflow"><small>Bem vindo, <strong>Admin</strong></small> </h5>
                        </div>

                        <!-- item-->
                        <a href="<?= BASE ?>login/sair" class="dropdown-item notify-item">
                            <i class="fa fa-power-off"></i> <span>Sair</span>
                        </a>
                    </div>
                </li>

            </ul>

            <ul class="list-inline menu-left mb-0">
                <li class="float-left">
                    <button class="button-menu-mobile open-left">
						<i class="fa fa-fw fa-bars"></i>
                    </button>
                </li>                        
            </ul>

        </nav>

	</div>
	<!-- End Navigation -->
	
	<!-- Left Sidebar -->
	<div class="left main-sidebar" style="position: fixed; background: #000; padding-bottom: 0;">
	<img style="position: absolute; width: 100%; height: 100%; object-fit: cover; opacity: 0.2;" src="<?= BASE ?>assets/img/bg.jpg">
		<div class="sidebar-inner leftscroll">

			<div id="sidebar-menu">
        
    			<ul>
    				<li class="submenu">
    					<a href="<?= BASE ?>" class="li_home"><i class="fa fa-fw fa-bars"></i><span> Dashboard </span> </a>
                    </li>
                    <li class="submenu">
                        <a class="li_categorias" href="<?= BASE ?>categorias"><i class="fa fa-fw fa-sitemap"></i><span> Categorias</span></a>
                    </li>
                    <li class="submenu">
                        <a class="li_banner" href="<?= BASE ?>banner"><i class="fa fa-fw fa-photo"></i><span> Slider </span></a>
                    </li>
                    <li class="submenu">
                        <a class="li_promocoes" href="<?= BASE ?>promocoes"><i class="fa fa-fw fa-trophy"></i><span> Promoções </span></a>
                    </li>
                    <li class="submenu">
                        <a class="li_push" href="<?= BASE ?>push"><i class="fa fa-fw fa-bell-o"></i><span> Push Notification </span></a>
                    </li>
                    <li class="submenu">
                        <a class="li_usuarios" href="<?= BASE ?>usuarios"><i class="fa fa-fw fa-user"></i><span> Usuários </span></a>
                    </li>
                    <li class="submenu">
                        <a class="li_lojas" href="<?= BASE ?>lojas"><i class="fa fa-fw fa-home"></i><span> pressaofarmas </span></a>
                    </li>
                    <li class="submenu">
                        <a class="li_pagamentos" href="<?= BASE ?>pagamentos"><i class="fa fa-fw fa-money"></i><span> Pagamentos </span></a>
                    </li>
                    <li class="submenu">
                        <a class="li_api_pg" href="<?= BASE ?>api_pg"><i class="fa fa-fw fa-credit-card"></i><span> API de pagamento </span></a>
                    </li>
                    <li class="submenu">
                        <a class="li_duvidas" href="<?= BASE ?>duvidas"><i class="fa fa-fw fa-question"></i><span> Principais Dúvidas </span></a>
                    </li>
                    <li class="submenu">
                        <a class="li_suporte" href="<?= BASE ?>suporte"><i class="fa fa-fw fa-question-circle"></i><span> Suporte </span></a>
                    </li>
                    <li class="submenu">
                        <a class="li_politicas" href="<?= BASE ?>politicas"><i class="fa fa-fw fa-info"></i><span> Políticas de uso </span></a>
                    </li>
                    <li class="submenu">
                        <a class="li_editar_conta" href="<?= BASE ?>editar_conta"><i class="fa fa-fw fa-pencil"></i><span> Acesso e contato </span></a>
                    </li>
                </ul>

                <div class="clearfix"></div>
            </div>
        
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- End Sidebar -->

    <div class="content-page pb-2">	
        <?php $this->loadViewInTemplate($viewName, $viewData); ?>
    </div>
	<!-- END content-page -->
    
	<footer class="footer" style="position: fixed;">
		<span class="text-right">Copyright &copy;</span>
		<span class="float-right">pressaofarma</span>
	</footer>
</div>
               
<script src="<?= BASE; ?>assets/js/popper.min.js"></script>
<script src="<?= BASE; ?>assets/js/bootstrap.min.js"></script>

<script src="<?= BASE; ?>assets/js/detect.js"></script>
<script src="<?= BASE; ?>assets/js/fastclick.js"></script>
<script src="<?= BASE; ?>assets/js/jquery.blockUI.js"></script>
<script src="<?= BASE; ?>assets/js/jquery.nicescroll.js"></script>

<!-- App js -->
<script src="<?= BASE; ?>assets/js/pikeadmin.js"></script>

<!-- BEGIN Java Script for this page -->
    <script src="<?= BASE; ?>assets/js/jquery.dataTables.min.js"></script>
    <script src="<?= BASE; ?>assets/js/dataTables.bootstrap4.min.js"></script>

    <script type="text/javascript">
        $(window).ready(function() {
            
            if($('#erro_absolute').css('display') == 'block') {
                setTimeout(function() {
                
                    $('#erro_absolute').fadeOut();
                }, 3000);
            }

            if($('#erro_absolute_2').css('display') == 'block') {
                setTimeout(function() {
                
                    $('#erro_absolute_2').fadeOut();
                }, 3000);
            }

            if($('.headerbar-left').width() < 250) {

                $(".img_logo_g").hide();
                $(".img_logo_p").show();
            } else {

                $(".img_logo_g").show();
                $(".img_logo_p").hide();
            }

            $(window).on('resize', function(event) {
                
                setTimeout(function() {

                    if($('.headerbar-left').width() < 250) {

                        $(".img_logo_g").hide();
                        $(".img_logo_p").show();
                    } else {

                        $(".img_logo_g").show();
                        $(".img_logo_p").hide();
                    }
                }, 100);
            });

            $(".button-menu-mobile").on("click", function() {

                setTimeout(function() {

                    if($('.headerbar-left').width() < 250) {

                        $(".img_logo_g").hide();
                        $(".img_logo_p").show();
                    } else {

                        $(".img_logo_g").show();
                        $(".img_logo_p").hide();
                    }
                }, 10)
            })
        });
    </script>
</body>
</html>