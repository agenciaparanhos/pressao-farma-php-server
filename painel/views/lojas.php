<style type="text/css">
	#sidebar-menu > ul > li > a.li_lojas {color: #FFF; background-color: #414d58; border-left: 2px solid #608ab3;}
</style>

<script type="text/javascript" src="<?= BASE ?>assets/js/js_views/lojas.js?<?= time(); ?>"></script>

<script type="text/javascript">
	$(window).ready(function() {
		
		$('#example1').DataTable({
			"ordering": false
		});
	});
</script>

<div class="content" style="padding-bottom: 50px;">
            
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<ol class="breadcrumb float-left">
						<li class="breadcrumb-item">
                            <h5 class="modal-title float-left m-0">pressaofarmas</h5>
                        </li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-12">						
				<div class="card mb-3">
					<div class="card-header">
						Todos os pressaofarmas 
					</div>

					<div class="card-body pl-1 pr-1">
						<div class="table-responsive">
							<table id="example1" class="table table-striped table-bordered display">
								<thead>
									<tr>
										<th scope="col">Data cadastro</th>
										<th scope="col">Logo</th>
										<th scope="col">Banner</th>
										<th scope="col">Nome</th>
										<th scope="col">Endereço</th>
										<th scope="col">Seguidores</th>
										<th scope="col">Vendas</th>
										<th scope="col">Ação</th>
									</tr>
								</thead>										
								<tbody>
									<?php foreach ($lojas as $loja): ?>
										<tr>						
											<td><?= date('d/m/Y',strtotime($loja['data'])).' '.explode(' ', $loja['data'])[1]; ?></td>
											<td>
												<img style="width: 50px; height: 50px; border-radius: 50%; object-fit: cover;" src="<?= BASE_IMG_RESIZE; ?>assets/img/lojas/<?= $loja['logo']; ?>">
											</td>
											<td>
												<img style="width: 100px; height: 50px; object-fit: cover;" src="<?= BASE_IMG_RESIZE; ?>assets/img/lojas/<?= $loja['banner']; ?>">
											</td>
											<td>pressaofarma <?= $loja['nome']; ?></td>
											<td>
												<div><strong>CEP: </strong><?= $loja['cep']; ?></div>
												<div><strong>Cidade: </strong><?= $loja['cidade'].' - '.$loja['estado']; ?></div>
												<div><strong>Bairro: </strong><?= $loja['bairro']; ?></div>
												<div><strong>Endereço: </strong><?= $loja['endereco']; ?></div>
											</td>
											<td><?= $loja['seguidores']; ?></td>
											<td><?= $loja['vendas']; ?></td>
											<td>
											  	<button class="btn btn_1 btn_delet" id="<?= $loja['id']; ?>" data-toggle="modal" data-target="#cont_deletar_usuario">Deletar</button>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>							
				</div>			
			</div>
		</div>
	</div>

	<!-- deletar loja -->
		<div class="modal fade" id="cont_deletar">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Deletar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST" action="<?= BASE?>lojas/deletar">
		      	<div class="modal-body">
		        	<div class="form-group">
						<p style="text-align: center;">Você realmende deseja deletar?</p>
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1 btn_nao" data-dismiss="modal">Não</button>
			        <button type="button" class="btn btn_2 btn_sim">Sim</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim deletar loja -->
</div>









