<style type="text/css">
	#sidebar-menu > ul > li > a.li_home {color: #FFF; background-color: #414d58; border-left: 2px solid #608ab3;}
</style>

<div class="content">
            
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<ol class="breadcrumb float-left">
						<li class="breadcrumb-item">
                            <h5 class="modal-title float-left m-0">Home</h5>
                        </li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<!-- end row -->
				
		<div class="row">
			<div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
				<div class="card-box noradius noborder bg-default">
					<i class="fa fa-shopping-cart float-right text-white"></i>
					<h6 class="text-white text-uppercase m-b-20" style="white-space: nowrap;">Produtos</h6>
					<h1 class="m-b-20 text-white counter pb-3"><?= $tot_produtos; ?></h1>
				</div>
			</div>

			<div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
				<div class="card-box noradius noborder bg-warning">
					<i class="fa fa-home float-right text-white"></i>
					<h6 class="text-white text-uppercase m-b-20">pressaofarmas</h6>
					<h1 class="m-b-20 text-white counter pb-3"><?= $tot_lojas; ?></h1>
				</div>
			</div>

			<div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
				<div class="card-box noradius noborder bg-info">
					<i class="fa fa-user float-right text-white"></i>
					<h6 class="text-white text-uppercase m-b-20">Usuários</h6>
					<h1 class="m-b-20 text-white counter mb-4"><?= $tot_usuarios; ?></h1>
				</div>
			</div>

			<div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
				<div class="card-box noradius noborder bg-danger">
					<i class="fa fa-money float-right text-white"></i>
					<h6 class="text-white text-uppercase m-b-20">Pagamentos</h6>
					<h1 class="m-b-20 text-white counter mb-4"><?= $tot_pagamentos; ?></h1>
				</div>
			</div>
		</div>
		<!-- end row -->
    </div>
	<!-- END container-fluid -->
	
	<!-- grafico 1 -->
	<div class="container-fluid mb-5">
		<div class="row">
			<div class="col-12">
				<div class="card mb-3">
					<div class="card-header">
						<h3><i class="fa fa-line-chart"></i> Cadastros</h3>
						Usuários e profissionais cadastrados este mês e no mês passado
					</div>
						
					<div class="card-body" style="height: 300px;">
						<canvas id="lineChart"></canvas>
					</div>							
					<div class="card-footer small text-muted">Atualizado a cada refresh da tela</div>
				</div><!-- end card-->					
			</div>
		</div>
	</div>
	<!-- fim grafico 1 -->
</div>
		
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script type="text/javascript">
	
	<?php $all_dias = array(); if(count(explode(',', $cadastros['dias'])) > count(explode(',', $cadastros_2['dias']))) {$all_dias = $cadastros['dias'];} else {$all_dias = $cadastros_2['dias'];} ?>

	var ctx1 = document.getElementById("lineChart").getContext('2d');
	var lineChart = new Chart(ctx1, {
		type: 'bar',
		data: {
			labels: [<?= $all_dias ?>],
			datasets: [{
					label: 'Mês atual',
					backgroundColor: '#3EB9DC',
					data: [<?= $cadastros['cadastros'] ?>]
				}, {
					label: 'Mês passado',
					backgroundColor: '#EBEFF3',
					data: [<?= $cadastros_2['cadastros'] ?>]
				}]
				
		},
		options: {
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			maintainAspectRatio: false,
			scales: {
				xAxes: [{
					stacked: true,
				}],
				yAxes: [{
					stacked: true
				}]
			}
		}
	});
</script>