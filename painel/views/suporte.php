<style type="text/css">
	#sidebar-menu > ul > li > a.li_suporte {color: #FFF; background-color: #414d58; border-left: 2px solid #608ab3;}
</style>

<script type="text/javascript" src="<?= BASE ?>assets/js/js_views/suporte.js?<?= time(); ?>"></script>

<script type="text/javascript">
	$(window).ready(function() {
		
		$('#example1, #example2').DataTable({
			"ordering": false
		});
	});
</script>

<div class="content" style="padding-bottom: 50px;">
            
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<ol class="breadcrumb float-left">
						<li class="breadcrumb-item">
                            <h5 class="modal-title float-left m-0">Suporte</h5>
                        </li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-12">						
				<div class="card mb-3">
					<div class="card-header d-flex align-items-center justify-content-between">
						Todas as mensagens
					</div>

					<div class="card-body pl-1 pr-1">
						<div class="table-responsive">
							<table id="example1" class="table table-striped table-bordered display">
								<thead>
									<tr>	
										<th scope="col">Usuário</th>
										<th scope="col">Assunto</th>
										<th scope="col">Mensagem</th>
										<th scope="col">Resposta</th>
										<th scope="col">Ação</th>
									</tr>
								</thead>										
								<tbody>
									<?php foreach ($suporte as $s): ?>
										<tr>
											<td>
												<div><strong>Nome: </strong><?= $s['usuario']['nome']; ?></div>
												<div><strong>Email: </strong><?= $s['usuario']['email']; ?></div>
												<div><strong>Telefone: </strong><?= $s['usuario']['telefone']; ?></div>
													
											</td>
											<td><?= $s['assunto']; ?></td>
											<td><?= $s['duvida']; ?></td>
											<td><?= $s['resposta']; ?></td>
											<td>
											 	<div class="d-flex">
													<button class="btn btn_1 btn_delet" id="<?= $s['id']; ?>" >Deletar</button>
													<button class="btn btn_2 btn_editar ml-2" id="<?= $s['id']; ?>">Responder</button>
												</div>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>							
				</div>			
			</div>
		</div>
	</div>

	<!-- add e -->
		<div class="modal fade" id="cont_update">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Responder</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST" action="<?= BASE?>suporte/update">
		      	<div class="modal-body">

		        	<div class="form-group">
						<p class="mb-0">Assunto:</p>
						<div class="form-control assunto" style="background: #eee;"></div>
					</div>

					<div class="form-group">
						<p class="mb-0">Mensagem:</p>
						<div class="form-control duvida" style="background: #eee;"></div>
					</div>

					<div class="form-group">
						<p class="mb-0">Resposta: <span style="color: red;">*</span></p>
						<textarea class="form-control" name="resposta"></textarea>
					</div>

					<input type="hidden" name="id">
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1" data-dismiss="modal">Fechar</button>
			        <button type="submit" class="btn btn_2 btn_salvar">Salvar</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim add profissional -->

	<!-- deletar profissional -->
		<div class="modal fade" id="cont_deletar">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Deletar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      	<div class="modal-body">
		        	<div class="form-group">
						<p style="text-align: center;">Você realmende deseja deletar?</p>
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1 btn_nao" data-dismiss="modal">Não</button>
			        <button type="button" class="btn btn_2 btn_sim">Sim</button>
			      </div>
		    </div>
		  </div>
		</div>
	<!-- fim deletar profissional -->
</div>
