<style type="text/css">
	#sidebar-menu > ul > li > a.li_pagamentos {color: #FFF; background-color: #414d58; border-left: 2px solid #608ab3;}
</style>

<script type="text/javascript" src="<?= BASE ?>assets/js/js_views/pagamentos.js?<?= time(); ?>"></script>

<script type="text/javascript">
	$(window).ready(function() {
		
		$('#example1').DataTable({
			"ordering": false
		});
	});
</script>

<div class="content" style="padding-bottom: 50px;">
            
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<ol class="breadcrumb float-left">
						<li class="breadcrumb-item">
                            <h5 class="modal-title float-left m-0">pressaofarmas</h5>
                        </li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-12">						
				<div class="card mb-3">
					<div class="card-header">
						Todos os pressaofarmas 
					</div>

					<div class="card-body pl-1 pr-1">
						<div class="table-responsive">
							<table id="example1" class="table table-striped table-bordered display">
								<thead>
									<tr>
										<th scope="col">Data cadastro</th>
										<th scope="col">Usuário</th>
										<th scope="col">pressaofarma</th>
										<th scope="col">Valor</th>
										<th scope="col">Produto</th>
										<th scope="col">Endereço de entrega</th>
										<th scope="col">Status</th>
										<th scope="col">Ação</th>
									</tr>
								</thead>										
								<tbody>
									<?php foreach ($pagamentos as $loja): ?>
										<tr>						
											<td><?= date('d/m/Y',strtotime($loja['data'])).' '.explode(' ', $loja['data'])[1]; ?></td>
											<td><?= $loja['usuario']; ?></td>
											<td>pressaofarma <?= $loja['loja']; ?></td>
											<td>R$ <?= number_format($loja['produto_preco'] + $loja['frete'], 2, ',','.'); ?></td>
											<td><?= $loja['produto_nome']; ?></td>
											<td>
												<div><strong>CEP: </strong><?= $loja['cep']; ?></div>
												<div><strong>Cidade: </strong><?= $loja['cidade'].' - '.$loja['estado']; ?></div>
												<div><strong>Bairro: </strong><?= $loja['bairro']; ?></div>
												<div><strong>Endereço: </strong><?= $loja['endereco']; ?></div>
											</td>
											<td>
												<?php if($loja['status_pg'] == '0'): ?>
													<div class="badge badge-info">Aguardando pagamento</div>
												<?php endif; ?>

												<?php if($loja['status_pg'] == '1'): ?>
													<div class="badge badge-danger">Pagamento Cancelado</div>
												<?php endif; ?>

												<?php if($loja['status_pg'] == '2'): ?>
													<div class="badge badge-success">Pago</div>
												<?php endif; ?>
											</td>
											<td>
											  	<button class="btn btn_1 btn_delet" id="<?= $loja['id']; ?>" data-toggle="modal" data-target="#cont_deletar_usuario">Deletar</button>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>							
				</div>			
			</div>
		</div>
	</div>

	<!-- deletar loja -->
		<div class="modal fade" id="cont_deletar">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Deletar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST" action="<?= BASE?>pagamentos/deletar">
		      	<div class="modal-body">
		        	<div class="form-group">
						<p style="text-align: center;">Você realmende deseja deletar?</p>
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1 btn_nao" data-dismiss="modal">Não</button>
			        <button type="button" class="btn btn_2 btn_sim">Sim</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim deletar loja -->
</div>









