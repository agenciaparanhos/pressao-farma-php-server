<style type="text/css">
	#sidebar-menu > ul > li > a.li_usuarios {color: #FFF; background-color: #414d58; border-left: 2px solid #608ab3;}
</style>

<script type="text/javascript" src="<?= BASE ?>assets/js/js_views/usuarios.js?<?= time(); ?>"></script>

<script type="text/javascript">
	$(window).ready(function() {
		
		$('#example1').DataTable({
			"ordering": false
		});
	});
</script>

<div class="content" style="padding-bottom: 50px;">
            
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<ol class="breadcrumb float-left">
						<li class="breadcrumb-item">
                            <h5 class="modal-title float-left m-0">Usuários</h5>
                        </li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-12">						
				<div class="card mb-3">
					<div class="card-header">
						<button class="btn float-right btn_1" type="button" data-toggle="modal" data-target="#cont_add_usuario">Novo usuário</button>
					</div>

					<div class="card-body pl-1 pr-1">
						<div class="table-responsive">
							<table id="example1" class="table table-striped table-bordered display">
								<thead>
									<tr>
										<th scope="col">Data cadastro</th>
										<th scope="col">Nome</th>
										<th scope="col">Telefone</th>
										<th scope="col">Email</th>
										<th scope="col">Ação</th>
									</tr>
								</thead>										
								<tbody>
									<?php foreach ($usuarios as $usuario): ?>
										<tr>						
											<td><?= date('d/m/Y',strtotime($usuario['data'])).' '.explode(' ', $usuario['data'])[1]; ?></td>
											<td><?= $usuario['nome']; ?></td>
											<td><?= $usuario['telefone']; ?></td>
											<td><?= $usuario['email']; ?></td>
											<td>
											  	<button class="btn btn_1 btn_delet" id="<?= $usuario['id']; ?>" data-toggle="modal" data-target="#cont_deletar_usuario">Deletar</button>
											  	<button class="btn btn_2 btn_editar" id="<?= $usuario['id']; ?>" data-toggle="modal" data-target="#cont_update_usuario">Editar</button>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>							
				</div>			
			</div>
		</div>
	</div>

	<!-- add usuario -->
		<div class="modal fade" id="cont_add_usuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalVerticalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Novo</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST" action="<?= BASE?>usuarios/add">
		      	<div class="modal-body">

		        	<div class="form-group">
						<p class="mb-0">Nome: <span style="color: red;">*</span></p>
						<input type="text" name="nome" class="form-control" autofocus="autofocus" required="required">
					</div>

					<div class="form-group">
						<p class="mb-0">Telefone: <span style="color: red;">*</span></p>
						<input type="text" name="telefone" class="form-control" required="required">
					</div>

					<div class="form-group">
						<p class="mb-0">Email: <span style="color: red;">*</span></p>
						<input type="email" name="email" class="form-control" required="required">
					</div>

					<div class="form-group">
						<p class="mb-0">Senha: <span style="color: red;">*</span></p>
						<input type="password" name="senha" class="form-control" required="required">
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1" data-dismiss="modal">Fechar</button>
			        <button type="submit" class="btn btn_2 btn_salvar">Salvar</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim add usuario -->

	<!-- deletar usuario -->
		<div class="modal fade" id="cont_deletar_usuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalVerticalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Deletar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST" action="<?= BASE?>usuarios/deletar">
		      	<div class="modal-body">
		        	<div class="form-group">
						<p style="text-align: center;">Você realmende deseja deletar?</p>
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1 btn_nao" data-dismiss="modal">Não</button>
			        <button type="button" class="btn btn_2 btn_sim">Sim</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim deletar usuario -->

	<!-- update usuario -->
		<div class="modal fade" id="cont_update_usuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalVerticalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Editar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>

		      <form method="POST" action="<?= BASE?>usuarios/update">

		      	<div class="modal-body">

		        	<div class="form-group">
						<p class="mb-0">Nome: <span style="color: red;">*</span></p>
						<input type="text" name="nome" class="form-control" autofocus="autofocus" required="required">
					</div>

					<div class="form-group">
						<p class="mb-0">Telefone: <span style="color: red;">*</span></p>
						<input type="text" name="telefone" class="form-control" required="required">
					</div>

					<div class="form-group">
						<p class="mb-0">Email: <span style="color: red;">*</span></p>
						<input type="email" name="email" class="form-control" required="required">
					</div>

					<div class="form-group">
						<p class="mb-0">Senha (opcional):</p>
						<input type="password" name="senha_2" class="form-control">
					</div>

					<input type="hidden" name="id">
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1" data-dismiss="modal">Fechar</button>
			        <button type="submit" class="btn btn_2">Salvar</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim update usuario -->

		<div class="modal" id="cont_add_aviso">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Novo Push</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST" enctype="multipart/form-data" action="<?= BASE ?>push/add_usuario">
		      	<div class="modal-body">

		      		<div class="form-group">
						<p class="mb-0">Imagem (opcional):</p>
						<input type="file" name="img" accept="image/png,image/jpeg,image/jpg" class="form-control">
					</div>

		        	<div class="form-group">
						<p class="mb-0">Título: <span style="color: red;">*</span></p>
						<input type="text" name="titulo" class="form-control" required="required">
					</div>
	
					<div class="form-group">
						<p class="mb-0">Mensagem: <span style="color: red;">*</span></p>
						<textarea name="msg" class="form-control" required="required"></textarea>
					</div>
					<input type="hidden" name="id">
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1" data-dismiss="modal">Fechar</button>
			        <button type="submit" class="btn btn_2 btn_salvar">Salvar</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>

		<div class="modal" id="cont_add_whatsapp">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Mensagem via Whatsapp</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST" enctype="multipart/form-data" action="<?= BASE ?>push/add_whatsapp">
		      	<div class="modal-body">
	
					<div class="form-group">
						<p class="mb-0">Mensagem: <span style="color: red;">*</span></p>
						<textarea name="msg" class="form-control" required="required"></textarea>
					</div>
					<input type="hidden" name="id">
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1" data-dismiss="modal">Fechar</button>
			        <button type="submit" class="btn btn_2 btn_salvar">Salvar</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
</div>

<script>
	$(function() {

		$('input[name="telefone"]').mask('(99) 9 9999-9999');
	});
</script>









