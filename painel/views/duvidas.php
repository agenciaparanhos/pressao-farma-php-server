<style type="text/css">
	#sidebar-menu > ul > li > a.li_duvidas {color: #FFF; background-color: #414d58; border-left: 2px solid #608ab3;}
</style>

<script type="text/javascript" src="<?= BASE ?>assets/js/js_views/duvidas.js?<?= time(); ?>"></script>

<script type="text/javascript">
	$(window).ready(function() {
		
		$('#example1').DataTable({
			"ordering": false
		});
	});
</script>

<div class="content" style="padding-bottom: 50px;">
            
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<ol class="breadcrumb float-left">
						<li class="breadcrumb-item">
                            <h5 class="modal-title float-left m-0">Principais Dúvidas</h5>
                        </li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-12">						
				<div class="card mb-3">
					<div class="card-header">
						<button class="btn float-right btn_1" type="button" data-toggle="modal" data-target="#cont_add_profissional">Nova</button>
					</div>

					<div class="card-body pl-1 pr-1">
						<div class="table-responsive">
							<table id="example1" class="table table-striped table-bordered display">
								<thead>
									<tr>	
										<th scope="col">Dúvida</th>
										<th scope="col">Resposta</th>
										<th scope="col">Ação</th>
									</tr>
								</thead>										
								<tbody>
									<?php foreach ($duvidas as $d): ?>
										<tr>
											<td><?= $d['duvida']; ?></td>
											<td><?= $d['resposta']; ?></td>
											<td style="white-space: nowrap;">
											  	<button class="btn btn_1 btn_delet" id="<?= $d['id']; ?>" >Deletar</button>
											  	<button class="btn btn_2 btn_editar" id="<?= $d['id']; ?>">Editar</button>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>							
				</div>			
			</div>
		</div>
	</div>

	<!-- add e -->
		<div class="modal fade" id="cont_add_profissional">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Nova</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST" action="<?= BASE?>duvidas/add">
		      	<div class="modal-body">

		        	<div class="form-group">
						<p class="mb-0">Dúvida: <span style="color: red;">*</span></p>
						<input type="text" name="duvida" class="form-control" required="required">
					</div>

					<div class="form-group">
						<p class="mb-0">Resposta: <span style="color: red;">*</span></p>
						
						<textarea class="form-control" name="resposta" required="required"></textarea>
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1" data-dismiss="modal">Fechar</button>
			        <button type="submit" class="btn btn_2 btn_salvar">Salvar</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim add profissional -->

	<!-- deletar profissional -->
		<div class="modal fade" id="cont_deletar">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Deletar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      	<div class="modal-body">
		        	<div class="form-group">
						<p style="text-align: center;">Você realmende deseja deletar?</p>
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1 btn_nao" data-dismiss="modal">Não</button>
			        <button type="button" class="btn btn_2 btn_sim">Sim</button>
			      </div>
		    </div>
		  </div>
		</div>
	<!-- fim deletar profissional -->

	<!-- update profissional -->
		<div class="modal" id="cont_update">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Editar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>

		      <form method="POST" enctype="multipart/form-data" action="<?= BASE?>duvidas/update">

		      	<div class="modal-body">

		        	<div class="form-group">
						<p class="mb-0">Dúvida: <span style="color: red;">*</span></p>
						<input type="text" name="duvida" class="form-control" required="required">
					</div>

					<div class="form-group">
						<p class="mb-0">Resposta: <span style="color: red;">*</span></p>
						
						<textarea class="form-control" name="resposta" required="required"></textarea>
					</div>

					<input type="hidden" name="id">
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1" data-dismiss="modal">Fechar</button>
			        <button type="submit" class="btn btn_2">Salvar</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim update profissional -->
</div>
