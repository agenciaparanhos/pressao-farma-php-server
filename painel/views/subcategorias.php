<style type="text/css">
	#sidebar-menu > ul > li > a.li_categorias {color: #FFF; background-color: #414d58; border-left: 2px solid #608ab3;}
</style>

<script type="text/javascript" src="<?= BASE ?>assets/js/js_views/subcategorias.js?<?= time(); ?>"></script>
<script type="text/javascript">
	$(window).ready(function() {
		
		$('#example1').DataTable({
			"ordering": false,
			"oLanguage": {
		        "sEmptyTable": "Nenhuma Subcategoria ainda!"
		    }
		});
	});
</script>

<div class="content" style="padding-bottom: 50px;">
            
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<ol class="breadcrumb float-left">
						<li class="breadcrumb-item">
                            <h5 class="modal-title float-left m-0">Categorias / Subcategorias</h5>
                        </li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card mb-3">
					<div class="card-header d-flex align-items-center justify-content-between">
						<?= $categoria['nome']; ?>
						<button class="btn float-right btn_1" type="button" data-toggle="modal" data-target="#cont_add">Nova</button>
					</div>
						
					<div class="card-body pl-0 pr-0 table-responsive">
						
						<table id="example1" class="table table-striped">
						  <thead>
							<tr>
							  <th scope="col">Nome</th>
							  <th scope="col">Ação</th>
							</tr>
						  </thead>
						  <tbody>
						  	<?php foreach ($sub as $p): ?>
								<tr>
									<td><?= $p['nome'] ?></td>
								  <td>
								  	<button class="btn btn_1 btn_delet" id="<?= $p['id']; ?>" data-id_categoria="<?= $categoria['id']; ?>">Deletar</button>
								  	<button class="btn btn_2 btn_editar" id="<?= $p['id']; ?>">Editar</button>
								  </td>
								</tr>
							<?php endforeach; ?>
						  </tbody>
						</table>
						
					</div>							
				</div>			
			</div>
		</div>
	</div>

	<!-- add promocao -->
		<div class="modal" id="cont_add">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Nova</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST" enctype="multipart/form-data" action="<?= BASE?>categorias/add_sub">
		      	<div class="modal-body">

		        	<div class="form-group">
						<p class="mb-0">Nome: <span style="color: red;">*</span></p>
						<input type="text" name="nome" class="form-control" required="required">
					</div>

					<input type="hidden" name="id_categoria" value="<?= $categoria['id']; ?>">
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1" data-dismiss="modal">Fechar</button>
			        <button type="submit" class="btn btn_2">Salvar</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim add promocao -->

	<!-- deletar categoria -->
		<div class="modal" id="cont_deletar">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Deletar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      	<div class="modal-body">
		        	<div class="form-group">
						<p style="text-align: center;">Você realmende deseja deletar?</p>
					</div>
					
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1 btn_nao" data-dismiss="modal">Não</button>
			        <button type="button" class="btn btn_2 btn_sim">Sim</button>
			      </div>
		    </div>
		  </div>
		</div>
	<!-- fim deletar categorias -->

	<!-- ditar promocao -->
		<div class="modal" id="cont_update">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Editar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST" enctype="multipart/form-data" action="<?= BASE?>categorias/update_sub">
		      	<div class="modal-body">

		        	<div class="form-group">
						<p class="mb-0">Nome: <span style="color: red;">*</span></p>
						<input type="text" name="nome" class="form-control" required="required">
					</div>

					<input type="hidden" name="id_categoria" value="<?= $categoria['id']; ?>">

					<input type="hidden" name="id">
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1" data-dismiss="modal">Fechar</button>
			        <button type="submit" class="btn btn_2">Salvar</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim ditar promocao -->
</div>

