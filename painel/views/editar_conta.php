
<style type="text/css">
	#sidebar-menu > ul > li > a.li_editar_conta{color: #FFF; background-color: #414d58; border-left: 2px solid #608ab3;}
</style>

<div class="content">
    
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<ol class="breadcrumb float-left">
						<li class="breadcrumb-item">
                            <h5 class="modal-title float-left m-0">Acesso e contato</h5>
                        </li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-12">
				<form method="POST" action="<?= BASE ?>editar_conta/editar">
					<div class="col-12 col-md-6 float-left">
						<div class="form-group">
							<p class="mb-0">Nome:</p>
							<input type="text" name="nome" class="form-control" required="required" value="<?= $admin['nome']; ?>">
						</div>

						<div class="form-group">
							<p class="mb-0">Email de login:</p>
							<input type="email" name="email" class="form-control" required="required" value="<?= $admin['email']; ?>">
						</div>

						<div class="form-group">
							<p class="mb-0">Email de contato:</p>
							<input type="email" name="email_contato" class="form-control" required="required" value="<?= $config['email_contato']; ?>">
						</div>
					</div>

					<div class="col-12 col-md-6 float-left">
						<div class="form-group">
							<p class="mb-0">Senha:</p>
							<input type="password" name="senha" class="form-control" required="required">
						</div>

						<div class="form-group">
							<p class="mb-0">Repetir senha:</p>
							<input type="password" name="senha_2" class="form-control" required="required">
						</div>

						<div class="alert alert-warning alert_1" style="text-align: center; display: none;">
							
						</div>

						<button class="btn float-right btn_2" id="btn_salvar">Salvar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(window).ready(function() {
		
		$('#btn_salvar').on('click', function(e) {
			
			var senha = $.trim($('input[name="senha"]').val());
			var senha_2 = $.trim($('input[name="senha_2"]').val());

			if(senha == '' || senha == '' || senha != senha_2) {

				e.preventDefault();
				$('form').find('.alert_2').hide();
				$('form').find('.alert_1').html('<strong>Oops: </strong>As senhas não correspondem ou estão em branco!').fadeIn();
			}
		})
	});
</script>