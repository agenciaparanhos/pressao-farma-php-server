<style type="text/css">
	#sidebar-menu > ul > li > a.li_banner {color: #FFF; background-color: #414d58; border-left: 2px solid #608ab3;}

	#example1_filter, #example2_filter, #example3_filter {display: none !important;}
</style>

<script type="text/javascript" src="<?= BASE ?>assets/js/js_views/banner.js?<?= time(); ?>"></script>
<script type="text/javascript">
	$(window).ready(function() {
		
		$('#example1, #example2, #example3').DataTable({
			"ordering": false,
			"oLanguage": {
		        "sEmptyTable": "Nenhuma Imagem ainda!"
		    }
		});
	});
</script>

<div class="content" style="padding-bottom: 50px;">
            
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<ol class="breadcrumb float-left">
						<li class="breadcrumb-item">
                            <h5 class="modal-title float-left m-0">Slider</h5>
                        </li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card mb-3">
					<div class="card-header d-flex align-items-center justify-content-between">
						Slider Inicial
						<button class="btn float-right btn_1" type="button" data-toggle="modal" data-target="#cont_add_promocao">Nova Imagem</button>
					</div>
						
					<div class="card-body pl-0 pr-0">
						
						<table id="example1" class="table table-striped">
						  <thead>
							<tr>
							  <th scope="col">Imagem</th>
							  <th scope="col">Detalhes</th>
							  <th scope="col">Ação</th>
							</tr>
						  </thead>
						  <tbody>
						  	<?php foreach ($banner as $p): ?>
								<tr>
								  <td>
								  	<div class="" style="position: relative; width: 150px; padding-top: 75px;">
								  		<img class="w-100 h-100" style="position: absolute; top: 0; left: 0; border-radius: 5px; object-fit: cover;" src="<?= BASE_IMG_RESIZE ?>assets/img/produtos/<?= $p['img']; ?>?w=400">
								  	</div>
								  </td>
								  <td>
								  	<div>
								  		<strong>Tipo: </strong> 
								  		<?php 
								  			if($p['tipo'] == 'produto') {

								  				echo 'Produto';
								  			} else if($p['tipo'] == 'loja') {

								  				echo 'pressaofarma';
								  			} else if($p['tipo'] == 'link') {

								  				echo 'Link';
								  			} else {

								  				echo 'Sem ação';
								  			}
								  		 ?>
								  	</div>

								  	<div>
								  		<?php 
								  			if($p['tipo'] == 'produto') {

								  				echo '<strong>Produto: </strong>  pressaofarma '.$p['loja'].' - '.$p['produto'];
								  			} else if($p['tipo'] == 'loja') {

								  				echo '<strong>pressaofarma: </strong> pressaofarma '.$p['loja'];
								  			} else if($p['tipo'] == 'link') {

								  				echo '<strong>Link: </strong> '.$p['link'];
								  			}
								  		 ?>
								  	</div>
								  </td>
								  <td style="white-space: nowrap;">
								  	<button class="btn btn_1 btn_delet" id="<?= $p['id']; ?>">Deletar</button>
								  	<button class="btn btn_2 btn_editar" id="<?= $p['id']; ?>">Editar</button>
								  </td>
								</tr>
							<?php endforeach; ?>
						  </tbody>
						</table>
						
					</div>							
				</div>			
			</div>
		</div>
	</div>

	<!-- add promocao -->
		<div class="modal fade" id="cont_add_promocao">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Nova</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST" enctype="multipart/form-data" action="<?= BASE?>banner/add">
		      	<div class="modal-body">

		        	<div class="form-group">
						<p class="mb-0">Imagem (png, jpeg ou jpg): <span style="color: red;">*</span></p>
						<input type="file" name="img" accept="image/png,image/jpeg,image/jpg" class="form-control" required="required">
					</div>
					<div class="form-group">
						<p class="mb-0">Tipo:</p>
						<select class="form-control" name="tipo">
							<option value="link">Link</option>
							<option value="loja">pressaofarma</option>
							<option value="produto">Produto</option>
							<option value="nada">Sem ação</option>
						</select>
					</div>
					<div class="form-group cont_opt cont_link">
						<p class="mb-0">Link: *</p>
						<input type="text" name="link" class="form-control" placeholder="https://exemple.com.br">
					</div>
					<div class="form-group cont_opt cont_produtos" style="display: none;">
						<p class="mb-0">Produto: *</p>
						<select class="form-control" name="id_produto">
							<?php foreach ($produtos as $p): ?>
								<option value="<?= $p['id']; ?>">pressaofarma <?= $p['loja']; ?> - <?= $p['nome']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group cont_opt cont_lojas" style="display: none;">
						<p class="mb-0">pressaofarma: *</p>
						<select class="form-control" name="id_loja">
							<?php foreach ($lojas as $l): ?>
								<option value="<?= $l['id']; ?>">pressaofarma <?= $l['nome']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1" data-dismiss="modal">Fechar</button>
			        <button type="submit" class="btn btn_2">Salvar</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim add promocao -->

	<!-- deletar categoria -->
		<div class="modal" id="cont_deletar">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Deletar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      	<div class="modal-body">
		        	<div class="form-group">
						<p style="text-align: center;">Você realmende deseja deletar?</p>
					</div>
					
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1 btn_nao" data-dismiss="modal">Não</button>
			        <button type="button" class="btn btn_2 btn_sim">Sim</button>
			      </div>
		    </div>
		  </div>
		</div>
	<!-- fim deletar banner -->

	<!-- add promocao -->
		<div class="modal fade" id="cont_update">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Editar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST" enctype="multipart/form-data" action="<?= BASE?>banner/update">
		      	<div class="modal-body">

		        	<div class="form-group">
						<p class="mb-0">Editar (Opcional):</p>
						<input type="file" name="img" accept="image/png,image/jpeg,image/jpg" class="form-control">
					</div>
					<div class="form-group">
						<p class="mb-0">Tipo:</p>
						<select class="form-control" name="tipo">
							<option value="link">Link</option>
							<option value="loja">pressaofarma</option>
							<option value="produto">Produto</option>
							<option value="nada">Sem ação</option>
						</select>
					</div>
					<div class="form-group cont_opt cont_link">
						<p class="mb-0">Link: *</p>
						<input type="text" name="link" class="form-control" placeholder="https://exemple.com.br">
					</div>
					<div class="form-group cont_opt cont_produtos" style="display: none;">
						<p class="mb-0">Produto: *</p>
						<select class="form-control" name="id_produto">
							<?php foreach ($produtos as $p): ?>
								<option value="<?= $p['id']; ?>">pressaofarma <?= $p['loja']; ?> - <?= $p['nome']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group cont_opt cont_lojas" style="display: none;">
						<p class="mb-0">pressaofarma: *</p>
						<select class="form-control" name="id_loja">
							<?php foreach ($lojas as $l): ?>
								<option value="<?= $l['id']; ?>">pressaofarma <?= $l['nome']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<input type="hidden" name="id">
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1" data-dismiss="modal">Fechar</button>
			        <button type="submit" class="btn btn_2">Salvar</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim add promocao -->
</div>

