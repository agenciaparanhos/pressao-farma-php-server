<style type="text/css">
	#sidebar-menu > ul > li > a.li_push {color: #FFF; background-color: #414d58; border-left: 2px solid #608ab3;}
</style>

<script type="text/javascript" src="<?= BASE ?>assets/js/js_views/push.js"></script>

<div class="content">
            
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<ol class="breadcrumb float-left">
						<li class="breadcrumb-item">
                            <h5 class="modal-title float-left m-0">Push notification</h5>
                        </li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div> 
		<!-- end row -->
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-12">						
				<div class="card mb-3">
					<div class="card-header">
						<button class="btn float-right btn_1" type="button" data-toggle="modal" data-target="#cont_add_aviso">Novo Push</button>
					</div>

					<div class="card-body">

						<?php if($avisos == array()): ?>
							<div class="alert text-center d-block mb-0">
								Nenhum Push ainda!
							</div>
						<?php endif; ?>

						<?php if($avisos != array()): ?>
							<div class="table-responsive">
								<table id="example1" class="table table-striped table-bordered display">
									<thead>
										<tr>
											<th scope="col">Imagem</th>
											<th scope="col">Título</th>
											<th scope="col">Mensagem</th>
											<th scope="col">Data criação</th>
											<th scope="col">Ação</th>
										</tr>
									</thead>										
									<tbody>
										<?php foreach ($avisos as $aviso): ?>
											<tr>
												<td>
													<?php if($aviso['img'] != ''): ?>

														<img style="width: 50px; height: 35px; object-fit: cover; border-radius: 5px; background: #fff;" src="<?= BASE_PAI ?>assets/img/push/<?= $aviso['img']; ?>">

													<?php endif; ?>
												</td>
												<td><?= $aviso['titulo']; ?></td>
												<td><?= $aviso['msg']; ?></td>
												<td><?= $aviso['data']; ?></td>
												<td>
												  	<button class="btn btn_1 btn_delet" id="<?= $aviso['id']; ?>" data-toggle="modal" data-target="#cont_deletar_aviso">Deletar</button>
												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- add aviso -->
		<div class="modal" id="cont_add_aviso" tabindex="-1" role="dialog" aria-labelledby="exampleModalVerticalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Novo Push</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST" enctype="multipart/form-data" action="<?= BASE ?>push/add">
		      	<div class="modal-body">

		      		<div class="form-group">
						<p class="mb-0">Imagem (opcional):</p>
						<input type="file" name="img" accept="image/png,image/jpeg,image/jpg" class="form-control">
					</div>

		        	<div class="form-group">
						<p class="mb-0">Título: <span style="color: red;">*</span></p>
						<input type="text" name="titulo" class="form-control" required="required">
					</div>
	
					<div class="form-group">
						<p class="mb-0">Mensagem: <span style="color: red;">*</span></p>
						<textarea name="msg" class="form-control" required="required"></textarea>
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1" data-dismiss="modal">Fechar</button>
			        <button type="submit" class="btn btn_2 btn_salvar">Salvar</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim add aviso -->

	<!-- deletar aviso -->
		<div class="modal" id="cont_deletar_aviso" tabindex="-1" role="dialog" aria-labelledby="exampleModalVerticalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h5>Deletar Push</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <form method="POST">
		      	<div class="modal-body">
		        	<div class="form-group">
						<p style="text-align: center;">Você realmende deseja deletar este Push?</p>
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn_1 btn_nao" data-dismiss="modal">Não</button>
			        <button type="button" class="btn btn_2 btn_sim">Sim</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim deletar aviso -->
</div>












