
<style type="text/css">
	#sidebar-menu > ul > li > a.li_api_pg {color: #FFF; background-color: #414d58; border-left: 2px solid #608ab3;}
</style>

<div class="content">
    
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<ol class="breadcrumb float-left">
						<li class="breadcrumb-item">
                            <h5 class="modal-title float-left m-0">API de pagamento</h5>
                        </li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 mb-3">
				<div class="card">
					<div class="card-header">
						Info
					</div>
					<div class="card-body">
						<div>1 - Crie uma conta no <a target="_blank" href="https://www.mercadopago.com.br/">https://www.mercadopago.com.br/</a></div>
						<div>2 - Acesse <a target="_blank" href="https://applications.mercadopago.com.br/">https://applications.mercadopago.com.br/</a></div>
						<div>3 - Clique em "Create new application"</div>
						<div>4 - Preencha os campos da seguinte forma</div>
							<div class="pl-3"><strong>Name</strong>: nome da sua loja</div>
							<div class="pl-3"><strong>Short name</strong>: nome da sua loja sem espaço e sem caracteres especiais</div>
							<div class="pl-3"><strong>Description</strong>: breve descrição do seu estabelecimento</div>
							<div class="pl-3"><strong>Redirect URI</strong>: <?= BASE_PAI ?>app/mp/success_mp</div>
							<div class="pl-3"><strong>Scopes</strong>: Marque "read" e "write"</div>
							<div class="pl-3"><strong>Notifications Callback URL</strong>: <?= BASE_PAI ?>app/mp/notificacao_mp</div>
						<div>5 - Clique em "Create application"</div>
						<div>6 - Na página seguinte você verá seu "App ID" e seu "Secret Key"</div>
						<div>7 - Pronto, agora seus clientes poderam fazer pagamentos com o mercadoPago e você receberá direto na sua conta do mercadoPago.</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 mb-5">
				<div class="card">
					<form method="POST" class="card" action="<?= BASE ?>api_pg/update">
						<div class="card-header">
							Dados da API
						</div>
						<div class="card-body">
							<div class="col-12 col-md-6 float-left">
								<div class="form-group">
									<p class="mb-0">App ID:</p>
									<input type="text" name="MP_AppID" class="form-control" required="required" value="<?= $config['MP_AppID']; ?>">
								</div>
							</div>

							<div class="col-12 col-md-6 float-left">
								<div class="form-group">
									<p class="mb-0">Secret Key:</p>
									<input type="text" name="MP_SecretKey" class="form-control" required="required" value="<?= $config['MP_SecretKey']; ?>">
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn float-right btn_2" id="btn_salvar">Salvar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>