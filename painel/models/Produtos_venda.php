<?php
class Produtos_venda extends model {

	public function get_tot() {

		$sql = "SELECT count(id) as tot FROM produtos_venda";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();

			$sql = $sql['tot'];
		}

		return $sql;
	}

	public function get() {

		$sql = "SELECT * FROM produtos_venda ORDER BY data DESC";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetchAll();

			foreach ($sql as $key => $value) {
				
				$s = "SELECT nome FROM usuarios WHERE id = '".$value['id_usuario']."'";
				$s = $this->db->query($s);

				if($s->rowCount() > 0) {

					$sql[$key]['usuario'] = $s->fetch()['nome'];
				} else {

					$sql[$key]['usuario'] = 'Usuário deletado';	
				}

				$s = "SELECT nome FROM lojas WHERE id = '".$value['id_loja']."'";
				$s = $this->db->query($s);

				if($s->rowCount() > 0) {

					$sql[$key]['loja'] = $s->fetch()['nome'];
				}
			}
		} else {

			$sql = array();
		}

		return $sql;
	}

	public function deletar($id) {

		$sql = "DELETE FROM produtos_venda WHERE id = '$id'";
		$this->db->query($sql);

		$_SESSION['error'] = 'Deletado com sucesso!';

		header('location: '.BASE.'pagamentos');
	}
}











