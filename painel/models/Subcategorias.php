<?php
class SubCategorias extends model {

	public function add($nome, $id_categoria) {

		$sql = "SELECT * FROM subcategorias WHERE nome = '$nome' AND id_categoria = '$id_categoria'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() == 0) {

			$sql = "INSERT INTO subcategorias SET nome = '$nome', id_categoria = '$id_categoria'";
			$this->db->query($sql);

			$_SESSION['error'] = "Adicionado com sucesso!";

			header('location: '.BASE.'categorias/sub/'.$id_categoria);
		} else {

			$_SESSION['error'] = "A Subcategoria digitado já existe!";
			header('location: '.BASE.'categorias/sub/'.$id_categoria);
		}
	}

	public function get($id) {

		$sql = "SELECT * FROM subcategorias WHERE id_categoria = '$id' ORDER BY nome";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetchAll();
		}

		return $sql;
	}

	public function deletar($id, $id_categoria) {

		$sql = "DELETE FROM subcategorias WHERE id = '$id'";
		$sql = $this->db->query($sql);

		$_SESSION['error'] = 'Deletado com sucesso!';

		header('location: '.BASE.'categorias/sub/'.$id_categoria);
	}

	public function get_dados($id) {

		$sql = "SELECT * FROM subcategorias WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();

			echo json_encode($sql);
		} else {

			echo json_encode('1');
		}
	}

	public function update($nome, $id, $id_categoria) {

		$sql = "SELECT * FROM subcategorias WHERE nome = '$nome' AND id_categoria = '$id_categoria' AND id != '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() == 0) {

			$sql = "UPDATE subcategorias SET nome = '$nome' WHERE id = '$id'";
			$this->db->query($sql);

			$_SESSION['error'] = "Editado com sucesso!";

			header('location: '.BASE.'categorias/sub/'.$id_categoria);
		} else {

			$_SESSION['error'] = "A Categoria digitado já existe!";
			header('location: '.BASE.'categorias/sub/'.$id_categoria);
		}
	}
}











