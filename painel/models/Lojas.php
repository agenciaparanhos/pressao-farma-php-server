<?php
class Lojas extends model {

	public function get() {

		$sql = "SELECT * FROM lojas ORDER BY nome";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetchAll();

			foreach ($sql as $key => $value) {
				
				$s = "SELECT count(id) as tot FROM loja_seguidores WHERE id_loja = '".$value['id']."'";
				$s = $this->db->query($s);

				if($s->rowCount() > 0) {

					$sql[$key]['seguidores'] = $s->fetch()['tot'];
				} 

				$s = "SELECT count(id) as tot FROM produtos_venda WHERE id_loja = '".$value['id']."' AND status_pg = '2'";
				$s = $this->db->query($s);

				if($s->rowCount() > 0) {

					$sql[$key]['vendas'] = $s->fetch()['tot'];
				} 
			}
		}

		return $sql;
	}

	public function deletar($id) {

		$sql = "DELETE FROM lojas WHERE id = '$id'";
		$this->db->query($sql);

		$sql = "DELETE FROM produtos_venda WHERE id_loja = '$id'";
		$this->db->query($sql);

		$sql = "DELETE FROM loja_seguidores WHERE id_loja = '$id'";
		$this->db->query($sql);

		$sql = "DELETE FROM produtos WHERE id_loja = '$id'";
		$this->db->query($sql);

		$_SESSION['error'] = 'Deletado com sucesso!';

		header('location: '.BASE.'lojas');
	}

	public function get_tot() {

		$sql = "SELECT count(id) as tot FROM lojas";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$tot = $sql->fetch()['tot'];
		}

		return $tot;
	}
}












