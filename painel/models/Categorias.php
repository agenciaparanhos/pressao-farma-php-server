<?php
class Categorias extends model {

	public function add($nome) {

		$sql = "SELECT * FROM categorias WHERE nome = '$nome'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() == 0) {

			$sql = "INSERT INTO categorias SET nome = '$nome'";
			$this->db->query($sql);

			$_SESSION['error'] = "Adicionado com sucesso!";

			header('location: '.BASE.'categorias');
		} else {

			$_SESSION['error'] = "A Categoria digitado já existe!";
			header('location: '.BASE.'categorias');
		}
	}

	public function get() {

		$sql = "SELECT * FROM categorias ORDER BY nome";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetchAll();

			foreach ($sql as $key => $value) {
				
				$s = "SELECT count(*) as tot FROM subcategorias WHERE id_categoria = '".$value['id']."'";
				$s = $this->db->query($s);

				if($s->rowCount() > 0) {

					$s = $s->fetch();

					$sql[$key]['tot_sub'] = $s['tot'];
				}
			}
		}

		return $sql;
	}

	public function deletar($id) {

		$sql = "DELETE FROM categorias WHERE id = '$id'";
		$sql = $this->db->query($sql);

		$_SESSION['error'] = 'Deletado com sucesso!';

		header('location: '.BASE.'categorias');
	}

	public function get_dados($id) {

		$sql = "SELECT * FROM categorias WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();

			echo json_encode($sql);
		} else {

			echo json_encode('1');
		}
	}

	public function get_one($id) {

		$sql = "SELECT * FROM categorias WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();
		}

		return $sql;
	}

	public function update($nome, $id) {

		$sql = "SELECT * FROM categorias WHERE nome = '$nome' AND id != '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() == 0) {

			$sql = "UPDATE categorias SET nome = '$nome' WHERE id = '$id'";
			$this->db->query($sql);

			$_SESSION['error'] = "Editado com sucesso!";

			header('location: '.BASE.'categorias');
		} else {

			$_SESSION['error'] = "A Categoria digitado já existe!";
			header('location: '.BASE.'categorias');
		}
	}

	public function get_tot() {

		$sql = "SELECT count(*) as tot FROM categorias";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();

			$sql = $sql['tot'];
		}

		return $sql;
	}
}











