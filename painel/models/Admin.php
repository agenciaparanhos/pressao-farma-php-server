<?php 
class Admin extends model {

	public function is_logged() {
		
		if(isset($_SESSION['AppLanches']) && !empty($_SESSION['AppLanches'])) {
			return true;
		} else {
			return false;
		}
	}

	public function logar($email, $senha) {

		$sql = "SELECT * FROM admin WHERE email = '$email' AND senha = '$senha'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();

			$_SESSION['AppLanches'] = $sql['id'];

			echo json_encode('5');
		} else {

			echo json_encode('2');
		}
	}

	public function get_dados_admin($id) {

		$sql = "SELECT * FROM admin WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();
		}

		return $sql;
	}

	public function update_admin($email, $nome, $senha, $id) {

		$sql = "SELECT * FROM admin WHERE email = ? AND id != ?";
		$sql = $this->db->prepare($sql);

		$sql->execute(array($email, $id));

		if($sql->rowCount() == 0) {

			$sql = "UPDATE admin SET email = ?, nome = ?, senha = ? WHERE id = ?";
			$sql = $this->db->prepare($sql);

			$sql->execute(array($email, $nome, $senha, $id));

			$_SESSION['erro_update_conta'] = 'Editado com sucesso!';
		} else {

			$_SESSION['erro_update_conta'] = '<strong>Oops: </strong>Você não pode usar o email digitado!';
		}

		header('location: '.BASE.'editar_conta');
	}
}