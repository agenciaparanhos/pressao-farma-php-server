<?php 
class Avisos extends model {

	public function get_avisos() {

		$sql = "SELECT * FROM avisos WHERE id_usuario = '0'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetchAll();

			$d_h = new dadaHelpers();

			foreach ($sql as $key => $value) {
				
				$sql[$key]['data'] = $d_h->get_data($value['data']);
			}
		} else {

			$sql = array();
		}

		return $sql;
	}

	public function add($img_name, $titulo, $msg) {

		$registrationIds = array();

		$sql = "SELECT * FROM usuarios WHERE push_app_key != ''";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetchAll();

			foreach ($sql as $key => $value) {

				$registrationIds[] = $value['push_app_key'];
			}
		}

		$data_ = array('tipo' => 'new_aviso', 'msg' => $msg, 'titulo' => $titulo, 'img' => $img_name);

		$push = new push_notificationHelpers();
		$push->sendMessage($msg, $data_, $img_name, $registrationIds);

		$time_identificacao = time();

		$sql_2 = "INSERT INTO avisos SET img = '$img_name', titulo = '$titulo', msg = '$msg', data = NOW()";
		$this->db->query($sql_2);

		header('location: '.BASE.'push');
	}

	public function add_usuario($img_name, $titulo, $msg, $id) {

		$registrationIds = array();

		$sql = "SELECT * FROM usuarios WHERE push_app_key != '' AND id = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetchAll();

			foreach ($sql as $key => $value) {

				$registrationIds[] = $value['push_app_key'];
			}
		}

		$data_ = array('tipo' => 'new_aviso', 'msg' => $msg, 'titulo' => $titulo, 'img' => $img_name);

		$push = new push_notificationHelpers();
		$push->sendMessage($msg, $data_, $img_name, $registrationIds);

		$_SESSION['error'] = 'Enviado com sucesso!';
		header('location: '.BASE.'usuarios');
	}

	public function deletar($id) {

		$sql = "DELETE FROM avisos WHERE id = '$id'";
		$this->db->query($sql);

		header('location: '.BASE.'push');
	}
}















