<?php
class Banner extends model {

	public function add($img, $id_produto, $id_loja, $link, $tipo) {

		$sql = "INSERT INTO banner SET img = '$img', link = '$link', id_produto = '$id_produto', id_loja = '$id_loja', tipo = '$tipo', data = NOW()";
		$this->db->query($sql);

		$_SESSION['error'] = "Adicionado com sucesso!";
		header('location: '.BASE.'banner');
	}

	public function get() {

		$sql = "SELECT * FROM banner";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetchAll();

			foreach ($sql as $key => $value) {
				
				if($value['tipo'] == 'produto' || $value['tipo'] == 'loja') {

					$s = "SELECT nome FROM lojas WHERE id = '".$value['id_loja']."'";
					$s = $this->db->query($s);

					if($s->rowCount() > 0) {

						$sql[$key]['loja'] = $s->fetch()['nome'];

						$s = "SELECT nome FROM produtos WHERE id = '".$value['id_produto']."'";
						$s = $this->db->query($s);

						if($s->rowCount() > 0) {

							$sql[$key]['produto'] = $s->fetch()['nome'];
						} else {

							unset($sql[$key]);
						}
					} else {

						unset($sql[$key]);
					}
				}
			}
		} else {

			$sql = array();
		}

		return $sql;
	}

	public function deletar($id) {

		$sql = "SELECT * FROM banner WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();

			$img_old = $sql['img'];

			$sql = "DELETE FROM banner WHERE id = '$id'";
			$sql = $this->db->query($sql);

			@unlink('../assets/img/produtos/'.$img_old);

			$_SESSION['error'] = 'Deletado com sucesso!';
		}

		header('location: '.BASE.'banner');
	}

	public function get_dados($id) {

		$sql = "SELECT * FROM banner WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();

			echo json_encode($sql);
		}
	}

	public function update_img($img, $id) {

		$sql = "SELECT * FROM banner WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();

			@unlink('../assets/img/produtos/'.$sql['img']);

			$sql = "UPDATE banner SET img = '$img' WHERE id = '$id'";
			$this->db->query($sql);
		}
	}

	public function update($id, $id_produto, $id_loja, $tipo, $link) {

		$sql = "UPDATE banner SET link = '$link', id_produto = '$id_produto', id_loja = '$id_loja', tipo = '$tipo' WHERE id = '$id'";
		$this->db->query($sql);

		$_SESSION['error'] = "Editado com sucesso!";
		header('location: '.BASE.'banner');
	}
}











