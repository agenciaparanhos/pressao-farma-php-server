<?php
class Suporte extends model {

	public function get() {

		$sql = "SELECT * FROM suporte";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetchAll();

			foreach ($sql as $key => $value) {
				
				$s = "SELECT nome, email, telefone FROM usuarios WHERE id = '".$value['id_usuario']."' OR nome != ''";
				$s = $this->db->query($s);

				if($s->rowCount() > 0) {

					$sql[$key]['usuario'] = $s->fetch();
				} else {

					unset($sql[$key]);
				}
			}
		}

		return $sql;
	}

	public function deletar($id) {

		$sql = "DELETE FROM suporte WHERE id = '$id'";
		$sql = $this->db->query($sql);

		header('location: '.BASE.'suporte');
	}

	public function get_dados($id) {

		$sql = "SELECT * FROM suporte WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();

			echo json_encode($sql);
		} else {

			echo json_encode('1');
		}
	}

	public function update($resposta, $id) {

		$sql = "SELECT * FROM suporte WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$s = $sql->fetch();

			$sql = "SELECT email FROM usuarios WHERE id = '".$s['id_usuario']."'";
			$sql = $this->db->query($sql);

			if($sql->rowCount() > 0) {

				$email = $sql->fetch()['email'];

				if(ENVIRONMENT != 'development') {

					$html = "Assunto: ".$s['assunto']."\r\n";
					$html = "Dúvida: ".$s['duvida']."\r\n";
					$html = "Resposta: ".$resposta."\r\n";

	                $headers = 'From: '.$dados_config['email_contato'].''."\r\n";
	                $headers .= 'Reply-To: '.$dados_config['email_contato']."\r\n";
	                $headers .= 'X-Mailer: PHP/'.phpversion();

	                mail($email, "Resposta a sua solicitação de suporte", $html, $headers);
				}
			}

			$sql = "UPDATE suporte SET resposta = '$resposta' WHERE id = '$id'";
			$sql = $this->db->query($sql);

			$_SESSION['error'] = "Salvo com sucesso!";
		}

		header('location: '.BASE.'suporte');
	}

	public function get_tot() {

		$sql = "SELECT count(*) as tot FROM suporte";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();

			$sql = $sql['tot'];
		}

		return $sql;
	}
}











