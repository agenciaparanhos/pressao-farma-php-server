<?php
class Usuarios extends model {

	public function get_usuarios() {

		$sql = "SELECT * FROM usuarios ORDER BY nome";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetchAll();
		}

		return $sql;
	}

	public function add_usuario($telefone, $nome, $senha, $email) {

        $sql = "SELECT * FROM usuarios WHERE email = '$email'";
        $sql = $this->db->query($sql);

        if($sql->rowCount() == 0) {

			$token = md5(time().rand(0,999));

			$sql = "INSERT INTO usuarios SET nome = '$nome', telefone = '$telefone', email = '$email', token = '$token', senha = '$senha', img_perfil = 'user_m.png', data = NOW()";
			$this->db->query($sql);

			$id = $this->db->lastInsertId();

			$_SESSION['error'] = 'Cadastrado com sucesso!';
			header('location: '.BASE.'usuarios');
        } else {

            $_SESSION['error'] = 'O Email digitado já está sendo usando!';
        	header('location: '.BASE.'usuarios');
        }
	}
	
	public function update_senha($id, $senha) {

		$sql = "UPDATE usuarios SET senha = '$senha' WHERE id = '$id'";
		$this->db->query($sql);
	}

	public function deletar($id) {

		$sql = "DELETE FROM usuarios WHERE id = '$id'";
		$this->db->query($sql);

		$sql = "SELECT * FROM lojas WHERE id_usuario = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();

			$sql = "DELETE FROM produtos WHERE id_loja = '".$sql['id']."'";
			$this->db->query($sql);
		}

		$sql = "DELETE FROM lojas WHERE id_usuario = '$id'";
		$this->db->query($sql);

		$sql = "DELETE FROM produtos_venda WHERE id_usuario = '$id'";
		$this->db->query($sql);

		$_SESSION['error'] = 'Deletado com sucesso!';

		header('location: '.BASE.'usuarios');
	}

	public function update($telefone, $nome, $id, $email) {

		$sql = "SELECT * FROM usuarios WHERE email = '$email' AND id != '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() == 0) {

			$sql = "UPDATE usuarios SET email = '$email', telefone = '$telefone', nome = '$nome' WHERE id = '$id'";
			$this->db->query($sql);

			$_SESSION['error'] = 'Editado com sucesso!';
    		header('location: '.BASE.'usuarios');
		} else {

			$_SESSION['error'] = 'O Email digitado já está sendo usando!';
        	header('location: '.BASE.'usuarios');
		}
	}

	public function get_tot_usuarios() {

		$sql = "SELECT count(*) as tot FROM usuarios";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch()['tot'];
		}

		return $sql;
	}

	public function get_cadastros() {

		$dados = array();

		$current = $this->db->prepare("SELECT NOW() AS now");
		$current->execute();
		$row = $current->fetchObject();
		$timestamp = $row->now;

		list($data, $time) = explode(' ', $timestamp);

		list($ano, $mes, $dia) = explode('-', $data);

        $sql = "SELECT DAY(data) as dia, COUNT(id) as views FROM usuarios WHERE MONTH(data) = '{$mes}' AND YEAR(data) = '{$ano}' GROUP BY DAY(data)";
        $query = $this->db->query($sql);
       
        if($query->rowCount() > 0) {
            $result = $query->fetchAll(PDO::FETCH_OBJ);

            foreach($result as $res):
                $dados[$res->dia] = $res->views;
            endforeach;
        }

        $dias_do_mes = $this->_dias_do_mes($mes, $ano);
        $final = array_replace($dias_do_mes, $dados);
        
        $dias = '';
        $cadastros = '';
        foreach ($final as $key => $value) {
        	
        	$dias .= $key.',';
        	$cadastros .= $value.',';
        }

        return array('dias' => $dias, 'cadastros' => $cadastros);
	}

	public function get_cadastros_2() {

		$dados = array();

		$current = $this->db->prepare("SELECT DATE_SUB(NOW(), INTERVAL 1 MONTH) AS now");
		$current->execute();
		$row = $current->fetchObject();
		$timestamp = $row->now;

		list($data, $time) = explode(' ', $timestamp);

		list($ano, $mes, $dia) = explode('-', $data);

        $sql = "SELECT DAY(data) as dia, COUNT(id) as views FROM usuarios WHERE 
        	YEAR(data) = YEAR(NOW()) AND MONTH(data) = MONTH(DATE_SUB(NOW(), INTERVAL 1 MONTH)) GROUP BY DAY(data)";
        $query = $this->db->query($sql);
       
        if($query->rowCount() > 0) {
            $result = $query->fetchAll(PDO::FETCH_OBJ);

            foreach($result as $res):
                $dados[$res->dia] = $res->views;
            endforeach;
        }

        $dias_do_mes = $this->_dias_do_mes_passado($mes, $ano);
        $final = array_replace($dias_do_mes, $dados);
        
        $dias = '';
        $cadastros = '';
        foreach ($final as $key => $value) {
        	
        	$dias .= $key.',';
        	$cadastros .= $value.',';
        }

        return array('dias' => $dias, 'cadastros' => $cadastros);
	}

	private function _dias_do_mes($mes, $ano) {
		$esse_mes = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);

		$dias = [1 => '0'];
		for ($i = 1; $i < $esse_mes; $i++):
			array_push($dias, '0');
		endfor;

		return $dias;
	}

	private function _dias_do_mes_passado($mes, $ano) {
		$esse_mes = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);

		$dias = [1 => '0'];
		for ($i = 1; $i < $esse_mes; $i++):
			array_push($dias, '0');
		endfor;

		return $dias;
	}

	public function get_dados_usuario($id) {

		$sql = "SELECT * FROM usuarios WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();

		} else {

			$sql = '1';
		}

		echo json_encode($sql);
	}
}












