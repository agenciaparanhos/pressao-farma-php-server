<?php
class Promocoes_produtos extends model {

	public function add($id_produto, $id_promocao, $desconto) {

		$sql = "SELECT * FROM promocoes_produtos WHERE id_produto = '$id_produto' AND id_promocao = '$id_promocao' AND desconto = '$desconto'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() == 0) {

			$sql = "INSERT INTO promocoes_produtos SET id_promocao = '$id_promocao', id_produto = '$id_produto', desconto = '$desconto'";
			$this->db->query($sql);

			$_SESSION['error'] = "Adicionado com sucesso!";

			header('location: '.BASE.'promocoes/produtos/'.$id_promocao);
		} else {

			$_SESSION['error'] = "O Produto selecionado já está cadastrado nesta promoção!";
			header('location: '.BASE.'promocoes/produtos/'.$id_promocao);
		}
	}

	public function get($id) {

		$sql = "SELECT * FROM promocoes_produtos WHERE id_promocao = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetchAll();

			foreach ($sql as $key => $value) {
				
				$s = "SELECT * FROM produtos WHERE id = '".$value['id_produto']."'";
				$s = $this->db->query($s);

				if($s->rowCount() > 0) {

					$s = $s->fetch();

					$sql[$key]['produto'] = $s;

					$s = "SELECT * FROM lojas WHERE id = '".$s['id_loja']."'";
					$s = $this->db->query($s);

					if($s->rowCount() > 0) {

						$sql[$key]['loja'] = $s->fetch();
					}
				}
			}
		}

		return $sql;
	}

	public function deletar($id, $id_promocao) {

		$sql = "DELETE FROM promocoes_produtos WHERE id = '$id'";
		$sql = $this->db->query($sql);

		$_SESSION['error'] = 'Deletado com sucesso!';

		header('location: '.BASE.'promocoes/produtos/'.$id_promocao);
	}

	public function get_dados($id) {

		$sql = "SELECT * FROM promocoes_produtos WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();

			echo json_encode($sql);
		} else {

			echo json_encode('1');
		}
	}

	public function update($id_produto, $id, $desconto, $id_promocao) {

		$sql = "SELECT * FROM promocoes_produtos WHERE id_produto = '$id_produto' AND id_promocao = '$id_promocao' AND desconto = '$desconto' AND id != '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() == 0) {

			$sql = "UPDATE promocoes_produtos SET id_produto = '$id_produto', id_promocao = '$id_promocao', desconto = '$desconto' WHERE id = '$id'";
			$this->db->query($sql);

			$_SESSION['error'] = "Editado com sucesso!";

			header('location: '.BASE.'promocoes/produtos/'.$id_promocao);
		} else {

			$_SESSION['error'] = "A Categoria digitado já existe!";
			header('location: '.BASE.'promocoes/produtos/'.$id_promocao);
		}
	}
}











