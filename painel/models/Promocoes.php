<?php
class Promocoes extends model {

	public function add($nome, $cor) {

		$sql = "SELECT * FROM promocoes WHERE nome = '$nome'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() == 0) {

			$sql = "INSERT INTO promocoes SET nome = '$nome', cor = '$cor'";
			$this->db->query($sql);

			$_SESSION['error'] = "Adicionado com sucesso!";

			header('location: '.BASE.'promocoes');
		} else {

			$_SESSION['error'] = "A Categoria digitado já existe!";
			header('location: '.BASE.'promocoes');
		}
	}

	public function get() {

		$sql = "SELECT * FROM promocoes";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetchAll();

			foreach ($sql as $key => $value) {
				
				$s = "SELECT count(id) as tot FROM promocoes_produtos WHERE id_promocao = '".$value['id']."'";
				$s = $this->db->query($s);

				if($s->rowCount() > 0) {

					$sql[$key]['tot_produtos'] = $s->fetch()['tot'];
				}
			}
		}

		return $sql;
	}

	public function get_one($id) {

		$sql = "SELECT * FROM promocoes WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();
		}

		return $sql;
	}

	public function deletar($id) {

		$sql = "DELETE FROM promocoes WHERE id = '$id'";
		$sql = $this->db->query($sql);

		$_SESSION['error'] = 'Deletado com sucesso!';

		header('location: '.BASE.'promocoes');
	}

	public function get_dados($id) {

		$sql = "SELECT * FROM promocoes WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();

			echo json_encode($sql);
		} else {

			echo json_encode('1');
		}
	}

	public function update($nome, $id, $cor) {

		$sql = "SELECT * FROM promocoes WHERE nome = '$nome' AND id != '$id'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() == 0) {

			$sql = "UPDATE promocoes SET nome = '$nome', cor = '$cor' WHERE id = '$id'";
			$this->db->query($sql);

			$_SESSION['error'] = "Editado com sucesso!";

			header('location: '.BASE.'promocoes');
		} else {

			$_SESSION['error'] = "A Categoria digitado já existe!";
			header('location: '.BASE.'promocoes');
		}
	}

	public function get_tot() {

		$sql = "SELECT count(*) as tot FROM promocoes";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {

			$sql = $sql->fetch();

			$sql = $sql['tot'];
		}

		return $sql;
	}
}











