<?php 
class homeController extends controller {

	public function __construct() {
		$a = new Admin();
		if($a->is_logged() == false) {
			header("location: ".BASE."login");
		}
	}

	public function index() {
		
		$dados = array();

		$u = new Usuarios();
		$l = new Lojas();
		$p = new Produtos_venda();
		$pd = new Produtos();
		// $l = new Lavacoes();
		// $a = new Agendamentos();

		$dados['tot_usuarios'] = $u->get_tot_usuarios();
		$dados['tot_lojas'] = $l->get_tot();
		$dados['tot_pagamentos'] = $p->get_tot();
		$dados['tot_produtos'] = $pd->get_tot();
		// $dados['tot_lavacoes'] = $l->get_tot();
		// $dados['tot_agendamentos'] = $a->get_tot();
		// $dados['tot_pagamentos'] = $a->get_tot_pagamentos();
		$dados['cadastros'] 	= $u->get_cadastros();
		$dados['cadastros_2'] 	= $u->get_cadastros_2();

		$this->loadTemplate('home', $dados);
	}
}

















