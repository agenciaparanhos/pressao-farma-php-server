<?php
class editar_contaController extends controller {

	public function __construct() {
		$a = new Admin();
		if($a->is_logged() == false) {
			header("location: ".BASE."login");
		}
	}

	public function index() {
		$dados = array();

		$a = new Admin();
		$co = new Config();

		$dados['config'] = $co->get_config();
		$dados['admin'] = $a->get_dados_admin($_SESSION['AppLanches']);

		$this->loadTemplate('editar_conta', $dados);

		unset($_SESSION['error']);
	}

	public function editar() {

		if(isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['nome']) && !empty($_POST['nome']) && isset($_POST['senha']) && !empty($_POST['senha']) && isset($_POST['senha_2']) && !empty($_POST['senha_2']) && isset($_POST['email_contato']) && !empty($_POST['email_contato'])) {

			$email = addslashes($_POST['email']);
			$nome = addslashes($_POST['nome']);
			$email_contato = addslashes($_POST['email_contato']);
			$senha = md5($_POST['senha']);
			$senha_2 = md5($_POST['senha_2']);

			$a = new Admin();
			$c = new Config();

			if($senha == $senha_2) {
				
				$a->update_admin($email, $nome, $senha, $_SESSION['AppLanches']);
				$c->update_emailcontato($email_contato);
			} else {

				$_SESSION['erro_update_conta'] = '<strong>Oops: </strong>As senha não correspondem!';
				header('location: '.BASE.'editar');
			}
		} else {

			$_SESSION['erro_update_conta'] = '<strong>Oops: </strong>Todos os campos são obrigatórios!';
			header('location: '.BASE.'editar');
		}
	}
}