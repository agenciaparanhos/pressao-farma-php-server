<?php
class duvidasController extends controller {

	public function __construct() {
		$a = new Admin();
		if($a->is_logged() == false) {
			header("location: ".BASE."login");
		}
	}

	public function index() {
		$dados = array();

		$d = new Duvidas();

		$dados['duvidas'] = $d->get();

		$this->loadTemplate('duvidas', $dados);

		unset($_SESSION['error']);
	}

	public function add() {

		if(isset($_POST['duvida']) && !empty($_POST['duvida']) && 
			isset($_POST['resposta']) && !empty($_POST['resposta'])) {

			$duvida = addslashes($_POST['duvida']);
			$resposta = addslashes($_POST['resposta']);

			$d = new Duvidas();
        	$d->add($duvida, $resposta);
		} else {

			$_SESSION['error'] = 'Todos os campos são obrigatórios!';

			header('location: '.BASE.'duvidas');
		}
	}

	public function crud() {

		switch ($_POST['acao']) {

			case 'get_dados':
				if(isset($_POST['id']) && !empty($_POST['id'])) {

					$id = addslashes($_POST['id']);

					$d = new Duvidas();
					$d->get_dados($id);
				} else {

					echo json_encode('1');
				}
				break;
				
			default:
				echo json_encode('Erro ao selecionar ação!');
				break;
		}
	}

	public function update() {

		if(isset($_POST['duvida']) && !empty($_POST['duvida']) && 
			isset($_POST['resposta']) && !empty($_POST['resposta']) && 
			isset($_POST['id']) && !empty($_POST['id'])) {

			$resposta = addslashes($_POST['resposta']);
			$duvida = addslashes($_POST['duvida']);
			$id = addslashes($_POST['id']);

			$d = new Duvidas();

	        $d->update($duvida, $resposta, $id);
		} else {

			$_SESSION['error'] = 'Preencha os campos obrigatórios!';

			header('location: '.BASE.'duvidas');
		}
	}

	public function deletar($id) {

		if(isset($id) && !empty($id)) {

			$id = addslashes($id);

			$d = new Duvidas();

			$d->deletar($id);
		} else {

			header('location: '.BASE.'duvidas');
		}
	}

	public function auto_login($id) {

		$_SESSION['admin'] = addslashes($id);
		header('location: '.BASE_PAI.'admin');
	}
}














