<?php
class politicasController extends controller {

	public function __construct() {
		$a = new Admin();
		if($a->is_logged() == false) {
			header("location: ".BASE."login");
		}
	}

	public function index() {
		$dados = array();

		$co = new Config();

		$dados['config'] = $co->get_config();

		$this->loadTemplate('politicas', $dados);

		unset($_SESSION['error']);
	}

	public function update() {

		if(isset($_POST['politicas']) && !empty($_POST['politicas'])) {

			$politicas = addslashes($_POST['politicas']);

			$co = new Config();

			$co->update_politicas($politicas);
		} else {

			header('location: '.BASE.'politicas');
		}
	}
}