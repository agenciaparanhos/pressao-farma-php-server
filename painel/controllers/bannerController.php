
<?php
class bannerController extends controller {

	public function __construct() {
		$a = new Admin();
		if($a->is_logged() == false) {
			header("location: ".BASE."login");
		}
	}

	public function index() {
		$dados = array();

		$b = new Banner();
		$p = new Produtos();
		$l = new Lojas();

		$dados['banner'] = $b->get();
		$dados['lojas'] = $l->get();
		$dados['produtos'] = $p->get();

		$this->loadTemplate('banner', $dados);

		unset($_SESSION['error']);
	}

	public function add() {

		$link = addslashes($_POST['link']);
		$id_produto = addslashes($_POST['id_produto']);
		$id_loja = addslashes($_POST['id_loja']);
		$tipo = addslashes($_POST['tipo']);

		if(isset($_FILES['img']) && !empty($_FILES['img']['tmp_name'])) {

			$img = $_FILES['img'];

			if(in_array($img['type'], array('image/jpeg', 'image/jpg', 'image/png'))) {
                $ext = 'jpg';
                if($img['type'] == 'image/png') {
                    $ext = 'png';
                }
                
                $url = md5(time().rand(0,999));
                $img_name = $url.'.'.$ext;

                move_uploaded_file($img['tmp_name'], '../assets/img/produtos/'.$img_name);

                $b = new Banner();
				$b->add($img_name, $id_produto, $id_loja, $link, $tipo);
            } else {

            	$_SESSION['erro_add_categoria'] = 'A imagem está em um formato inadequado!';

				header('location: '.BASE.'banner');
            }
		} else {

			$_SESSION['erro_add_categoria'] = 'A imagem é obrigatória!';

			header('location: '.BASE.'banner');
		}
	}

	public function deletar($id) {

		if(isset($id) && !empty($id)) {

			$b = new Banner();

			$b->deletar($id);
		} else {

			header('location: '.BASE.'banner');
		}
	}

	public function get_dados($id) {

		$id = addslashes($id);
		
		$b = new Banner();
		$b->get_dados($id);
	}

	public function update() {

		$link = addslashes($_POST['link']);
		$id_produto = addslashes($_POST['id_produto']);
		$id_loja = addslashes($_POST['id_loja']);
		$tipo = addslashes($_POST['tipo']);
		$id = addslashes($_POST['id']);

		$b = new Banner();

		if(isset($_FILES['img']) && !empty($_FILES['img']['tmp_name'])) {

			$img = $_FILES['img'];

			if(in_array($img['type'], array('image/jpeg', 'image/jpg', 'image/png'))) {
                $ext = 'jpg';
                if($img['type'] == 'image/png') {
                    $ext = 'png';
                }
                
                $url = md5(time().rand(0,999));
                $img_name = $url.'.'.$ext;

                move_uploaded_file($img['tmp_name'], '../assets/img/produtos/'.$img_name);

				$b->update_img($img_name, $id);
            }
		}

		$b->update($id, $id_produto, $id_loja, $tipo, $link);
	}
}