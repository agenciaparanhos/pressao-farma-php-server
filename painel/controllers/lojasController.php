<?php
class lojasController extends controller {

	public function __construct() {
		$a = new Admin();
		if($a->is_logged() == false) {
			header("location: ".BASE."login");
		}
	}

	public function index() {
		$dados = array();

		$l = new Lojas();

		$dados['lojas'] = $l->get();

		$this->loadTemplate('lojas', $dados);

		unset($_SESSION['error']);
	}

	public function deletar($id) {

		if(isset($id) && !empty($id)) {

			$l = new Lojas();

			$l->deletar($id);
		} else {

			header('location: '.BASE.'lojas');
		}
	}
}