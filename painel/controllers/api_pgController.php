<?php
class api_pgController extends controller {

	public function __construct() {
		$a = new Admin();
		if($a->is_logged() == false) {
			header("location: ".BASE."login");
		}
	}

	public function index() {
		$dados = array();

		$c = new Config();

		$dados['config'] = $c->get_config();

		$this->loadTemplate('api_pg', $dados);

		unset($_SESSION['error']);
	}

	public function update() {

		$MP_SecretKey = addslashes($_POST['MP_SecretKey']);
		$MP_AppID = addslashes($_POST['MP_AppID']);

		$a = new Config();
	
		$a->update_api_pg($MP_SecretKey, $MP_AppID);
	}
}