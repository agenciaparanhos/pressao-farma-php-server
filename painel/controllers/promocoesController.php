<?php
class promocoesController extends controller {

	public function __construct() {
		$a = new Admin();
		if($a->is_logged() == false) {
			header("location: ".BASE."login");
		}
	}

	public function index() {
		$dados = array();

		$p = new Promocoes();

		$dados['promocoes'] = $p->get();

		$this->loadTemplate('promocoes', $dados);

		unset($_SESSION['error']);
	}

	public function add() {

		$nome = addslashes($_POST['nome']);
		$cor = addslashes($_POST['cor']);

		$p = new Promocoes();
    	$p->add($nome, $cor);
	}

	public function crud() {

		switch ($_POST['acao']) {

			case 'get_dados':
				if(isset($_POST['id']) && !empty($_POST['id'])) {

					$id = addslashes($_POST['id']);

					$p = new Promocoes();
					$p->get_dados($id);
				} else {

					echo json_encode('1');
				}
				break;

			case 'get_dados_produto':
				if(isset($_POST['id']) && !empty($_POST['id'])) {

					$id = addslashes($_POST['id']);

					$p = new Promocoes_produtos();
					$p->get_dados($id);
				} else {

					echo json_encode('1');
				}
				break;
				
			default:
				echo json_encode('Erro ao selecionar ação!');
				break;
		}
	}

	public function update() {

		$nome = addslashes($_POST['nome']);
		$cor = addslashes($_POST['cor']);
		$id = addslashes($_POST['id']);

		$p = new Promocoes();

    	$p->update($nome, $id, $cor);
	}

	public function deletar($id) {

		if(isset($id) && !empty($id)) {

			list($id, $id_promocao) = explode('_', $id);

			$id = addslashes($id);
			$id_promocao = addslashes($id_promocao);

			$p = new Promocoes_produtos();

			$p->deletar($id, $id_promocao);
		} else {

			header('location: '.BASE.'promocoes');
		}
	}

	public function produtos($id) {
		$dados = array();

		$p = new Promocoes_produtos();
		$pd = new Produtos();
		$pm = new Promocoes();

		$id = addslashes($id);

		$dados['promocoes_produtos'] = $p->get($id);
		$dados['produtos'] = $pd->get();
		$dados['promocao'] = $pm->get_one($id);

		$this->loadTemplate('promocoes_produtos', $dados);

		unset($_SESSION['error']);
	}

	public function add_produto() {

		$id_produto = addslashes($_POST['id_produto']);
		$id_promocao = addslashes($_POST['id_promocao']);
		$desconto = addslashes($_POST['desconto']);

		$p = new Promocoes_produtos();
    	$p->add($id_produto, $id_promocao, $desconto);
	}

	public function update_produto() {

		$desconto = addslashes($_POST['desconto']);
		$id_produto = addslashes($_POST['id_produto']);
		$id_promocao = addslashes($_POST['id_promocao']);
		$id = addslashes($_POST['id']);

		$p = new Promocoes_produtos();

    	$p->update($id_produto, $id, $desconto, $id_promocao);
	}
}














