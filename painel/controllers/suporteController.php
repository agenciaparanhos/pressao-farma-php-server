<?php
class suporteController extends controller {

	public function __construct() {
		$a = new Admin();
		if($a->is_logged() == false) {
			header("location: ".BASE."login");
		}
	}

	public function index() {
		$dados = array();

		$a = new Suporte();

		$dados['suporte'] = $a->get();

		$this->loadTemplate('suporte', $dados);

		unset($_SESSION['error']);
	}

	public function crud() {

		switch ($_POST['acao']) {

			case 'get_dados':
				if(isset($_POST['id']) && !empty($_POST['id'])) {

					$id = addslashes($_POST['id']);

					$s = new Suporte();
					$s->get_dados($id);
				} else {

					echo json_encode('1');
				}
				break;

			default:
				echo json_encode('Erro ao selecionar ação!');
				break;
		}
	}

	public function update() {

		if(isset($_POST['id']) && !empty($_POST['id'])) {

			$resposta = addslashes($_POST['resposta']);
			$id = addslashes($_POST['id']);

			$a = new Suporte();

	        $a->update($resposta, $id);
		} else {

			$_SESSION['error'] = 'Preencha os campos obrigatórios!';

			header('location: '.BASE.'suporte');
		}
	}

	public function deletar($id) {

		if(isset($id) && !empty($id)) {

			$id = addslashes($id);

			$v = new Suporte();

			$v->deletar($id);
		} else {

			header('location: '.BASE.'suporte');
		}
	}
}














