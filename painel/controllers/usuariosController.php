<?php
class usuariosController extends controller {

	public function __construct() {
		$a = new Admin();
		if($a->is_logged() == false) {
			header("location: ".BASE."login");
		}
	}

	public function index() {
		$dados = array();

		$u = new Usuarios();
		$c = new Config();

		$dados['config'] = $c->get_config();
		$dados['usuarios'] = $u->get_usuarios();

		$this->loadTemplate('usuarios', $dados);

		unset($_SESSION['error']);
	}

	public function add() {

		if(isset($_POST['telefone']) && !empty($_POST['telefone']) && 
			isset($_POST['nome']) && !empty($_POST['nome']) && 
			isset($_POST['email']) && !empty($_POST['email']) && 
			isset($_POST['senha']) && !empty($_POST['senha'])) {

			$telefone = addslashes($_POST['telefone']);
			$email = addslashes($_POST['email']);
			$nome = addslashes($_POST['nome']);
			$senha = md5($_POST['senha']);
			$u = new Usuarios();
			$u->add_usuario($telefone, $nome, $senha, $email);
		} else {

			$_SESSION['error'] = 'Preencha todos os campos';
			header('location: '.BASE.'usuarios');
		}
	}

	public function crud() {

		switch ($_POST['acao']) {

			case 'get_dados_usuario':
				if(isset($_POST['id']) && !empty($_POST['id'])) {

					$id = addslashes($_POST['id']);

					$u = new Usuarios();
					$u->get_dados_usuario($id);
				} else {

					echo json_encode('1');
				}
				break;
			
			default:
				echo json_encode('Erro ao selecionar ação!');
				break;
		}
	}

	public function deletar($id) {

		if(isset($id) && !empty($id)) {

			$u = new Usuarios();

			$u->deletar($id);
		} else {

			header('location: '.BASE.'usuarios');
		}
	}

	public function update() {

		if(isset($_POST['telefone']) && !empty($_POST['telefone']) && 
			isset($_POST['nome']) && !empty($_POST['nome']) && 
			isset($_POST['email']) && !empty($_POST['email'])) {

			$telefone = addslashes($_POST['telefone']);
			$email = addslashes($_POST['email']);
			$nome = addslashes($_POST['nome']);
			$id = addslashes($_POST['id']);

			$u = new Usuarios();

			if(isset($_POST['senha']) && !empty($_POST['senha'])) {

				$senha = md5($_POST['senha']);

				$u->update_senha($id, $senha);
			}

			$u->update($telefone, $nome, $id, $email);
		} else {

			$_SESSION['error'] = 'Preencha os campos obrigatórios';
			header('location: '.BASE.'usuarios');
		}
	}
}