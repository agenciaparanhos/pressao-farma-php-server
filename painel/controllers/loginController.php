<?php
class loginController extends controller {

	public function __construct() {
		$a = new Admin();
		if($a->is_logged() == true) {
			header("location: ".BASE);
		}
	}

	public function index() {

		$dados = array();

		$this->loadView('login', $dados);
	}

	public function sair() {

		unset($_SESSION['AppLanches']);

		header("location: ".BASE.'login');
	}

	public function crud() {
		switch ($_POST['acao']) {
			case 'logar':

				if(isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['senha']) && !empty($_POST['senha'])) {

					$email = addslashes($_POST['email']);
					$senha = md5($_POST['senha']);

					$a = new Admin();
					$a->logar($email, $senha);
				} else {

					echo json_encode('1');
				}
				break;
			
			default:
				echo json_encode('Erro ao selecionar ação!');
				break;
		}
	}
}