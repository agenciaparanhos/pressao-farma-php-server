<?php
class pagamentosController extends controller {

	public function __construct() {
		$a = new Admin();
		if($a->is_logged() == false) {
			header("location: ".BASE."login");
		}
	}

	public function index() {
		$dados = array();

		$p = new Produtos_venda();

		$dados['pagamentos'] = $p->get();

		$this->loadTemplate('pagamentos', $dados);

		unset($_SESSION['error']);
	}

	public function deletar($id) {

		if(isset($id) && !empty($id)) {

			$p = new Produtos_venda();

			$p->deletar($id);
		} else {

			header('location: '.BASE.'pagamentos');
		}
	}
}