<?php
class pushController extends controller {

	public function __construct() {
		$a = new Admin();
		if($a->is_logged() == false) {
			header("location: ".BASE."login");
		}
	}

	public function index() {
		$dados = array();
		$a = new Avisos();
		$co = new Config();

		$dados['config'] = $co->get_config();
		$dados['avisos'] = $a->get_avisos();

		$this->loadTemplate('push', $dados);

		unset($_SESSION['error']);
	}

	public function add() {

		if(isset($_POST['titulo']) && !empty($_POST['titulo']) && 
			isset($_POST['msg']) && !empty($_POST['msg'])) {

			$titulo = addslashes($_POST['titulo']);
			$msg = addslashes($_POST['msg']);

			$a = new Avisos();

			if(isset($_FILES['img']) && !empty($_FILES['img']['tmp_name'])) {

				$img = $_FILES['img'];

				if(in_array($img['type'], array('image/jpeg', 'image/jpg', 'image/png'))) {
	                $ext = 'jpg';
	                if($img['type'] == 'image/png') {
	                    $ext = 'png';
	                }
	                
	                $url = md5(time().rand(0,999));
	                $img_name = $url.'.'.$ext;

	                move_uploaded_file($img['tmp_name'], '../assets/img/push/'.$img_name);

		            $a->add($img_name, $titulo, $msg);
	            } else {

	            	$_SESSION['error'] = 'A imagem está em um formato inadequado!';

					header('location: '.BASE.'push');
	            }
	        } else {

	        	$a->add('', $titulo, $msg);
	        }

		} else {
			
			header('location: '.BASE.'push');
		}
	}

	public function add_usuario() {

		if(isset($_POST['titulo']) && !empty($_POST['titulo']) && 
			isset($_POST['id']) && !empty($_POST['id']) &&
			isset($_POST['msg']) && !empty($_POST['msg'])) {

			$titulo = addslashes($_POST['titulo']);
			$msg = addslashes($_POST['msg']);
			$id = addslashes($_POST['id']);

			$a = new Avisos();

			if(isset($_FILES['img']) && !empty($_FILES['img']['tmp_name'])) {

				$img = $_FILES['img'];

				if(in_array($img['type'], array('image/jpeg', 'image/jpg', 'image/png'))) {
	                $ext = 'jpg';
	                if($img['type'] == 'image/png') {
	                    $ext = 'png';
	                }
	                
	                $url = md5(time().rand(0,999));
	                $img_name = $url.'.'.$ext;

	                move_uploaded_file($img['tmp_name'], '../assets/img/push/'.$img_name);

		            $a->add_usuario($img_name, $titulo, $msg, $id);
	            } else {

	            	$_SESSION['error'] = 'A imagem está em um formato inadequado!';

					header('location: '.BASE.'push');
	            }
	        } else {

	        	$a->add_usuario('', $titulo, $msg, $id);
	        }

		} else {
			
			header('location: '.BASE.'usuarios');
		}
	}

	public function add_whatsapp() {

		if(isset($_POST['id']) && !empty($_POST['id']) &&
			isset($_POST['msg']) && !empty($_POST['msg'])) {

			$msg = addslashes($_POST['msg']);
			$id = addslashes($_POST['id']);

			$a = new Avisos();
			$a->add_whatsapp($id, $msg);
		} else {
			
			header('location: '.BASE.'usuarios');
		}
	}

	public function deletar($id) {

		if(isset($id) && !empty($id)) {

			$a = new Avisos();

			$a->deletar($id);
		} else {

			header('location: '.BASE.'noticias');
		}
	}
}











