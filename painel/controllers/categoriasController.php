<?php
class categoriasController extends controller {

	public function __construct() {
		$a = new Admin();
		if($a->is_logged() == false) {
			header("location: ".BASE."login");
		}
	}

	public function index() {
		$dados = array();

		$c = new Categorias();

		$dados['categorias'] = $c->get();

		$this->loadTemplate('categorias', $dados);

		unset($_SESSION['error']);
	}

	public function add() {

		$nome = addslashes($_POST['nome']);

		$c = new Categorias();
    	$c->add($nome);
	}

	public function add_sub() {

		$nome = addslashes($_POST['nome']);
		$id_categoria = addslashes($_POST['id_categoria']);

		$c = new Subcategorias();
    	$c->add($nome, $id_categoria);
	}

	public function crud() {

		switch ($_POST['acao']) {

			case 'get_dados':
				if(isset($_POST['id']) && !empty($_POST['id'])) {

					$id = addslashes($_POST['id']);

					$c = new Categorias();
					$c->get_dados($id);
				} else {

					echo json_encode('1');
				}
				break;

			case 'get_dados_sub':
				if(isset($_POST['id']) && !empty($_POST['id'])) {

					$id = addslashes($_POST['id']);

					$c = new Subcategorias();
					$c->get_dados($id);
				} else {

					echo json_encode('1');
				}
				break;
				
			default:
				echo json_encode('Erro ao selecionar ação!');
				break;
		}
	}

	public function update() {

		$nome = addslashes($_POST['nome']);
		$id = addslashes($_POST['id']);

		$c = new Categorias();

    	$c->update($nome, $id);
	}

	public function update_sub() {

		$nome = addslashes($_POST['nome']);
		$id = addslashes($_POST['id']);
		$id_categoria = addslashes($_POST['id_categoria']);

		$c = new Subcategorias();

    	$c->update($nome, $id, $id_categoria);
	}

	public function deletar($id) {

		if(isset($id) && !empty($id)) {

			$id = addslashes($id);

			$c = new Categorias();

			$c->deletar($id);
		} else {

			header('location: '.BASE.'categorias');
		}
	}

	public function deletar_sub($id) {

		if(isset($id) && !empty($id)) {

			list($id, $id_categoria) = explode('_', $id);

			$id = addslashes($id);
			$id_categoria = addslashes($id_categoria);

			$c = new Subcategorias();

			$c->deletar($id, $id_categoria);
		} else {

			header('location: '.BASE.'categorias');
		}
	}

	public function sub($id) {

		if(isset($id) && !empty($id)) {

			$id = addslashes($id);

			$dados = array();

			$c = new Categorias();
			$s = new Subcategorias();

			$dados['categoria'] = $c->get_one($id);
			$dados['sub'] = $s->get($id);

			$this->loadTemplate('subcategorias', $dados);

			unset($_SESSION['error']);
		} else {

			header('location: '.BASE.'categorias');
		}
	}
}














